require("./test1.js")

var a = 69 - 0x64;
let b = true;
var c = "test";
var d = null;
var e = `oui ${c}`
var f = /ab+c/;

{
    ;
}

with (a) {
    return;
}

label:
while (b) {
    if (a != b) {
        break;
    }
    continue;
}

switch (a) {
    case 69:
        try {
            throw "pas de chance";
        }
        catch (err) {
            ;
        }
        finally {
            ;
        }
}

do; while (b);

for (;;);

const obj = {a: 1, b: 2};

for (value in obj);

const obj2 = [1, 2];

for (value of obj2);

function test() {}

class Test {
    method() {}
};

let x = new Test();
this.method;

hello = () => {
    return 1 || 2;
}

f = -a-- - b ? 1 : 0;

test();

var g, h = 1;