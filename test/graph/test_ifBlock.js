
// // if/else - if in else with block
// if (true) {
// } else {
//     if (false) {
//     }
// }

// // PASS if/else - if in then with no block
// if (true)
//     if (false) {
//     } else {
//         c = 3;
//     }

// // PASS if - if in if - if in if in if
// if (true)
//     if (true)
//         if (true)
//             a = 1;

// // PASS if - if in if - if in if in if block
// if (true)
//     if (true)
//         if (true) {
//             a = 1;
//         }

// // PASS if/else - if in then
// if (true) {
//     if (false) {
//     }
// } else {
//     c = 3;
// }

// // if - no else
// if (true) {
//     a = 1;
// }

// // PASS if/else if/else - one not block
// if (true) {
//     a = 1;
// } else if (true && true)
//     b = 2;
// else {
//     c = 3;
// }

// // PASS if/else if - full block
// if (true) {
//     a = 1;
// } else if (true && true) {
//     b = 2;
// }

// // PASS if/else if/else - full block
// if (true) {
//     a = 1;
// } else if (true && true) {
//     b = 2;
// } else {
//     c = 3;
// }
