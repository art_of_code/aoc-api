"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require('fs');
const babel_eslint_1 = require("babel-eslint");
const index = require("deadcode");
try {
    const filename = process.argv[2];
    const reportName = process.argv[3];
    if (!filename)
        throw "missing filename argument.";
    if (!reportName)
        throw "missing report name argument.";
    let data = fs.readFileSync(filename, 'utf8');
    let ast = { ast: babel_eslint_1.parse(data, {}) };
    index.handler(ast, undefined)
        .then((ret) => {
        if (ret.statusCode !== 200) {
            console.log('Error:', ret.infos);
        }
        else {
            for (let i = 3; i < process.argv.length; i++) {
                const reportName = process.argv[i];
                console.log(`${reportName}:`);
                if (ret.infos._report[reportName]) {
                    ret.infos._report[reportName].forEach((report) => console.log(report));
                }
            }
        }
    })
        .catch((e) => {
        if (typeof e === 'string') {
            console.log('Error:', e);
        }
        else {
            console.log('Error:', e.stack);
        }
    });
}
catch (e) {
    if (typeof e === 'string') {
        console.log('Error:', e);
    }
    else {
        console.log('Error:', e.stack);
    }
}
//# sourceMappingURL=test.js.map