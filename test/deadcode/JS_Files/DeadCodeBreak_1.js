function CallFunction(i)
{
    return (5);
}

function DeadCodeTest()
{
    let i = 0;

    while (i <= 5)
    {
        if (i === 3)
        {
            console.log("I want to leave this loop and don't execute the rest of the code");
            break;
            i = Callfunction();
            i++;
            console.log("Print after break");
        }
        if (i === 5)
        {
            console.log("I value is 5, you win");
            return (true);
        }
        i++;
    }

    console.log("Hello");
}

DeadCodeTest();