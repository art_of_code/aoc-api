function main()
{
    let i = 0;

    while (i >= 0)
    {
        if (i === 3)
        {
            console.log("I break the loop");
            break;
        }
        i++;
    }
    console.log("i = " + i + "(should be 3)");
    return (true);
}

main();