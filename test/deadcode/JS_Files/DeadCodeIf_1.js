function Called(i)
{
    if (i === 7)
        return (true);
    return (false);
}

function main()
{
    let i = 0;

    i = 5;
    if (i === 5)
    {
        console.log("I return false");
        return (false);
        i = 7;
        if (Called(i))
            console.log("i == 7");
        else
            console.log("i != 7");

    }
    else
    {
        console.log("I wont be printed");
    }
    return (true);

    console.log("Hello dead code");
}

main();