function third()
{
    for (let i = 5; i <= 10; i++)
    {
        if (i === 9)
        {
            return (false);
            console.log("Close to end loop in third function");
        }
    }
    if (i === 10)
        return (true);
}

function second(i)
{
    return (5);
    if (i === 6)
    {
        console.log("Return 8");
        return (8);
    }
    else
    {
        console.log("Return 84");
        return (84);
    }
    return (6);
}


function first()
{
    let i = 0;

    if (i === 0)
    {
        return (false);
    }
    i = 9;
    console.log("Test first function")
    while (i <= 0)
    {
        i--;
        console.log("I decrement i var in while");
    }
    return (true);
}


function main()
{
    let n = 0;

    console.log("Test before calling first function in main (should print something)");
    console.log("first function = " + first() + " (should be true with all code)");
    console.log("Test before calling second function in main to give value to n var");
    n = second(8);
    console.log(`n = ${n} (should be 84 in code not dead)`);
    console.log("Test before calling third function in main (shoul print something)");
    console.log("third function = " + third() + " (should be true with all code)");

    console.log("Test before return in main (one more print)");
    return (true);
    console.log("Test after return in main");
}

main();