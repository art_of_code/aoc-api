#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m'

file1="received_output.txt"
file2="expected_output.txt"
JS_Files="JS_Files/"

clear_command="clear"
tests_command="test"

path_to_js=""

if [ $# = 0 -o "$1" = "${tests_command}" ]; then
        # npm install
        # npm run-script build

        for entry in JS_Files/*; do
            path_to_js="${entry}"
            entry=${entry//"JS_Files/"}

            echo -e "${CYAN}======== Test for ${entry} in Test_JS/Test_${entry//.js} ========${NC}"

            node test.js "${path_to_js}" "deadcode" > "Test_JS/Test_${entry//.js}/${file1}"

            if cmp -s "Test_JS/Test_${entry//.js}/${file1}" "Test_JS/Test_${entry//.js}/${file2}"; then
                echo -e "${GREEN}The expected and received output are the same for ${entry} test.${NC}\n"
                echo -e "Received output : ${GREEN}\n" && cat "Test_JS/Test_${entry//.js}/${file1}" && echo -e "${NC}"
            else
                echo -e "${RED}The received output is different than expected for ${entry} test.${NC}\n"
                echo -e "Expected output : ${RED}\n" && cat "Test_JS/Test_${entry//.js}/${file2}" && echo -e "${NC}"
                echo -e "Received output : ${RED}\n" && cat "Test_JS/Test_${entry//.js}/${file1}" && echo -e "${NC}"
            fi
        done
elif [ "$1" = "${clear_command}" ]; then
        for entry in JS_Files/*; do
            entry=${entry//"JS_Files/"}
            echo -e "${GREEN}Clear output file for ${entry}${NC}"
            rm Test_JS/Test_${entry//.js}/${file1}
        done
else
    echo -e "\ntest:\t./launch_test_on_js_file.sh test\n"
    echo -e "clear:\t./launch_test_on_js_file.sh clear\n"
fi