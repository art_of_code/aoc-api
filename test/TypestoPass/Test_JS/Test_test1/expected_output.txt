id: a
type: NUMBER
id: b
type: STRING
id: c
type: BOOLEAN
id: d
type: OBJECT {
  id: a
  type: NUMBER
  id: b
  type: STRING
  id: c
  type: BOOLEAN
}
id: e
type: OBJECT {
  id: a
  type: NUMBER
  id: b
  type: STRING
}
id: f
type: UNDEFINED
id: g
type: NULL
id: h
type: OBJECT {
  id: a
  type: NUMBER
  id: b
  type: OBJECT {
    id: aa
    type: BOOLEAN
    id: bb
    type: NULL
  }
}
id: i
type: ARRAY [
]
id: j
type: ERROR
id: k
type: OBJECT {
  id: a
  type: NUMBER
  id: b
  type: STRING
  id: c
  type: BOOLEAN
}
id: f_42
type: FUNCTION(
)
id: f_id
type: FUNCTION(
id:e
type: ANY
)
30: operator === no error
31: operator === no error
32: operator === no error
33: operator === no error
34: operator === no error
35: operator === no error
37: operator === is always false
38: operator === is always false
39: operator === is always false
40: operator === is always false
42: operator === is always false
43: operator === is always false
44: operator === is always false
46: operator === is always false
47: operator === is always false
49: operator === is always false
50: operator === is always false
54: operator == no error
55: operator == no error
56: operator == no error
57: operator == no error
58: operator == no error
59: operator == no error
61: operator == no error
62: operator == no error
63: operator == is always false
64: operator == is always false
66: operator == no error
67: operator == is always false
68: operator == is always false
70: operator == is always false
71: operator == is always false
73: operator == is always false
74: operator == is always false
78: operator === no error
79: operator === no error
80: operator === no error
81: operator === no error
82: operator === no error
83: operator === no error
85: operator === no error
86: operator === no error
87: operator === no error
88: operator === no error
90: operator === no error
91: operator === no error
92: operator === no error
94: operator === no error
95: operator === no error
97: operator === no error
98: operator === no error
102: operator === no error
103: operator === no error
104: operator === no error
105: operator === is always false
106: operator === is always false
107: operator === is always false
109: operator === no error
110: operator === no error
111: operator === is always false
112: operator === is always false
114: operator === no error
115: operator === is always false
116: operator === is always false
118: operator === is always false
119: operator === is always false
121: operator === is always false
122: operator === is always false
126: operator === no error
128: operator === is always false
130: operator === no error
134: operator === no error
135: operator === is always false
136: operator === is always false
137: operator === is always false
138: operator === is always false
139: operator === no error
140: operator === no error
142: operator === no error
143: operator === no error
144: operator === is always false
145: operator === no error
146: operator === is always false
