
const fs = require('fs');

import { parse } from "babel-eslint";
const index = require("typeCheck");

try {
    const filename = process.argv[2];
    const reportName = process.argv[3];

    if (!filename)
        throw "missing filename argument.";
    if (!reportName)
        throw "missing report name argument.";

    let data = fs.readFileSync(filename, 'utf8');

    let ast = {ast: parse(data, {})};

    index.handler(ast, undefined)
    .then((ret:any) => {
        if (ret.statusCode !== 200) {
            console.log('Error:', ret.infos);
        } else {
            if (ret.infos._allVarTypes)
                ret.infos._allVarTypes.forEach(e => e.dump());
            for (const key in ret.infos._report) {
                // console.log(ret.infos._report[key]);
                ret.infos._report[key].forEach(e => {
                    console.log(e.loc.start.line+': '+e.msg);
                });
                // ret.infos._report[key].forEach((report: any) => console.log(JSON.stringify(report)));
            }
            // for (let i = 3; i < process.argv.length; i++) {
            //     const reportName = process.argv[i];
                // console.log(`${reportName}:`);
                // if (ret.infos._report[reportName]) {
                // }
            // }
        }
    })
    .catch((e:any) => {
        if (typeof e === 'string') {
            console.log('Error:', e);
        } else {
            console.log('Error:', e.stack);
        }
    });

} catch(e) {
    if (typeof e === 'string') {
        console.log('Error:', e);
    } else {
        console.log('Error:', e.stack);
    }
}


