/** Test 1
 * No extrenal link
 * No dynamic evaluation
 * No variadic element
 * No branching
 */

const a = 42;
const b = "42";
const c = true;
const d = {a: 1, b: "abc", c: false};
const e = {a: 1, b: "abc"};
const f = undefined;
const g = null;
const h = {a: 1, b: {aa: true, bb: null}};
const i = [];
const i1 = [1, 2];
const i2 = [1, 2, "abc"];
const i3 = [{a: 1, b: "abc"}, {a: 2, b: "abc"}];
const i4 = [{a: 1, b: "abc"}, {b: "abc"}];
const i5 = [false, {b: "abc"}];
const i6 = [[1, 2], [3, 4]];
const i7 = [[[1, 2]], [[3, "bla"]]];
const j = ``;
const k = d;

function f_42() {
    return 42;
}

function f_id(e=1) {
    return e;
}

/** === */

a === a; /** number === number => no error */
b === b; /** string === string => no error */
c === c; /** boolean === boolean => no error */
d === d; /** {a:number, b:string, c: boolean} === {a:number, b:string, c: boolean} => no error */
f_42 === f_42; /** F(number) === F(number) => no error */
f_id === f_id; /** F(a -> a) === F(a -> a) => no error */

a === b; /** number === string => always false */
a === c; /** number === boolean => always false */
a === d; /** number === {a:number, b:string, c: boolean} => always false */
a === f_42; /** number === F(number) => always false */

b === c; /** string === boolean => always false */
b === d; /** string === {a:number, b:string, c: boolean} => always false */
b === f_42; /** string === F(number) => always false */

c === d; /** boolean === {a:number, b:string, c: boolean} => always false */
c === f_42; /** boolean === F(number) => always false */

d === f_42; /** {a:number, b:string, c: boolean} === F(number) => always false */
d === e; /** {a:number, b:string, c: boolean} === {a:number, b:string} => always false */

/** == */

a == a; /** number == number => no error */
b == b; /** string == string => no error */
c == c; /** boolean == boolean => no error */
d == d; /** {a:number, b:string, c: boolean} == {a:number, b:string, c: boolean} => no error */
f_42 == f_42; /** F(number) == F(number) => no error */
f_id == f_id; /** F(a -> a) == F(a -> a) => no error */

a == b; /** number == string => no error */
a == c; /** number == boolean => no error */
a == d; /** number == {a:number, b:string, c: boolean} => always false */
a == f_42; /** number == F(number) => always false */

b == c; /** string == boolean => no error */
b == d; /** string == {a:number, b:string, c: boolean} => always false */
b == f_42; /** string == F(number) => always false */

c == d; /** boolean == {a:number, b:string, c: boolean} => always false */
c == f_42; /** boolean == F(number) => always false */

d == f_42; /** {a:number, b:string, c: boolean} == F(number) => always false */
d == e; /** {a:number, b:string, c: boolean} == {a:number, b:string} => always false */

/** + */

a + a === a; /** number + number === number => no error */
b + b === b; /** string + string === string => no error */
c + c === a; /** boolean + boolean === number => no error */
d + d === b; /** {a:number, b:string, c: boolean} + {a:number, b:string, c: boolean} === string => no error */
f_42 + f_42 === b; /** F(number) + F(number) === string => no error */
f_id + f_id === b; /** F(a -> a) + F(a -> a) === string => no error */

a + b === b; /** number + string === string => no error */
a + c === a; /** number + boolean === number => no error */
a + d === b; /** number + {a:number, b:string, c: boolean} === string => no error */
a + f_42 === b; /** number + F(number) === string => no error */

b + c === b; /** string + boolean === string => no error */
b + d === b; /** string + {a:number, b:string, c: boolean} === string => no error */
b + f_42 === b; /** string + F(number) === string => alwayse false */

c + d === b; /** boolean + {a:number, b:string, c: boolean} === string => no error */
c + f_42 === b; /** boolea + F(number) === string => no error */

d + f_42 === b; /** {a:number, b:string, c: boolean} + F(number) === string => no error */
d + e === b; /** {a:number, b:string, c: boolean} + {a:number, b:string} === string => no error */

/** - */

a - a === a; /** number - number === number => no error */
b - b === a; /** string - string === number => no error */
c - c === a; /** boolean - boolean === number => no error */
d - d === a; /** {a:number, b:string, c: boolean} - {a:number, b:string, c: boolean} === number => always NaN */
f_42 - f_42 === a; /** F(number) - F(number) === number => always NaN */
f_id - f_id === a; /** F(a -> a) - F(a -> a) === number => always NaN */

a - b === a; /** number - string === number => no error */
a - c === a; /** number - boolean === number => no error */
a - d === a; /** number - {a:number, b:string, c: boolean} === number => always NaN */
a - f_42 === a; /** number - F(number) === number => always NaN */

b - c === a; /** string - boolean === number => no error */
b - d === a; /** string - {a:number, b:string, c: boolean} === number => always NaN */
b - f_42 === a; /** string - F(number) === number => always NaN */

c - d === a; /** boolean - {a:number, b:string, c: boolean} === number => always NaN */
c - f_42 === a; /** boolea - F(number) === number => always NaN */

d - f_42 === a; /** {a:number, b:string, c: boolean} - F(number) === number => always NaN */
d - e === a; /** {a:number, b:string, c: boolean} - {a:number, b:string} === number => always NaN */

/** FONCTION */

a === f_42(); /** number === number => no error */

b === f_42(); /** string === number => always false */

b === f_id(b); /** string === string => no error */

/** MEMBRE D OBJET */

d.a === a; /** number === number => no error */
d.a === b; /** number === string => always false */
d.a === c; /** number === boolean => always false */
d.a === d; /** number === {a:number, b:string, c:boolean} => always false */
d.a === f_42; /** number === F(number) => always false */
d.b === b; /** string === string => no error */
d.c === c; /** boolean === boolean => no error */

d.a === e.a; /** number === number => no error */
d.b === e.b; /** string === string => no error */
d.c === e.c; /** boolean === undefined => always false */
h.b.aa === c; /** boolean === boolean => no error */
h.b.aa === h.b.bb; /** boolean === null => always false */