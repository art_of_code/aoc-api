#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m'

file1="received_output.txt"
file2="expected_output.txt"
JS_Files="JS_Files/"

clear_command="clear"
tests_command="test"

path_to_js=""

if [ $# = 0 -o "$1" = "${tests_command}" ]; then
        npm install
        npm run-script build

        for entry in JS_Files/*; do
            path_to_js="${entry}"
            entry=${entry//"JS_Files/"}

            echo -e "${CYAN}======== Test for ${entry} in Test_JS/Test_${entry//.js} ========${NC}"

            numberOfLineToCheck=$(wc -l < "Test_JS/Test_${entry//.js}/${file2}")
            numberOfLineValided=0

            node test.js "${path_to_js}" "tautology" > "Test_JS/Test_${entry//.js}/${file1}"

            while IFS= read -r line_file2
            do
                while IFS= read -r line_file1
                do
                if [ "$(echo ${line_file1} | tr -d '\t{} (),:.')" = "$(echo ${line_file2} | tr -d '\t{} (),:.')" ]; then
                    echo -e "${GREEN} "$(echo ${line_file2} | tr -d '\t{}(),')" ${NC}"
                    let "numberOfLineValided+=1"
                fi
                done <"Test_JS/Test_${entry//.js}/${file1}"
            done <"Test_JS/Test_${entry//.js}/${file2}"
        if [ ${numberOfLineValided} -eq ${numberOfLineToCheck} ]; then
            echo -e "${GREEN}\nResult: \nThe received output is equal than expected for ${entry} test.${NC}"
        else
            echo -e "${RED}\nResult: \nThe received output is different than expected for ${entry} test.${NC}"
        fi
    done
elif [ "$1" = "${clear_command}" ]; then
        for entry in JS_Files/*; do
            entry=${entry//"JS_Files/"}
            echo -e "${GREEN}Clear output file for ${entry}${NC}"
            rm Test_JS/Test_${entry//.js}/${file1}
        done
else
    echo -e "\ntest:\t./launch_test_on_js_file.sh test\n"
    echo -e "clear:\t./launch_test_on_js_file.sh clear\n"
fi