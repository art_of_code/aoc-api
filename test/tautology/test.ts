
const fs = require('fs');

import { parse } from "babel-eslint";
const index = require("tautology");

try {
    const filename = process.argv[2];
    const reportName = process.argv[3];

    if (!filename)
        throw "missing filename argument.";
    if (!reportName)
        throw "missing report name argument.";

    let data = fs.readFileSync(filename, 'utf8');

    let ast = {ast: parse(data, {})};

    index.handler(ast, undefined)
    .then((ret:any) => {
        if (ret.statusCode !== 200) {
            console.log('Error:', ret.infos);
        } else {
            console.log(ret.infos._report);
            /*for (let i = 3; i < process.argv.length; i++) {
                const reportName = process.argv[i];
                console.log(`${reportName}:`);
                if (ret.infos._report[reportName]) {
                    ret.infos._report[reportName].forEach((report: any) => console.log(report));
                }
            }*/
        }
    })
    .catch((e:any) => {
        if (typeof e === 'string') {
            console.log('Error:', e);
        } else {
            console.log('Error:', e.stack);
        }
    });

} catch(e) {
    if (typeof e === 'string') {
        console.log('Error:', e);
    } else {
        console.log('Error:', e.stack);
    }
}


