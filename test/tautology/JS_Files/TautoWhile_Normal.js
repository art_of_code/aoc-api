function Tauto()
{
    let i = 4;

    while (i === 4) // Always True
    {
        if (i === 0) { // Always False
            i++;
            break;
        }
    }
    return (0);
}

Tauto();