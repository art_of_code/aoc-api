function Calledfun(i)
{
    switch (i)
    {
        case 1:
            console.log("I print 1");
            break;
        case 2:
            console.log("I print 2");
            break;
        case 3:
            console.log("I print 3");
            break;
        default:
            break;    
    }
}

function main()
{
    Calledfun(1);
    Calledfun(3);
    Calledfun(2);
    Calledfun(1);
    Calledfun(3);
    Calledfun(3);
    return (true);
}

main();