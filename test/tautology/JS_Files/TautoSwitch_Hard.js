function foo(i)
{
    if (i <= 7) // Always False (never called)
        return (false);
    else
        return (true);
}

function bla(i)
{
    if (i === 6) { // Always False
        foo(4);
        return (true);
    }
    else if (i === 3) { // Always True
        return (false);
    }
}


function Tauto()
{
    let i = 0;

    i = 3;
    switch (i)
    {
        case 0: // Always False
            bla(i)
            return (true);
        case 1: // Always False
            bla(i)
            return (true);
        case 2: // Always False
            bla(i)
            return (true);
        case 3: // Always True
            bla(i)
            return (true);
        default: // Always False
            return (false);
    }
}

Tauto();