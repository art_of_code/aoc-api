function bla(i)
{
    while (i <= 6)
    {
        i++;
        if (i === 9) // Always false
            return (9);
    }
    return (i);
}

function Tauto()
{
    let i = 4;

    while (i === 4) // Always True
    {
        if (i === 0) { // Always false
            i++;
            if (bla(i) === 9) // Always false
                break;
        }
    }
    return (0);
}

Tauto();