"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require('fs');
const babel_eslint_1 = require("babel-eslint");
const index = require("varlog");
try {
    const filename = process.argv[2];
    const reportName = process.argv[3];
    if (!filename)
        throw "missing filename argument.";
    if (!reportName)
        throw "missing report name argument.";
    let data = fs.readFileSync(filename, 'utf8');
    let ast = { ast: babel_eslint_1.parse(data, {}) };
    index.handler(ast, undefined)
        .then((ret) => {
        if (ret.statusCode !== 200) {
            console.log('Error:', ret.infos);
        }
        else {
            console.log(ret.infos._report);
        }
    })
        .catch((e) => {
        if (typeof e === 'string') {
            console.log('Error:', e);
        }
        else {
            console.log('Error:', e.stack);
        }
    });
}
catch (e) {
    if (typeof e === 'string') {
        console.log('Error:', e);
    }
    else {
        console.log('Error:', e.stack);
    }
}
//# sourceMappingURL=test.js.map