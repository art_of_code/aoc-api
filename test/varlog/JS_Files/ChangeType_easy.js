function Foo()
{
    bla = "bla"; // Type string
}

let bla = 42; // type : int

//console.log("bla = " + bla); // bla = 42 -> type int


Foo();
//console.log("bla = " + bla); // bla = bla -> string

bla = ["Foo", "bar"]; // [ 'Foo', 'bar' ] -> type tab
//console.log(bla);