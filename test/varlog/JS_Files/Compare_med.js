var person = { name: 'Foo' };
var otherPerson = person;
if (person === { name: 'Foo' })
    console.log("Bar")          // -> false
if (person === otherPerson)
    console.log("Bla")          // -> true, JS compare reference
