var bla = {
 name: 'Foo',
 printFooBar: function() {
  return 'Bar' + this.name;
 }
}

console.log(bla.printFooBar()); // -> 'BarFoo'
