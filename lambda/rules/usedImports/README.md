# UsedImports

## Description

An import statement is used to import read-only bindings exported by another module.
Not using them is a flaw in the overall logic of the file and should be avoided.

### Field of work

This rule operates in three steps:
- It will first inspect every import statements to gather knowledge about what is imported, and especially the identifier used locally (such as `foo` in `import {bar as foo} from "foobar"`). Duplicates will also be reported.
- It will them iterate over the AST to pinpoint the usage of those identifiers and mark them as used if found within. The reassignment of an identifier is not yet supported and reported.
- When done iterating over the entire AST, it will flag every unused imports.

### Report

This rule returns a report of type "unused", to indicate that an import is unused within the file.

## Work in Progress

This rule needs context to work with in order to understand if an identifier is used as an object, a variable or a function call, in order
to report any wrong usage of an import local keyword (assigning a value over an import keyword is a bad practice).