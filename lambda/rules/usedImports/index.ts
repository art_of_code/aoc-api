import { Infos } from '../../../common/inspector/Infos';
import '../../../common/graph/instanciateAll';
import { regenDict, serializeDict } from "../../../common/inspector/RegenFactory";
import { ErrorMessage } from '../../../common/inspector/Report';
import { ImportStatement } from '../../../common/graph/Statements';
import { Identifier, AssignmentExpression } from '../../../common/graph/Expressions';
import { Block } from '../../../common/graph/Block';

/////////////////
//   COMMONS   //
/////////////////

const header = "usedImports";
const reportType = "unused";


class Import {
	used = false;
	identifiers?: Identifier[];
	keyword = [];
	from = "";
	node: any;
}
let importsList: Import[] = [];

/////////////////
//  FUNCTIONS  //
/////////////////



/////////////////
//   CLASSES   //
/////////////////

/* This will reference everything done with an import */
const usedImportReferencing = function(infos: Infos) {
	if (infos['allImportStatements'] instanceof Array) {
		infos['allImportStatements'].forEach((statement: ImportStatement) => {
			let newImport = new Import;
			newImport.node = statement;
			newImport.from = statement.source.value;

			statement.specifiers.forEach((specifier: any) => {
				const list = importsList.find(i => statement.source.value === i.from);

				if (list) {
					if (!list.keyword.includes(specifier.local.name)) {
						newImport.keyword.push(specifier.local.name);
						importsList.push(newImport);
					} else {
						infos.report.report(new ErrorMessage(header, reportType, `This import is a duplicate of another import.`, statement.loc));
					}
				} else {
					newImport.keyword.push(specifier.local.name);
					importsList.push(newImport);
				}
			});
		});
	}
}

/* This will start looking up the usage of every import, depending on what it is used for */
//TODO
/*
Because it is possible to reassign the name of which the import is referenced locally,
we need to check such things and flag them.
 */
const UsedImportUsage = function (infos: Infos) {
	if (infos['allIdentifiers'] instanceof Array) {
		infos['allIdentifiers'].forEach((identifier: Identifier) => {
			if (importsList.length === 0) {
				return;
			}

			let idx = importsList.findIndex((i) =>  i.keyword.find(keyword => keyword === identifier.name));
			if (idx >= 0) {
				importsList[idx].used = true;
				(importsList[idx].identifiers = (importsList[idx].identifiers || [])).push(identifier)
			}
		});
	}
}

const usedImportOverride = function(infos: Infos) {
	if (importsList instanceof Array) {
		importsList.forEach(imp => {
			if (imp.used === false) { return; }
			const id = (imp.identifiers || []).find(ident => (
				!!ident.statementLoc['expression']
				&& ident.statementLoc['expression'] instanceof AssignmentExpression
				&& (ident.statementLoc['expression'].left as Identifier).name === ident.name
			));
			if (!!id) {
				infos.report.report(new ErrorMessage(header, reportType, `Override import`, id.loc));
			}
		})
	}
}

/* Conclude on the usage of import keywords */
const usedImportConclude = function(infos: Infos) {
	if (importsList.length === 0) {
		return;
	}
	importsList.filter(i => i.used === false).forEach((imp) => {
		infos.report.report(new ErrorMessage(header, reportType, `This import is unused.`, imp.node.loc));
	});
}

exports.handler = async function (event: any, context: any) {
    if (!event['allBlocks']) {
        return {
            statusCode: 500,
            headers: 'usedImport',
            infos: {
                report: { internal: 'graph not found.' }
            }
        };
    }

    const infos = regenDict(new Infos(event));

    try {
				usedImportReferencing(infos);
				UsedImportUsage(infos);
				usedImportConclude(infos);
				usedImportOverride(infos);

        return {
            statusCode: 200,
            headers: 'usedImport',
            infos: serializeDict(infos),
        };
    } catch (e) {
        console.log('[usedImport] ', e);
        infos.report.report(new ErrorMessage('internal', 'internat', `[usedImport] ` + e));
        return {
            statusCode: 500,
            headers: 'usedImport',
            infos: serializeDict(infos),
        }
    }
}
