import { Infos } from "../../../common/inspector/Infos";
import '../../../common/graph/instanciateAll';
import { regenDict, serializeDict } from "../../../common/inspector/RegenFactory";
import { ErrorMessage } from '../../../common/inspector/Report';
import { Identifier, AssignmentExpression, UpdateExpression } from "../../../common/graph/Expressions";
import { Variable } from "../../../common/graph/Variables";
import { Loc } from "../../../common/graph/Block";

/** @TODO faire la création d'erreur pour l'utilisation des variables */
/** @TODO faire une fonction qui vérifie les overwrites des func pointers qui sont défini dans des AssignmentExpression de declaration */

const checkVariablesUse = function(infos: Infos) {
    if (infos['allVariables'] instanceof Array) {
        infos['allVariables'].forEach((v: Variable) => {
            if (v.loc.compare(Loc.noLoc()) === Loc.comp.EQ) {
                return;
            }
            if (!v.isRead) {
                infos.report.report(new ErrorMessage('variable', 'unused', `variable ${v.name} is never used`, v.loc, true));
            } else if (!v.isArgument && !v.isWrite) {
                infos.report.report(new ErrorMessage('variable', 'unused', `variable ${v.name} is never init`, v.loc, true));
            }
        });
    }
}

const constAssignementStatement = function(infos: Infos) {
    if (infos['allAssignmentExpressions'] instanceof Array) {
        infos['allAssignmentExpressions'].forEach((assignmentExpression: AssignmentExpression) => {
            if (assignmentExpression.left.type === "Identifier"
            && !assignmentExpression["declaration"]
            && (assignmentExpression.left as Identifier).variable
            && (assignmentExpression.left as Identifier).variable.isConstKind()) {
                infos.report.report(new ErrorMessage(
                    'vuManager', 'modified const',
                    `Identifer ${assignmentExpression.left["name"]} is a const and shouldn't be modified`,
                    assignmentExpression.loc
                ));
            }
        })
    }
}

const constUpdateStatement = function(infos: Infos) {
    if (infos['allUpdateExpressions'] instanceof Array) {
        infos['allUpdateExpressions'].forEach((updateExpression: UpdateExpression) => {
            if (updateExpression.argument.type === "Identifier"
            && !updateExpression["declaration"]
            && (updateExpression.argument as Identifier).variable
            && (updateExpression.argument as Identifier).variable.isConstKind()) {
                infos.report.report(new ErrorMessage(
                    'vuManager', 'modified const',
                    `Identifer ${updateExpression.argument["name"]} is a const and shouldn't be modified`,
                    updateExpression.loc
                ));
            }
        })
    }
}

exports.handler = async function (event: any, context: any) {
    if (!(event['allBlocks'] instanceof Array)) {
        return {
            statusCode: 500,
            headers: 'varlog',
            infos: {
                report: { internal: 'graph not found.' }
            }
        };
    }

    const infos = regenDict(new Infos(event));

    try {
        checkVariablesUse(infos);
        constAssignementStatement(infos);
        constUpdateStatement(infos);

        return {
            statusCode: 200,
            headers: 'varlog',
            infos: serializeDict(infos),
        };
    } catch (e) {
        console.log('[varlog] ', e);
        infos.report.report(new ErrorMessage('internal', 'internal', `[varlog] ` + e));
        return {
            statusCode: 500,
            headers: 'varlog',
            infos: serializeDict(infos),
        }
    }
}
