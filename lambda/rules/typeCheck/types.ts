import { Infos } from "../../../common/inspector/Infos";
import { ErrorMessage } from '../../../common/inspector/Report';
import { VarType, Types, Type, ComplexType, compareType, mergeTypes } from '../../../common/graph/Variables';
import { Expression, Identifier, ObjectExpression, BinaryExpression, MemberExpression, ArrayExpression, FuncPointerLiteral, AssignmentPattern } from '../../../common/graph/Expressions';

/** @WARNING the _currentBlock must be set BEFORE the call of this function */
export function computeType(infos: Infos, expr: Expression): Types {
    if (expr.staticTypes) {
        return expr.staticTypes;
    }
    switch (expr.type) {
        case "NumericLiteral": {
            return expr.staticTypes = [new Type(VarType.NUMBER)];
        }
        case "StringLiteral": {
            return expr.staticTypes = [new Type(VarType.STRING)];
        }
        case "BooleanLiteral": {
            return expr.staticTypes = [new Type(VarType.BOOLEAN)];
        }
        case "NullLiteral": {
            return expr.staticTypes = [new Type(VarType.NULL)];
        }
        case 'FuncPointerLiteral': {
            const funcPointer = expr as FuncPointerLiteral;
            if (!funcPointer.func) {
                return [new Type(VarType.NULL)];
            }
            const paramsTypes = funcPointer.func.params.map(param => new ComplexType("", computeType(infos, param)));

            if (funcPointer.func.returns.length > 0) {
                const retTypes = funcPointer.func.returns.map(ret => {
                    if (ret.argument) {
                        return computeType(infos, ret.argument);
                    } else {
                        return [new Type(VarType.UNDEFINED)];
                    }
                });
                paramsTypes.unshift(new ComplexType("return", mergeTypes(retTypes)));
            } else {
                paramsTypes.unshift(new ComplexType("return", [new Type(VarType.VOID)]));
            }
            return funcPointer.staticTypes = [new Type(VarType.FUNCTION, paramsTypes)];
        }
        case 'ObjectExpression': {
            const objExpr = expr as ObjectExpression;
            /** recursif sur expr.properties */
            return objExpr.staticTypes = [new Type(VarType.OBJECT, objExpr.properties.map(
                p => new ComplexType(p.key, computeType(infos, p.value))
            ))];
        }
        case 'Identifier': {
            const id = expr as Identifier;
            if (id.notaVar) {
                return id.staticTypes = [new Type(VarType.ANY)];
            }
            if (id.name === 'undefined') {
                return id.staticTypes = [new Type(VarType.UNDEFINED)];
            }
            if (id.name === 'NaN') {
                return id.staticTypes = [new Type(VarType.NAN)];
            }
            if (id.variable) {
                if (!id.variable.types) {
                    if (id.variable.isArgument) {
                        id.variable.types = [new Type(VarType.ANY)];
                    } else {
                        // there must be an UNDEFINED but does not work well because lake of implementations
                        id.variable.types = [new Type(VarType.UNDEFINED)];
                    }
                }
                return id.staticTypes = id.variable.types;
            }
            infos.report.report(new ErrorMessage('typeCheck', 'reference error', `${id.name} is not defined`, id.loc));
            return id.staticTypes = [new Type(VarType.ANY)];
        }
        case 'MemberExpression': {
            const memberExpr = expr as MemberExpression;
            if (memberExpr.computed === true) {
                return memberExpr.staticTypes = [new Type()];
            }
            const prop = memberExpr.property as Identifier;
            return memberExpr.staticTypes = computeType(infos, memberExpr.object).map((t: Type) => {
                switch (t.type) {
                    case VarType.ANY: {
                        return [new Type(VarType.ANY)];
                    }
                    case VarType.OBJECT: {
                        let v = t.complexType.find(v => v.id === prop.name);
                        return v ? v.types : [new Type(VarType.UNDEFINED)];
                    }
                    case VarType.NULL: {
                        infos.report.report(new ErrorMessage('typeCheck', 'access error', `Cannot read property '${prop.name}' of null`, memberExpr.loc));
                        return [new Type()];
                    }
                    case VarType.UNDEFINED: {
                        infos.report.report(new ErrorMessage('typeCheck', 'access error', `Cannot read property '${prop.name}' of undefined`, memberExpr.loc));
                        return [new Type()];
                    }
                    case VarType.ERROR: {
                        return [new Type()];
                    }
                    default: {
                        return [new Type(VarType.UNDEFINED)];
                    }
                }
            }).reduce((a,b) => a.concat(b.filter(bb => !a.find(aa => compareType(aa, bb)))));
        }
        case 'ArrayExpression': {
            const arrayExpr = expr as ArrayExpression;
            const elementTypes = arrayExpr.elements.map(e => computeType(infos, e));
            const mergedTypes = mergeTypes(elementTypes);
            return arrayExpr.staticTypes = [new Type(VarType.ARRAY, [new ComplexType("", mergedTypes)])];
        }
        case 'BinaryExpression': {
            const binExpr = expr as BinaryExpression;
            for (let rt of __return_types__) {
                if (rt.operators.find((x: string) => x === binExpr.operator)) {
                    return typeof rt.ret === 'function'
                        ? binExpr.staticTypes = rt.ret(computeType(infos, binExpr.left), computeType(infos, binExpr.right))
                        : binExpr.staticTypes = rt.ret;
                }
            }
        }
        case 'AssignmentPattern': {
            const assignmentPattern = expr as AssignmentPattern;
            const types = computeType(infos, assignmentPattern.right);
            if (assignmentPattern.left.type === 'Identifier') {
                const id = assignmentPattern.left as Identifier;
                if (id.variable) {
                    id.variable.types = types;
                }
            }
            return assignmentPattern.left.staticTypes = types;
        }
        default: {
            // unsuported
            console.log(`unsuported type computation for type ${expr.type}`);
            break;
        }
    }

    return expr.staticTypes = [new Type()];
}

const __return_types__: any[] = [
    { operators: ['===', '==', '!=', '!==', '>=', '<=', '>', '<'],
      ret: VarType.BOOLEAN
    },
    { operators: ['+'],
      ret: (ts1: Types, ts2: Types) => {
        const stypes = [VarType.NUMBER, VarType.BOOLEAN, VarType.NULL, VarType.UNDEFINED];
        let ret = new Set<VarType>();
        
        ts1.forEach(t1 => ts2.forEach(t2 => {
            if (stypes.find(t => t === t1.type) && stypes.find(t => t === t2.type)) {
                ret.add(VarType.NUMBER);
            } else if (t1.type === VarType.NAN || t2.type === VarType.NAN) {
                ret.add(VarType.NAN);
            } else {
                ret.add(VarType.STRING);
            }
        }));
        return [...ret].map(t => new Type(t));
    }},
    { operators: ['-'],
      ret: (ts1: Types, ts2: Types) => {
        const stypes = [VarType.NUMBER, VarType.BOOLEAN, VarType.NULL];
        const stypes2 = [VarType.NUMBER, VarType.BOOLEAN, VarType.NULL, VarType.STRING];
        let ret = new Set<VarType>();

        ts1.forEach(t1 => ts2.forEach(t2 => {
            if (stypes.find((t) => t === t1.type) && stypes.find((t) => t === t2.type)) {
                ret.add(VarType.NUMBER);
            } else if (stypes2.find((t) => t === t1.type) && stypes2.find((t) => t === t2.type)) {
                ret.add(VarType.NUMBER)
                ret.add(VarType.NAN);
            } else {
                ret.add(VarType.NAN);
            }
        }));
        return [...ret].map(t => new Type(t));
    }},
];
