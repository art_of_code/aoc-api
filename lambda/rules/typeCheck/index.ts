import { Infos } from "../../../common/inspector/Infos";
import '../../../common/graph/instanciateAll';
import { regenDict, serializeDict } from "../../../common/inspector/RegenFactory";
import { ErrorMessage } from '../../../common/inspector/Report';
import { computeType } from './types';
import { VarType, Type, Variable, compareType } from '../../../common/graph/Variables';
import { Expression, Identifier, BinaryExpression, AssignmentExpression, AssignmentPattern } from "../../../common/graph/Expressions";

const log = function(...s: any) {
    console.log(...s);
}

const typeCheckAssignmentExpressions = function(infos: Infos) {
    if (infos['allAssignmentExpressions'] instanceof Array) {
        infos['allAssignmentExpressions'].forEach((expr: AssignmentExpression) => {
            if (expr.left.type !== 'Identifier') {
                /** @TODO
                 * 1. vérifier le type dans le cas normal
                 * 2. vérifier et set le type dynamiquement dans le mode runtime
                 */
                log("AssignementExpression ignored.");
                return;
            }
            const variable = (expr.left as Identifier).variable;
            if (!variable) {
                log(`No variable was found for ${(expr.left as Identifier).name}.`);
                return;
            }
            variable.types = expr.left.staticTypes = computeType(infos, expr.right);
        });
    }
    if (infos['allAssignmentPatterns'] instanceof Array) {
        infos['allAssignmentPatterns'].forEach((expr: AssignmentPattern) => {
            if (expr.left.type !== 'Identifier') {
                /** @TODO
                 * 1. vérifier le type dans le cas normal
                 * 2. vérifier et set le type dynamiquement dans le mode runtime
                 */
                log("AssignmentPattern ignored.");
                return;
            }
            const variable = (expr.left as Identifier).variable;
            if (!variable) {
                log(`No variable was found for ${(expr.left as Identifier).name}.`);
                return;
            }
            variable.types = expr.left.staticTypes = expr.staticTypes = computeType(infos, expr.right);
        });
    }
}

const typeCheckExpressions = function(infos: Infos) {
    const expressionTypes = [
        'Identifier',
        'UnaryExpression',
        'BinaryExpression',
        'UpdateExpression',
        'AssignmentExpression',
        'LogicalExpression',
        'MemberExpression',
        'ConditionalExpression',
        'CallExpression',
        'NewExpression',
        'SequenceExpression',
        'ThisExpression',
        'ArrayExpression',
        'ObjectExpression',
        'ObjectProperty',
        'NumericLiteral',
        'BooleanLiteral',
        'StringLiteral',
        'NullLiteral',
        'TemplateLiteral',
        'RegExpLiteral',
        'FuncPointerLiteral',
        'AssignmentPattern',
    ];
    const anyType = [new Type(VarType.ANY)];
    expressionTypes.forEach(type => {
        const key = 'all'+type+'s';
        if (infos[key] instanceof Array) {
            infos[key].forEach((expr: Expression) => {
                const t = computeType(infos, expr);
                if (!t || !t.length || t[0].type === VarType.ERROR) {
                    expr.staticTypes = anyType;
                }
            });
        }
    });
}

const BinaryCompatible = {
    '===': [
        [VarType.NUMBER, VarType.NUMBER],
        [VarType.STRING, VarType.STRING],
        [VarType.BOOLEAN, VarType.BOOLEAN],
        [VarType.NULL, VarType.NULL],
        [VarType.UNDEFINED, VarType.UNDEFINED],
        [VarType.OBJECT, VarType.OBJECT, (t1: Type, t2: Type) => compareType(t1, t2)],
        [VarType.ARRAY, VarType.ARRAY, (t1: Type, t2: Type) => compareType(t1, t2)],
        [VarType.FUNCTION, VarType.FUNCTION, (t1: Type, t2: Type) => compareType(t1, t2)],
    ],
    '==': [
        [VarType.NUMBER, VarType.NUMBER],
        [VarType.NUMBER, VarType.STRING],
        [VarType.NUMBER, VarType.BOOLEAN],
        [VarType.STRING, VarType.NUMBER],
        [VarType.STRING, VarType.STRING],
        [VarType.STRING, VarType.BOOLEAN],
        [VarType.STRING, VarType.FUNCTION],
        [VarType.BOOLEAN, VarType.NUMBER],
        [VarType.BOOLEAN, VarType.STRING],
        [VarType.BOOLEAN, VarType.BOOLEAN],
        [VarType.NULL, VarType.NULL],
        [VarType.FUNCTION, VarType.STRING],
        [VarType.UNDEFINED, VarType.UNDEFINED],
        [VarType.OBJECT, VarType.OBJECT, (t1: Type, t2: Type) => compareType(t1, t2)],
        [VarType.ARRAY, VarType.ARRAY, (t1: Type, t2: Type) => compareType(t1, t2)],
        [VarType.FUNCTION, VarType.FUNCTION, (t1: Type, t2: Type) => compareType(t1, t2)],
    ],
};

const typeCheckBinary = function(infos: Infos) {
    if (infos['allBinaryExpressions'] instanceof Array) {
        infos['allBinaryExpressions'].forEach((expr: BinaryExpression) => {
            const a = BinaryCompatible[expr.operator];
            if (!a) {
                log(`No BinaryCompatible was found for operator ${expr.operator}`);
                // infos.report.report(new ErrorMessage("typeCheck", 'operand error', `operator ${block.operator} is unchecked`, block.loc));
                return;
            }
            const lhs = computeType(infos, expr.left),
                rhs = computeType(infos, expr.right);
            if (lhs.length === 0 && lhs[0].type === VarType.ERROR) {
                log(`left operand of operator ${expr.operator} is unchecked`);
                infos.report.report(new ErrorMessage("typeCheck", 'operand error', `left operand of operator ${expr.operator} is unchecked`, expr.loc));
                return;
            }
            if (rhs.length === 0 && rhs[0].type === VarType.ERROR) {
                log(`right operand of operator ${expr.operator} is unchecked`);
                infos.report.report(new ErrorMessage("typeCheck", 'operand error', `right operand of operator ${expr.operator} is unchecked`, expr.loc));
                return;
            }
            if (lhs.find(t => t.type === VarType.ANY) || rhs.find(t => t.type === VarType.ANY)) {
                // there is an any
                return;
            }
            const res = lhs
            .map(l => rhs.map(r => {
                const re = a.find(e => e[0] == l.type && e[1] == r.type);
                return re && (re.length === 2 || re[2](l, r));
            }))
            .reduce((a, b) => a.concat(b))
            .reduce((a, b) => a || b);

            if (!res) {
                log(`operator ${expr.operator} is always false`);
                infos.report.report(new ErrorMessage("typeCheck", 'operand error', `operator ${expr.operator} is always false`, expr.loc));
            } else {
                // infos.report.report(new ErrorMessage("typeCheck", 'operand error', `operator ${expr.operator} no error`, expr.loc));
            }
        });
    }
}

/** @TODO */
// class rule_TypeCheck_Function extends Verifier {
//     constructor() {
//         super();
//         this.whiteList = ['FunctionDeclaration', 'FunctionExpression', 'ArrowFunctionExpression'];
//         this.endApply = function(block: any, infos: Infos) {
//             if (!block.id) return;
//             // let f = Scope.findFunc_loc(block, infos);
//             // console.log('=======================');
//             // console.log('TypeCheck Function:');
//             // console.log(f);
//             // console.log(`current Scope: ${block.id.name}`);
//             let scope = infos.setCurrentScope(block);
//             // console.log(scope);
//             let v = scope.findVar(block.id.name, infos, 'global');
//             if (v) {
//                 // get all returns from internal var
//                 let ArgsTypes = block.params.map(p => {
                    
//                     if (p.type === 'Identifier') {
//                         let vv = scope.findVar(p.name, infos, 'global');
//                         let vt = new _Var(p.name, [new Type(VarType.ANY)], p.loc);
//                         if (p.optional) {
//                             vt['optional'] = true;
//                         }
//                         Indexer.index(infos, vt);
//                         vv.value['varTypes'] = [vt];
//                         return vt;
//                     } else if (p.type === 'AssignmentPattern') {
//                         let vv = scope.findVar(p.left.name, infos, 'global');
//                         let vt = new _Var(p.name, computeType(p.right, infos), p.left.loc);
//                         vt.types.push(new Type(VarType.ANY));
//                         if (p.left.optional) {
//                             vt['optional'] = true;
//                         }
//                         Indexer.index(infos, vt);
//                         vv.value['varTypes'] = [vt];
//                         return vt;
//                     }
//                 });

//                 let vt = new _Var(block.id.name, [new Type(VarType.FUNCTION, ArgsTypes)], block.loc);
//                 Indexer.index(infos, vt);
//                 v.value['varTypes'] = [vt];
//                 // console.log('Var: ');
//                 // console.log(v.value);
//             } else {
//                 infos.report.report(new ErrorMessage("internal", "internal", `rule_VariableDeclarator: identifier ${block.id.name} not found`, block.loc));
//             }
//         }
//     }
// }

/** @TODO */
// class rule_TypeCheck_Return extends Verifier {
//     constructor() {
//         super();
//         this.whiteList = ["ReturnStatement"];
//         this.endApply = function(block: any, infos: Infos) {
//             // let f = Scope.findFunc_loc(block, infos);
//             // console.log('=======================');
//             // console.log('TypeCheck Function:');
//             // console.log(f);
//             // console.log('--- return:');
//             // let scope = Scope.findScope_loc(block, infos, 'strong');

//             // console.log(scope);
//         }
//     }
// }

const setAnyToUndefinedVariables = function(infos: Infos) {
    if (infos['allVariables'] instanceof Array) {
        infos['allVariables'].forEach((v: Variable) => {
            if (!v.types || !v.types.length || v.types[0].type == VarType.ERROR) {
                v.types = [new Type(VarType.ANY)];
            }
        });
    }
}

const dumpTypes = function(infos: Infos) {
    if (infos['allVariables'] instanceof Array) {
        infos['allVariables'].forEach((v: Variable) => {
            v.dump();
        });
    }
    // if (infos['allIdentifiers'] instanceof Array) {
    //     infos['allIdentifiers'].forEach((i: Identifier) => {
    //         console.log(typesToString(i.staticTypes));
    //     });
    // }
}

exports.handler = async function(event: any, context: any) {
    if (!event['allBlocks']) {
        return {
            statusCode: 500,
            headers: "typeCheck",
            infos: {
                report: { internal: "graph not found." }
            }
        };
    }

    const infos = regenDict(new Infos(event));

    try {

        /** inspects */
        typeCheckAssignmentExpressions(infos);
        typeCheckBinary(infos);
        typeCheckExpressions(infos);
        setAnyToUndefinedVariables(infos);

        // dumpTypes(infos);

        return {
            statusCode: 200,
            headers: "typeCheck",
            infos: serializeDict(infos),
        };
    } catch (e) {
        console.log(e);
        return {
            statusCode: 500,
            headers: "typeCheck",
            infos: {
                report: { error: e }
            }
        }
    }
}
