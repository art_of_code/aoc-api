import { serializeDict } from "../../../common/inspector/RegenFactory";
import { generateGraph } from "../../../common/graph/GraphGenerator";

exports.handler = async function (event: any, context: any) {
    if (!event.ast) {
        return {
            "statusCode": 500,
            "headers": "genGraph",
            "infos": "ast not found."
        };
    }

    try {
        const infos = generateGraph(event.ast);
        delete infos['ast'];
        return {
            "statusCode": 200,
            "headers": "genGraph",
            // "infos": {report: { graph:[serializeDict(generateGraph(event.ast))]}},
            "infos": serializeDict(infos),
        };
    } catch (e) {
        console.log(e);
        return {
            "statusCode": 500,
            "headers": "genGraph",
            "infos": e.message ? e.message : e
        };
    }
}
