// From https://nodejs.org/api/globals.html and https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects

import { Type, VarType } from '../../../common/graph/Variables'

// Default global objects
export const defaultObjects = [
    // Node
    {
        name: "__dirname",
        type: new Type(VarType.ANY),
    },
    {
        name: "__filename",
        type: new Type(VarType.ANY),
    },
    {
        name: "console",
        type: new Type(VarType.ANY),
    },
    {
        name: "exports",
        type: new Type(VarType.ANY),
    },
    {
        name: "global",
        type: new Type(VarType.ANY),
    },
    {
        name: "module",
        type: new Type(VarType.ANY),
    },
    {
        name: "process",
        type: new Type(VarType.ANY),
    },
    // Classic JS
    {
        name: "arguments",
        type: new Type(VarType.ANY),
    },
    {
        name: "generator",
        type: new Type(VarType.ANY),
    },
];

// Default global classes
export const defaultClasses = [
    // Node
    {
        name: "Buffer",
        type: new Type(VarType.ANY),
    },
    {
        name: "TextDecoder",
        type: new Type(VarType.ANY),
    },
    {
        name: "TextEncoder",
        type: new Type(VarType.ANY),
    },
    {
        name: "URL",
        type: new Type(VarType.ANY),
    },
    {
        name: "URLSearchParams",
        type: new Type(VarType.ANY),
    },
    {
        name: "WebAssembly",
        type: new Type(VarType.ANY),
    },
    // Classic JS
    {
        name: "Object",
        type: new Type(VarType.ANY),
    },
    {
        name: "Function",
        type: new Type(VarType.ANY),
    },
    {
        name: "Boolean",
        type: new Type(VarType.ANY),
    },
    {
        name: "Symbol",
        type: new Type(VarType.ANY),
    },
    // Error objects
    {
        name: "Error",
        type: new Type(VarType.ANY),
    },
    {
        name: "AggregateError",
        type: new Type(VarType.ANY),
    },
    {
        name: "EvalError",
        type: new Type(VarType.ANY),
    },
    {
        name: "InternalError",
        type: new Type(VarType.ANY),
    },
    {
        name: "RangeError",
        type: new Type(VarType.ANY),
    },
    {
        name: "ReferenceError",
        type: new Type(VarType.ANY),
    },
    {
        name: "SyntaxError",
        type: new Type(VarType.ANY),
    },
    {
        name: "TypeError",
        type: new Type(VarType.ANY),
    },
    {
        name: "URIError",
        type: new Type(VarType.ANY),
    },
    // Numbers and dates
    {
        name: "Number",
        type: new Type(VarType.ANY),
    },
    {
        name: "BigInt",
        type: new Type(VarType.ANY),
    },
    {
        name: "Math",
        type: new Type(VarType.ANY),
    },
    {
        name: "Date",
        type: new Type(VarType.ANY),
    },
    // Text processing
    {
        name: "String",
        type: new Type(VarType.ANY),
    },
    {
        name: "RegExp",
        type: new Type(VarType.ANY),
    },
    // Indexed collections
    {
        name: "Array",
        type: new Type(VarType.ANY),
    },
    {
        name: "Int8Array",
        type: new Type(VarType.ANY),
    },
    {
        name: "Uint8Array",
        type: new Type(VarType.ANY),
    },
    {
        name: "Uint8ClampedArray",
        type: new Type(VarType.ANY),
    },
    {
        name: "Int16Array",
        type: new Type(VarType.ANY),
    },
    {
        name: "Uint16Array",
        type: new Type(VarType.ANY),
    },
    {
        name: "Int32Array",
        type: new Type(VarType.ANY),
    },
    {
        name: "Uint32Array",
        type: new Type(VarType.ANY),
    },
    {
        name: "Float32Array",
        type: new Type(VarType.ANY),
    },
    {
        name: "Float64Array",
        type: new Type(VarType.ANY),
    },
    {
        name: "BigInt64Array",
        type: new Type(VarType.ANY),
    },
    {
        name: "BigUint64Array",
        type: new Type(VarType.ANY),
    },
    // Keyed collections
    {
        name: "Map",
        type: new Type(VarType.ANY),
    },
    {
        name: "Set",
        type: new Type(VarType.ANY),
    },
    {
        name: "WeakMap",
        type: new Type(VarType.ANY),
    },
    {
        name: "WeakSet",
        type: new Type(VarType.ANY),
    },
    // Structured
    {
        name: "ArrayBuffer",
        type: new Type(VarType.ANY),
    },
    {
        name: "SharedArrayBuffer",
        type: new Type(VarType.ANY),
    },
    {
        name: "Atomics",
        type: new Type(VarType.ANY),
    },
    {
        name: "DataView",
        type: new Type(VarType.ANY),
    },
    {
        name: "JSON",
        type: new Type(VarType.ANY),
    },
    // Control abstraction objects
    {
        name: "Promise",
        type: new Type(VarType.ANY),
    },
    {
        name: "Generator",
        type: new Type(VarType.ANY),
    },
    {
        name: "GeneratorFunction",
        type: new Type(VarType.ANY),
    },
    {
        name: "AsyncFunction",
        type: new Type(VarType.ANY),
    },
    // Reflection
    {
        name: "Reflect",
        type: new Type(VarType.ANY),
    },
    {
        name: "Proxy",
        type: new Type(VarType.ANY),
    },
    // Internationalization
    {
        name: "Intl",
        type: new Type(VarType.ANY),
    },
];

// Default global functions
export const defaultFunctions = [
    // Node
    {
        name: "clearImmediate",
        type: new Type(VarType.ANY),
    },
    {
        name: "clearInterval",
        type: new Type(VarType.ANY),
    },
    {
        name: "clearTimeout",
        type: new Type(VarType.ANY),
    },
    {
        name: "queueMicrotask",
        type: new Type(VarType.ANY),
    },
    {
        name: "require",
        type: new Type(VarType.ANY),
    },
    {
        name: "setImmediate",
        type: new Type(VarType.ANY),
    },
    {
        name: "setInterval",
        type: new Type(VarType.ANY),
    },
    {
        name: "setTimeout",
        type: new Type(VarType.ANY),
    },
    // Classic JS
    {
        name: "eval",
        type: new Type(VarType.ANY),
    },
    {
        name: "uneval",
        type: new Type(VarType.ANY),
    },
    {
        name: "isFinite",
        type: new Type(VarType.ANY),
    },
    {
        name: "isNaN",
        type: new Type(VarType.ANY),
    },
    {
        name: "parseFloat",
        type: new Type(VarType.ANY),
    },
    {
        name: "parseInt",
        type: new Type(VarType.ANY),
    },
    {
        name: "decodeURI",
        type: new Type(VarType.ANY),
    },
    {
        name: "decodeURIComponent",
        type: new Type(VarType.ANY),
    },
    {
        name: "encodeURI",
        type: new Type(VarType.ANY),
    },
    {
        name: "encodeURIComponent",
        type: new Type(VarType.ANY),
    },
    {
        name: "Deprecated",
        type: new Type(VarType.ANY),
    },
    {
        name: "escape",
        type: new Type(VarType.ANY),
    },
    {
        name: "unescape",
        type: new Type(VarType.ANY),
    },
];

// Default global properties
export const defaultProperties = [
    // Classic JS
    {
        name: "Infinity",
        type: new Type(VarType.ANY),
    },
    {
        name: "NaN",
        type: new Type(VarType.ANY),
    },
    {
        name: "undefined",
        type: new Type(VarType.ANY),
    },
    {
        name: "globalThis",
        type: new Type(VarType.ANY),
    },
];
