import { Infos } from '../../../common/inspector/Infos';
import '../../../common/graph/instanciateAll';
import { regenDict, serializeDict } from "../../../common/inspector/RegenFactory";
import { inspectExpression } from "../../../common/graph/Inspector";
import { ErrorMessage } from '../../../common/inspector/Report';

import * as defaultGlobals from './default';
import { Block, Loc, Func } from '../../../common/graph/Block';
import { Identifier } from '../../../common/graph/Expressions';
import { Variable, Type } from "../../../common/graph/Variables";

import {
    BreakStatement,
    ContinueStatement,
    ExpressionStatement,
    ReturnStatement,
    ThrowStatement,
} from '../../../common/graph/Statements';

// const util = require('util');

const insertGlobals = function(infos: Infos) {
    if (infos['allBlocks'] instanceof Array) {
        infos['allBlocks'].forEach((block: Block) => {
            if (block["entrypoint"] === true) {
                const insertVar = (varInfo: {name: string, type: Type}) => {
                    const v = new Variable(infos, {name: varInfo.name, loc: Loc.noLoc()}, 'const');
                    v.types = [varInfo.type]
                    block["variables"].push(v);
                }
                defaultGlobals.defaultObjects.forEach(insertVar);
                defaultGlobals.defaultFunctions.forEach(insertVar);
                defaultGlobals.defaultProperties.forEach(insertVar);
                defaultGlobals.defaultClasses.forEach(insertVar);
            }
        })
    }
}

const inspectIdentifiers = function(infos: Infos) {
    if (infos['allIdentifiers'] instanceof Array) {
        infos['allIdentifiers'].forEach((identifier: Identifier) => {
            if (identifier.notaVar) {
                return;
            }
            const scope = identifier.getScope();
            if (!scope) {
                return;
            }
            let variable = scope.getVariable(identifier.name, 'strong');
            if (!variable) {
                variable = scope.getVariable(identifier.name, 'global');
                if (!variable) {
                    return;
                }
                variable['isUnsecurelyUsed'] = true;
            }
            identifier.variable = variable;
        });
    }

    const verifyReadWrite = (infos: Infos, expr: Identifier) => {
        if (expr.type === 'Identifier' && expr.variable) {
            if (!infos._expressionInfos.notAVar) {
                if (infos._expressionInfos.writeMode) {
                    expr.variable.isWrite = true;
                    expr.variable.writeExpr.push(expr);
                }
                if (infos._expressionInfos.readMode) {
                    expr.variable.isRead = true;
                    expr.variable.readExpr.push(expr);
                }
            }
        }
    };

    if (infos['allExpressionStatements'] instanceof Array) {
        infos['allExpressionStatements'].forEach((expressionStatement: ExpressionStatement) => {
            inspectExpression(infos, expressionStatement.expression, verifyReadWrite);
        });
    }

    if (infos['allReturnStatements'] instanceof Array) {
        infos['allReturnStatements'].forEach((returnStatement: ReturnStatement) => {
            if (returnStatement.argument) {
                infos._expressionInfos.readMode = true;
                inspectExpression(infos, returnStatement.argument, verifyReadWrite);
                infos._expressionInfos.readMode = false;
            }
        });
    }

    if (infos['allBlocks'] instanceof Array) {
        infos['allBlocks'].forEach((block: Block) => {
            if (block.type.includes('IfBlock')) {
                const ifBlock = block;
                infos._expressionInfos.readMode = true;
                inspectExpression(infos, ifBlock.links[0].condition.condition, verifyReadWrite);
                infos._expressionInfos.readMode = false;    
            } else if (block.type.includes('SwitchBlock')) {
                const switchBlock = block;
                infos._expressionInfos.readMode = true;
                inspectExpression(infos, switchBlock['discriminant'], verifyReadWrite);
                infos._expressionInfos.readMode = false;    
            } else if (block.type.includes('WhileBlock')) {
                const whileBlock = block;
                infos._expressionInfos.readMode = true;
                inspectExpression(infos, whileBlock.links[0].condition.condition, verifyReadWrite);
                infos._expressionInfos.readMode = false;
            } else if (block.type.includes('DoWhileBlock')) {
                const doWhileBlock = block;
                infos._expressionInfos.readMode = true;
                inspectExpression(infos, doWhileBlock.links[1].condition.condition, verifyReadWrite);
                infos._expressionInfos.readMode = false;
            } else if (block.type.includes('ForBlock')) {
                const forBlock = block;
                if (forBlock['init']) {
                    inspectExpression(infos, forBlock['init'], verifyReadWrite);
                }
                if (forBlock['update']) {
                    inspectExpression(infos, forBlock['update'], verifyReadWrite);
                }
                if (forBlock.links[1].condition) {
                    infos._expressionInfos.readMode = true;
                    inspectExpression(infos, forBlock.links[1].condition.condition, verifyReadWrite);
                    infos._expressionInfos.readMode = false;
                }
            } else if (block.type.includes('ForInBlock')) {
                const forInBlock = block;
                infos._expressionInfos.readMode = true;
                inspectExpression(infos, forInBlock['right'], verifyReadWrite);
                infos._expressionInfos.readMode = false;
            } else if (block.type.includes('ForOfBlock')) {
                const forOfBlock = block;
                infos._expressionInfos.readMode = true;
                inspectExpression(infos, forOfBlock['right'], verifyReadWrite);
                infos._expressionInfos.readMode = false;
            }
        });
    }

    if (infos['allThrowStatements'] instanceof Array) {
        infos['allThrowStatements'].forEach((throwStatement: ThrowStatement) => {
            infos._expressionInfos.readMode = true;
            inspectExpression(infos, throwStatement.argument, verifyReadWrite);
            infos._expressionInfos.readMode = false;    
        });
    }
}

const inspectBreaksAndContinues = function(infos: Infos) {
    if (infos["allBreakStatements"] instanceof Array) {
        infos["allBreakStatements"].forEach((statement: BreakStatement) => {
            /** @TODO vérifier si le label est TOUJOURS vérifié et validé par le parser
             * sinon faire un check si pas trouvé
             */
            let block = statement.blockLoc.scope;

            if (statement.label) {
                do {
                    if (statement.label === block['label']) {
                        block['broken'] = true;
                        statement.goto = block;
                        return;
                    }
                    block = block.prevLinks[0].from.scope;
                } while (block.prevLinks.length > 0);
            }
            else {
                const breakableBlockTypes = ['SwitchBlock', 'WhileBlock', 'DoWhileBlock', 'ForBlock', 'ForInStatement', 'ForOfStatement'];
                do {
                    if (breakableBlockTypes.indexOf(block.type)) {
                        block['continued'] = true;
                        statement.goto = block;
                        return;
                    }
                    block = block.prevLinks[0].from.scope;;
                } while (block.prevLinks.length > 0);
            }
        });
    }

    if (infos["allContinueStatements"] instanceof Array) {
        infos["allContinueStatements"].forEach((statement: ContinueStatement) => {
            let block = statement.blockLoc.scope;

            const continueableBlockTypes = ['WhileBlock', 'DoWhileBlock', 'ForBlock', 'ForInStatement', 'ForOfStatement'];
            do {
                if (continueableBlockTypes.indexOf(block.type)
                && (!statement.label || (statement.label && statement.label === block['label']))) {
                    block['continued'] = true;
                    statement.goto = block;
                    return;
                }
                block = block.prevLinks[0].from.scope;;
            } while (block.prevLinks.length > 0);
        });
    }
}

const inspectAndStoreReturns = function(infos: Infos) {
    if (infos['allReturnStatements'] instanceof Array) {
        infos['allReturnStatements'].forEach((statement: ReturnStatement) => {
            const func = Func.getFromLoc(infos, statement.loc);
            if (!func) {
                infos.report.report(new ErrorMessage('internal', 'internal', `could not find the function that contains the returnStatement`, statement.loc));
                return;
            }
            func.returns.push(statement);
        });
    }
}

const checkFunctionImplicitReturn = function(infos: Infos) {
    const canFunctionsHasImplicitReturn = function(func: Func): boolean {
        const visitedBlocks: Block[] = []; // used to break cycles
        const canBlockReachsEnd = (block: Block) => {
            if (visitedBlocks.find(b => b === block)) {
                // break the cycle
                return false;
            }
            visitedBlocks.push(block);
            if (block.statements.find(statement => statement.type === 'ReturnStatement')) {
                // if the block contains a returns, break the recursion for this branch.
                return false;
            }
            if (block.links.length === 0) {
                // if no more link, this is the end of the function.
                return true;
            }
            // return true if at least one branch is true
            return block.links
                .map(link => canBlockReachsEnd(link.to))
                .some(b => b);
        }
        return canBlockReachsEnd(func.body);
    }
    if (infos['allFuncs'] instanceof Array) {
        infos['allFuncs'].forEach((func: Func) => {
            if (canFunctionsHasImplicitReturn(func)) {
                let block = func.body;

                const visitedBlocks: Block[] = [];
                loop:
                while (block.links.length > 0) {
                    for (const link of block.links) {
                        if (!visitedBlocks.find(b => b === link.to)) {
                            visitedBlocks.push(link.to);
                            block = link.to;
                            continue loop;
                        } else {
                            // not possible
                            break loop;
                        }
                    }
                }

                const endLoc = new Loc(infos, {loc: {start: {...func.loc.end}, end: {...func.loc.end}}});
                const statement = new ReturnStatement(infos, {loc: endLoc}, undefined, block);
                statement.implicit = true;
                func.returns.push(statement);
            }
        });
    }
}

const dumpFuncReturns = function(infos: Infos) {
    if (infos['allFuncs'] instanceof Array) {
        infos['allFuncs'].forEach((func: Func) => {
            console.log(func.returns);
        });
    }
}

exports.handler = async function (event: any, context: any) {
    if (!(event['allBlocks'] instanceof Array)) {
        return {
            statusCode: 500,
            headers: "genMetadata",
            infos: {
                report: { internal: "graph not found." }
            }
        };
    }

    const infos: Infos = regenDict(new Infos(event));

    try {
        insertGlobals(infos);
        inspectIdentifiers(infos);
        inspectBreaksAndContinues(infos);
        inspectAndStoreReturns(infos);
        checkFunctionImplicitReturn(infos);

        // dumpFuncReturns(infos);

        return {
            statusCode: 200,
            headers: "genMetadata",
            infos: serializeDict(infos),
        };
    } catch (e) {
        console.log(e);
        return {
            statusCode: 500,
            headers: "genMetadata",
            infos: {
                report: { error: e }
            }
        }
    }
}
