# Deadcode

## Description

Deadcode is given to code which will never be executed, and is then useless.

### Field of work

This rule operates on all blocks of type "BlockStatement", those blocks are generally present within a block containing instructions and expressions.
It will then inspect the block to see if a statement of type Jump is present, and if its position within the block is last.

A statement of type jump could be:

- Return statement
- Break statement
- Continue statement

It also analyses the infos reported by the tautology rule, it will flag as deadcode:

- Any extra conditional branching with an expression whose expression of test has a predictable result.
- Any code following a never ending loop, or the code inside a never executed loop.
- The case of a switch statement which will never be used.

### Report

This rule returns a report of type "deadcode", to indicate that a block contains deadcode, which can be safely removed.

## Work In Progress

The work around switch cases with tautology is botched, as we don't have all possible values for a switch discriminant.
Since expressions flagged by the tautology rule can be always true or false, it can lead to deadcode.