import { Infos } from '../../../common/inspector/Infos';
import '../../../common/graph/instanciateAll';
import { regenDict, serializeDict } from "../../../common/inspector/RegenFactory";
import { ErrorMessage } from '../../../common/inspector/Report';
import { Block, Loc, Link, Func } from '../../../common/graph/Block';
import { TautologyInfos } from '../../../common/graph/SharedInfos';
import { inspectBlock, inspectGraphPart } from '../../../common/graph/Inspector';
import { Verifier } from '../../../common/inspector/Infos';

const jumps: string[] = ['ReturnStatement', 'BreakStatement', 'ContinueStatement'];

/* TODO
  - add deadcode on function never called from Varlog
  - if an already registered deadcode is included in another, merge them
*/
let lastBlock: Block;

function manageLinks(links: Link[], usedBlocks: Block[], currentBlock: Block): boolean {
    let res: boolean = false;
    for (const link of links) {
        const block = link.to;
        if (!usedBlocks.find((elem) => block === elem)) {
            usedBlocks.push(block);
            const newUsedBlocks = Object.assign([], usedBlocks);
            if (manageLinks(block.links, newUsedBlocks, block)) {
                res = true;
                lastBlock = block;
            }
        }
    }
    if (currentBlock) {
        for (const statement of currentBlock.statements) {
            if (statement.type === "ReturnStatement") {
                res = false;
            }
        }
    }
    if (links.length === 0) {
        return true;
    }
    return res;
}

const manageControleFlow = function (infos: Infos) {
    if (infos['allFuncs'] instanceof Array) {
        infos['allFuncs'].forEach((func: Func) => {
            lastBlock = undefined;
            let blocks: Block[] = [];
            if (!manageLinks(func.body.links, blocks, null) && lastBlock && lastBlock.statements.length > 0) {
                infos.report.report(new ErrorMessage(
                    'deadcode', 'deadcode',
                    `The end of this function will never be reached.`,
                    lastBlock.statements[0].loc
                ));
            }
        });
    }
}

/** @TODO implement control flow check */
const verifyDeadCode = function (infos: Infos) {
    if (infos['allBlocks'] instanceof Array) {
        infos['allBlocks'].forEach((block: Block) => {
            const idx = block.statements.findIndex(statement => !!jumps.find(type => type === statement.type));
            if (idx !== -1 && idx !== block.statements.length - 1) {
                infos.report.report(new ErrorMessage(
                    'deadcode', 'deadcode',
                    `This statement will not be executed.`,
                    block.statements[idx + 1].loc
                ));
            }
        });
    }
}

class hasJumpStatementVerifier extends Verifier {
    constructor() {
        super();
        this.whiteList = jumps;
        this.apply = function (infos: Infos, block: any) {
            infos['_has_jump_statement_verifier_return'] = true;
        }
    }
}

const hasJumpStatement = function (infos: Infos, block: Block): boolean {
    infos['_has_jump_statement_verifier_return'] = false;
    inspectGraphPart(infos, [new hasJumpStatementVerifier], block, block.links[0].to);
    return infos['_has_jump_statement_verifier_return'];
}

const fromTautologyVerifyDeadCode = function (infos: Infos) {
    if (infos['tautologyInfos'] instanceof Array) {

        infos['tautologyInfos'].forEach((ti: TautologyInfos) => {
            if (ti.type === 'choiceBlock') {
                // let idx = ti.entrypoint;
                // while (idx) {
                //     if (idx.loc.compare(ti.predictable.loc) === Loc.comp.AFTER) {
                //         infos.report.report(new ErrorMessage('deadcode', 'deadcode', 'This branch might not be executed.', idx.loc));
                //     }
                //     if (idx.links[1]) {
                //         idx = idx.links[1].to;
                //     } else {
                //         break;
                //     }
                // }
            } else if (ti.type === 'loopBlock') {
                //TODO when we can determine the value of a predictable expression, refactor (depending on if it is true or false, the behavior is not the same)
                if (!hasJumpStatement(infos, ti.entrypoint)) {
                    infos.report.report(new ErrorMessage('deadcode', 'deadcode', 'This loop has no jump statement to exit it, and its expression of test is predictable. The loop may never end.', ti.entrypoint.loc));
                }
            }
        });
    }
}

exports.handler = async function (event: any, context: any) {
    if (!(event['allBlocks'] instanceof Array)) {
        return {
            statusCode: 500,
            headers: 'deadcode',
            infos: {
                report: { internal: 'graph not found.' }
            }
        };
    }

    const infos = regenDict(new Infos(event));

    try {
        verifyDeadCode(infos);
        fromTautologyVerifyDeadCode(infos);
        manageControleFlow(infos);

        return {
            statusCode: 200,
            headers: 'deadcode',
            infos: serializeDict(infos),
        };
    } catch (e) {
        console.log('[deadcode] ', e);
        infos.report.report(new ErrorMessage('internal', 'internal', `[deadcode] ` + e));
        return {
            statusCode: 500,
            headers: 'deadcode',
            infos: serializeDict(infos),
        }
    }

}
