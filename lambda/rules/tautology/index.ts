import { Infos } from '../../../common/inspector/Infos';
import '../../../common/graph/instanciateAll';
import { ErrorMessage } from '../../../common/inspector/Report';
import { regenDict, serializeDict } from '../../../common/inspector/RegenFactory';
import { Block, Loc } from '../../../common/graph/Block';
import { Expression, Identifier } from '../../../common/graph/Expressions';
import { TautologyInfos } from '../../../common/graph/SharedInfos';
import { kind_e } from '../../../common/graph/Variables';

/////////////////
//   COMMONS   //
/////////////////

const header = 'tautology';

const literals = [
    'RegExpLiteral',
    'NullLiteral',
    'StringLiteral',
    'BooleanLiteral',
    'NumericLiteral',
    'Literal',
];

const readWriteOperators = [
    '++', '--',
    '+=', '-=', '*=', '/=', '%=',
    '&=', '|=', '^=', '<<=', '>>=', '>>>=',
];

const assertByOperator = {
    '<': (x, y) => { return x < y },
    '>': (x, y) => { return x > y },
    '<=': (x, y) => { return x <= y },
    '>=': (x, y) => { return x >= y },
    '!=': (x, y) => { return x != y },
    '==': (x, y) => { return x == y },
    '===': (x, y) => { return x === y },
    '!==': (x, y) => { return x !== y }
};

enum IsLiteral {
    BOTH,
    LEFT,
    RIGHT,
    NONE
};

/////////////////
//  FUNCTIONS  //
/////////////////

function isLiteral(block: any): boolean {
    return !!(literals.find(l => l === block.type));
}

function checkLiteral(left: any, right: any): IsLiteral {
    const l: boolean = isLiteral(left);
    const r: boolean = isLiteral(right);

    if (l === r) {
        return l ? IsLiteral.BOTH : IsLiteral.NONE;
    } else {
        return l ? IsLiteral.LEFT : IsLiteral.RIGHT;
    }
}

/*
**
** For now, we consider that a write instruction of our current variable localized before its use in our test expression
** results in an unpredictable value.
** But that write result can be a mere literal.
** We need to see if the write expressions can have deterministic values.
**
** It also checks if the write instructions are located before our current block or not, to determiner the predictability of the variable
**
** @TODO the logic commented is from a period in which Varlog existed and is not adapted today (writeExpr holds Identifiers now)
*/
let rec = [];
let superBlock: any;
function isValuePredictable(block: Expression, infos: Infos): boolean {
    if (block.type !== "Identifier") {
        return false;
    }
    const id = block as Identifier;
    if (!id.variable) {
        return false;
    }
    const v = id.variable;

    if (v.kind === kind_e.const) {
        return true;
    }

    if (!v.isWrite) {
        return true;
    }

    // if (rec.find(o => o === v)) {
    //     return false;
    // }
    const beforeWrites = v.writeExpr.filter(w => block.loc.compare(w.loc) === Loc.comp.AFTER);
    // console.log('name', block.name);
    // console.log('beforeWrites', beforeWrites);
    if (beforeWrites.length === 0 || (superBlock && v.writeExpr.find(w => {
        // console.log(w, ' & ', superBlock, ' == ', compareLoc(w.loc, superBlock.loc));
        return w.loc.compare(superBlock.loc) === Loc.comp.IN;
    }))) {
        return false;
    }
    return false;
    // const lastWrite = beforeWrites[beforeWrites.length - 1];
    // if (readWriteOperators.find(op => lastWrite.op === op)) {
    //     return false;
    // }

    // // rec permits to break infinite loops when Identifier values depend on themselves
    // const toDelete = rec.length === 0;
    // rec.push(v);

    // const res = isExpressionPredictable(lastWrite.value, infos);
    // if (toDelete) {
    //     rec = [];
    // }
    // return res.result;
}

function isUnaryExpressionPredictable(): void {
    // TODO write this function to evaluate unary expressions
}

function isBinaryExpressionPredictable(test: any, infos: Infos): boolean {
    const flag: IsLiteral = checkLiteral(test.left, test.right);

    switch (flag) {
        case IsLiteral.BOTH:
            return true;
        case IsLiteral.LEFT:
            return isValuePredictable(test.right, infos);
        case IsLiteral.RIGHT:
            return isValuePredictable(test.left, infos);
        case IsLiteral.NONE:
            return isValuePredictable(test.left, infos) && isValuePredictable(test.right, infos);
    }
}

/*
Determine if a test expression leads to predictable results.
*/

function isExpressionPredictable(block: Expression, infos: Infos): any {
    if (!block) {
        return false;
    }
    if (literals.find(l => l === block.type)) {
        return {
            result: true,
            info: {
                expressionType: block.type,
                computableResult: true,
                expressionResult: null  // value key does not exist on Expression and has no replacement ?
            }
        };
    } else {
        //TODO if assertable, use assertByOperator
        if (block.type === 'BinaryExpression') {
            if (isBinaryExpressionPredictable(block, infos)) {// TODO get more infos from this function (type of binary expression, which member is literal / predictable)
                return {
                    result: true,
                    info: {
                        expressionType: 'BooleanLiteral',
                        computableResult: false,
                        expressionResult: null
                    }
                };
            }

        } else if (block.type === 'Identifier') {
            if (isValuePredictable(block, infos)) {
                return {
                    result: true,
                    info: {
                        expressionType: 'Identifier'
                    }
                };
            }
        } else if (block.type === 'UnaryExpression') {
            // TODO link to isUnaryExpressionPredictable()
        }
    }
    return { result: false, info: null };
}

/////////////////
//    RULES    //
/////////////////

const tautologyChoice = function(infos: Infos) {
    if (infos['allBlocks'] instanceof Array) {
        infos['allBlocks'].forEach((block:Block) => {
            if (block.type === "IfBlock") {
                let idx: Block = block;

                while (idx.type.endsWith('IfBlock')) {
                    const analysis = isExpressionPredictable(idx.links[0].condition.condition, infos);

                    if (analysis.result) {
                        infos['tautologyInfos'].push(new TautologyInfos('choiceBlock', block, idx));
                        break;
                    } else if (idx.links[1]) {
                        idx = idx.links[1].to;
                    } else {
                        break;
                    }
                }

            } else if (block.type === "SwitchBlock") {
                const analysis = isExpressionPredictable(block['discriminant'], infos);

                if (analysis.result) {
                    infos['tautologyInfos'].push(new TautologyInfos('choiceBlock', block, block['discriminant']));
                }

            } else if (block.type === "ConditionalExpression") {
                const analysis = isExpressionPredictable(block['test'], infos);

                if (analysis.result) {
                    infos['tautologyInfos'].push(new TautologyInfos('choiceBlock', block, block['test']));
                }

            }
        });
    }
};

const tautologyLoop = function(infos: Infos) {
    const loopBlockNames = ['ForBlock', 'WhileBlock', 'DoWhileBlock'];

    if (infos['allBlocks'] instanceof Array) {
        infos['allBlocks'].forEach((block:Block) => {
            if (loopBlockNames.includes(block.type)) {
                const analysis = isExpressionPredictable(block.links[0].condition.condition, infos);
                superBlock = block;
                if (analysis.result) {
                    infos['tautologyInfos'].push(new TautologyInfos('loopBlock', block, block.links[0].condition.condition));
                }
                superBlock = undefined;
            }
        });
    }
};

exports.handler = async function (event: any, context: any) {
    if (!event['allBlocks'] || !(event['allBlocks'] instanceof Array)) {
        return {
            statusCode: 500,
            headers: header,
            infos: {
                report: { internal: 'graph not found.' }
            }
        };
    }
    let infos = regenDict(new Infos(event));
    infos['tautologyInfos'] = [];

    try {

        tautologyChoice(infos);
        tautologyLoop(infos);

        return {
            statusCode: 200,
            headers: header,
            infos: serializeDict(infos),
        };
    } catch (e) {
       console.log('[tautology] ', e);
        infos.report.report(new ErrorMessage('internal', 'internat', `[tautology] ` + e));
        return {
            statusCode: 500,
            headers: header,
            infos: serializeDict(infos),
        }
    }
}