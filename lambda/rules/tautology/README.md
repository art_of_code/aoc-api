# Tautology

## Description

Is tautologic an expression which results in an always or (or always false) expression.
An expression which can never have both results possible is a problem of control flow.

It can lead to deadcode or infinite loops.

### Field of work

This rule operates on all type of block which contains a key of type "test":
-	If/Else
-	Switch (the test expression is accessible via the "discriminant" key)
-	Ternary
-	For
-	While
-	DoWhile

It then searches to evaluate the "test" block and to analyse if the expression leads to a predictable output, it could either be:

- A binary expression with predictable operands (literals, variables without modification instruction)
- An unary expression with predictable operand (literal, variable without modification instruction)
- A literal
- A variable without modification instruction

Depending on if we work with expressions of type Loop (For, While, DoWhile) or with expressions of type Choice (If/Else, Switch, Ternary), the outcome will be different:
- If we can predict a Choice expression, it either means that the expression of test is always true or always false.
- If we can predict a Loop expression, this can mean that the loop will always be true (leading to an infinite loop, the test expression is then to modify), or always false (meaning that the loop will not be executed at all).

### Report

This rule returns a report of type "predictable", to indicate that the expression of test is predictable.

It will also add a new key in the report object, named "tautologyInfos", which points to the faulty expression, and the block of origin (if it is from an IfElseStatement,
it will be "loopBlock" for instance).

## Work in Progress

This rule needs extra context and information about Functions (especially about the return type and possible values),
Identifiers (especially about the possible values) in order to have a greater impact on the analysis.