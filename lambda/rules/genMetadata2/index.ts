import { Infos } from '../../../common/inspector/Infos';
import '../../../common/graph/instanciateAll';
import { regenDict, serializeDict } from "../../../common/inspector/RegenFactory";
import { inspectGraph } from "../../../common/graph/Inspector";
import { ErrorMessage } from '../../../common/inspector/Report';

const util = require('util');

exports.handler = async function (event: any, context: any, debug: boolean = false) {
    // console.log()
    if (!event['allBlocks']) {
        return {
            "statusCode": 500,
            "headers": "genMetadata",
            "infos": {
                report: { internal: "graph not found." }
            }
        };
    }
    var infos = regenDict(new Infos(event));
    // console.log(util.inspect(infos, false, null, false /* enable colors */));
    // event = regenDict(infos);
    try {
        // const rules: Verifier[] = [];
        // const infos = inspectGraph(event, rules);
        return {
            "statusCode": 200,
            "headers": "genMetadata",
            // "infos": { report: {graph: [serializeDict(infos)]}},
            "infos": serializeDict(infos),
        };

    } catch (e) {
        console.log('ERROR', e)
        return {
            "statusCode": 500,
            "headers": "genMetadata",
            "infos": {
                report: { error: e }
            }
        }
    }
}
