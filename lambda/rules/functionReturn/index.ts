import { regenDict, serializeDict } from "../../../common/inspector/RegenFactory";
import { Infos } from '../../../common/inspector/Infos';
import { ErrorMessage } from '../../../common/inspector/Report';
import { Block, Loc, Func } from '../../../common/graph/Block';
import { CallExpression, AssignmentExpression } from '../../../common/graph/Expressions';
import { ReturnStatement } from '../../../common/graph/Statements';
import { Variable } from "../../../common/graph/Variables";

class nFunc {
    hasReturn: boolean = false;
    borderEffect: boolean = false;
    func: Func

    constructor(func?: Func) {
        this.func = func;
    }

    compare(other: nFunc): boolean {
        if (this.func.body.loc.compare(other.func.body.loc) === Loc.comp.EQ && this.func.name === other.func.name) {
            return true;
        }
        return false;
    }

    compareFunc(other: Func): boolean {
        if (this.func.body.loc.compare(other.body.loc) === Loc.comp.EQ && this.func.name === other.name) {
            return true;
        }
        return false;
    }

    compareCall(other) {
        if (other.name === this.func.name) {
            return true
        }
        return false;
    }
}

let funcs: nFunc[] = [];

function getFunctionCalls(infos: Infos, func: Func) {
    const res = [];

    if (infos['allCallExpressions'] instanceof Array) {
        infos['allCallExpressions'].forEach((cal: CallExpression) => {
            if (cal.loc.compare(func.loc) === Loc.comp.IN && cal.callee.type === "Identifier") {
                const id = cal.callee;
                if (infos['allFuncs'] instanceof Array) {
                    const found = infos['allFuncs'].find((e) => e.name === id["name"]);
                    res.push(found);
                }
            }
        });
    }
    return res;
}

function getFunctionVariableDeclarations(infos: Infos, func: Func) {
    const vars = [];
    if (infos['allVariables'] instanceof Array) {
        infos['allVariables'].forEach((cal) => {
            if (cal.loc.compare(func.body.loc) === Loc.comp.IN) {
                vars.push(cal);
            }
        });
    }
    return vars;
}

function checkBorderEffect(infos: Infos, func: nFunc) {
    const vars = getFunctionVariableDeclarations(infos, func.func);
    const calls = getFunctionCalls(infos, func.func);

    for (let c of calls) {
        const found: nFunc = funcs.find((e) => e.compareFunc(c));
        if (!found) {
            const elem: nFunc = new nFunc(c)
            funcs.push(elem);
            checkBorderEffect(infos, elem);
            checkFunctionForReturn(infos, elem);
            if (elem.borderEffect) {
                func.borderEffect = true;
            }
        } else if (found.borderEffect) {
            func.borderEffect = true;
        }
    }

    if (infos['allAssignmentExpressions'] instanceof Array) {
        infos['allAssignmentExpressions'].forEach((assign: AssignmentExpression) => {
            if (assign.loc.compare(func.func.loc) === Loc.comp.IN) {
                const found = vars.find((e) => {
                    if (assign.left["variable"]) {
                        if (assign.left["variable"].loc.compare(e.loc) === Loc.comp.EQ && assign.left["variable"].name === e.name) {
                            return true;
                        }
                    } else {
                        return true;
                    }
                    return false;
                });
                if (!found) {
                    func.borderEffect = true;
                }
            }
        });
    }
}

function checkFunctionForReturn(infos: Infos, func: nFunc) {
    if (infos['allReturnStatements'] instanceof Array) {
        infos['allReturnStatements'].forEach((ret: ReturnStatement) => {
            if (ret.loc.compare(func.func.loc) === Loc.comp.IN && !ret.implicit) {
                func.hasReturn = true;
            }
        });
    }
}

function sendErrors(infos: Infos, func: nFunc, loc: Loc) {
    if (func.hasReturn) {
        infos.report.report(
            new ErrorMessage(
                "functionReturn",
                "function-return-ignored",
                `Your function return is ignored`,
                loc,
                true
            )
        );
    }
    if (!func.borderEffect) {
        infos.report.report(
            new ErrorMessage(
                "functionReturn",
                "function-call-no-effect",
                `This function call has no effect`,
                loc,
                true
            )
        );
    }
}

function hasEffect(infos: Infos, func: nFunc) {
    if (!func.borderEffect && !func.hasReturn) {
        infos.report.report(
            new ErrorMessage(
                "functionReturn",
                "function-no-effect",
                `Your function has no effect on the program`,
                func.func.loc,
                true
            )
        );
    }
}

const verifyFuncCall = function (infos: Infos) {
    funcs = [];
    if (infos['allFuncs'] instanceof Array) {
        infos['allFuncs'].forEach((func: Func) => {
            const found: nFunc = funcs.find((e) => e.compareFunc(func));
            if (!found) {
                const elem: nFunc = new nFunc(func)
                funcs.push(elem);
                checkBorderEffect(infos, elem);
                checkFunctionForReturn(infos, elem);
                hasEffect(infos, elem);
            } else {
                hasEffect(infos, found);
            }
        });
    }
    if (infos['allCallExpressions'] instanceof Array) {
        infos['allCallExpressions'].forEach((ex: CallExpression) => {
            if (ex.statementLoc && ex.statementLoc['expression'].type === "CallExpression") {
                const c: CallExpression = ex.statementLoc['expression'];
                if (c.callee.type === "Identifier") {
                    const id = c.callee;
                    if (id['variable']) {
                        const found: nFunc = funcs.find((e) => e.compareCall(id['variable']));
                        if (found) {
                            sendErrors(infos, found, ex.loc);
                        }
                    }
                }
            }
        });
    }
}

exports.handler = async function (event: any, context: any, debug: boolean = false) {
    if (!event['allBlocks']) {
        return {
            statusCode: 500,
            headers: "genMetadata",
            infos: {
                report: { internal: "graph not found." }
            }
        };
    }

    const infos = regenDict(new Infos(event));

    try {
        verifyFuncCall(infos);

        return {
            statusCode: 200,
            headers: 'functionReturn',
            infos: serializeDict(infos),
        };
    } catch (e) {
        console.log('[functionReturn] ', e);
        infos.report.report(new ErrorMessage('internal', 'internat', `[functionReturn] ` + e));
        return {
            statusCode: 500,
            headers: 'functionReturn',
            infos: serializeDict(infos),
        }
    }
}