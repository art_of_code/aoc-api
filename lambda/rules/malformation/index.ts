import { Infos } from "../../../common/inspector/Infos";
import '../../../common/graph/instanciateAll';
import { regenDict, serializeDict } from "../../../common/inspector/RegenFactory";
import { ErrorMessage } from '../../../common/inspector/Report';
import { Block, Func, Condition, Link, Loc, Class } from '../../../common/graph/Block';
import { Identifier, CallExpression, Expression, LogicalExpression, MemberExpression, NumericLiteral, BinaryExpression, UnaryExpression, AssignmentExpression } from '../../../common/graph/Expressions';
import { compareTypes, Variable, Types, compareType, VarType, ComplexType } from "../../../common/graph/Variables";
import { deductConstant, deductExpression } from '../../../common/inspector/constantDeductor';

function checkForSuper(infos: Infos, func: Func) {
    let num: number = 0;
    let loc = func.loc;

    if (infos['allCallExpressions'] instanceof Array) {
        infos['allCallExpressions'].forEach((call: CallExpression) => {
            if (call.loc.compare(func.loc) === Loc.comp.IN && call.callee.type === "Super") {
                num += 1;
                loc = call.loc;
            }
        });
    }
    if (num === 0) {
        infos.report.report(new ErrorMessage("malformation", "malformation", "This class constructor should have a call to super().", func.loc));
    } else if (num > 1) {
        infos.report.report(new ErrorMessage("malformation", "malformation", "Their should be only one call to super in this constructor.", loc));
    }
    if (num > 0 && !(func.body && func.body.statements && func.body.statements[0] && func.body.statements[0]['expression'] && func.body.statements[0]['expression'].callee && func.body.statements[0]['expression'].callee.type === "Super")) {
        infos.report.report(new ErrorMessage("malformation", "malformation", "The super should be placed at the start of the constructor.", loc));
    }
}

const malformedSuper = function (infos: Infos) {
    if (infos['allClasss'] instanceof Array) {
        infos['allClasss'].forEach((clas: Class) => {
            if (clas.superClass && clas.methods) {
                clas.methods.forEach((method) => {
                    if (method.name === "constructor") {
                        checkForSuper(infos, method);
                    }
                })
            }
        });
    }
}

function checkForLabel(label: string, infos: Infos): boolean {
    let called: boolean = false;
    if (infos['allBreakStatements'] instanceof Array) {
        infos['allBreakStatements'].forEach((brea: any) => {
            if (brea.label === label) {
                called = true;
            }
        });
    }
    if (infos['allContinueStatements'] instanceof Array) {
        infos['allContinueStatements'].forEach((cont: any) => {
            if (cont.label === label) {
                called = true;
            }
        });
    }
    return called;
}

const malformedLabels = function (infos: Infos) {
    const labels: string[] = [];
    if (infos['allLinks'] instanceof Array) {
        infos['allLinks'].forEach((link: any) => {
            if (link.labeled && !labels.find((elem) => elem === link.labeled)) {
                labels.push(link.labeled);
                if (!checkForLabel(link.labeled, infos)) {
                    const loc: Loc = new Loc(infos, { loc: { start: link.from.loc.end, end: link.to.loc.start } });
                    infos.report.report(new ErrorMessage("malformation", "malformation", "the label " + link.labeled + " is never called.", loc));
                }
            }
        });
    }
}

function isFloat(num: NumericLiteral): boolean {
    return num.extra.raw.includes(".");
}

function manageFloat(infos: Infos) {
    if (infos['allBinaryExpressions'] instanceof Array) {
        infos['allBinaryExpressions'].forEach((bin: BinaryExpression) => {
            if (((bin.left.type === "NumericLiteral" && (bin.operator === "===" || bin.operator === "==")) && isFloat(bin.left as NumericLiteral)) || ((bin.right.type === "NumericLiteral" && (bin.operator === "===" || bin.operator === "==")) && isFloat(bin.right as NumericLiteral))) {
                infos.report.report(new ErrorMessage("malformation", "malformation", "Floats should not be compared like this.", bin.loc));
            }
        });
    }
}

function manageTablesCall(infos: Infos) {
    if (infos['allMemberExpressions'] instanceof Array) {
        infos['allMemberExpressions'].forEach((member: MemberExpression) => {
            if (member.property.type === "UnaryExpression") {
                const expr: UnaryExpression = member.property as UnaryExpression;
                if (expr.operator && expr.operator === '-') {
                    infos.report.report(new ErrorMessage("malformation", "malformation", "Used a negative index for a table.", member.loc));
                }
            }
        });
    }
}

const malformedTableInsertion = function (infos: Infos) {
    if (infos['allAssignmentExpressions'] instanceof Array) {
        infos['allAssignmentExpressions'].forEach((assign: AssignmentExpression) => {
            if (assign.left && assign.left.type === "MemberExpression") {
                const left: MemberExpression = assign.left as MemberExpression;
                const leftObject: Identifier = left.object as Identifier;
                if (!leftObject.staticTypes || leftObject.staticTypes.length <= 0
                    || !assign.left.staticTypes || assign.left.staticTypes.length <= 0 || leftObject.staticTypes[0].type !== VarType.ARRAY) {
                    return;
                }
                const leftType = leftObject.staticTypes[0];
                const rightTypes = assign.right.staticTypes;
                if (leftType.type !== VarType.ARRAY || leftType.complexType.length <= 0 || !compareTypes(rightTypes, leftType.complexType[0].types)) {
                    infos.report.report(new ErrorMessage("malformation", "malformation", "Inserted different type of values in a single array.", assign.loc));
                }
            }
        });
    }
}

function manageExpression(expr, infos: Infos) {
    if (expr.type === "LogicalExpression" || expr.type === "BinaryExpression") {
        if (expr.operator === "==") {
            infos.report.report(new ErrorMessage("malformation", "malformation", "Should use \"===\" instead of \"==\".", expr.loc));
        }
        if (expr.left) {
            manageExpression(expr.left, infos);
        }
        if (expr.right) {
            manageExpression(expr.right, infos);
        }
    } else if (expr.type === "AssignmentExpression") {
        infos.report.report(new ErrorMessage("malformation", "malformation", "Shouldn't assign in a condition.", expr.loc));
    }
}

const malformedEqualities = function (infos: Infos) {
    manageTablesCall(infos);
    manageFloat(infos);
    if (infos['allConditions'] instanceof Array) {
        infos['allConditions'].forEach((condition: Condition) => {
            manageExpression(condition.condition, infos);
        });
    }
}

const malformedIfStatement = function (infos: Infos) {
    if (infos['allBlocks'] instanceof Array) {
        infos['allBlocks'].forEach((block: Block) => {
            if (block.type === "IfBlock") {
                const consequent = block.links[0].to
                const alternate = block.links[1].to
                if (consequent.statements.length === 1 && consequent.statements[0].type === "EmptyStatement") {
                    if (alternate === block["nextBlock"]) {
                        infos.report.report(new ErrorMessage("malformation", "malformation", "The If body won't be executed.", consequent.loc));
                    }
                } else if (alternate.statements.length === 1 && alternate.statements[0].type === "EmptyStatement") {
                    infos.report.report(new ErrorMessage("malformation", "malformation", "Unused else", alternate.loc, true));
                }
            }
        })
    }
}

const minimumArgumentsNumber = function (func: Func) {
    let nMinArgs = 0;
    func.params.forEach((arg: Expression, i: number) => {
        if (arg.type !== "AssignmentPattern") {
            nMinArgs = i + 1;
        }
    });
    return nMinArgs;
}

const malformedCallExpressionType = function (infos: Infos) {
    if (infos['allCallExpressions'] instanceof Array) {
        infos['allCallExpressions'].forEach((call: CallExpression) => {
            if (call.callee.type === "Identifier") {
                const func = call.callee as Identifier
                if (func.variable && func.variable.isFunction === true) {
                    const block: Block = infos['allBlocks'].find((block: Block) => block.variables.find(v => v === func.variable));
                    if (!block) {
                        console.log('[malformedCallExpressionType] block not found');
                        return;
                    }
                    const f: Func = block.functions.find(f => f.name === func.name);
                    if (!f) {
                        console.log(`[malformedCallExpressionType] ${func.name} function not found`);
                        return;
                    }
                    if (call.arguments.length > f.params.length || call.arguments.length < minimumArgumentsNumber(f)) {
                        infos.report.report(new ErrorMessage("malformation", "malformation", `The function '${func.name}' is missing arguments`, call.loc));
                    } else {
                        call.arguments.forEach((arg: Expression, i: number) => {
                            if (compareTypes(arg.staticTypes, f.params[i].staticTypes) === false) {
                                infos.report.report(new ErrorMessage("malformation", "malformation", `The argument number ${i} does not have the right type`, arg.loc));
                            }
                        });
                    }
                }
            }
        });
    }
}

const malformedFunctionParameters = function (infos: Infos) {
    const reducer = function (params: Array<{ name: string, start: any, end: any }>): Array<{ of: typeof params[0], to: typeof params[0] }> {
        if (params.length == 0) {
            return [];
        }

        let first = params.shift();

        let badNamedParameters = (params as any).reduce((acc: any, param: typeof params[0]) => {
            if (param.name === first.name) {
                acc.push({
                    of: first,
                    to: param
                });
            }
            return acc;
        }, []);

        return [...badNamedParameters, ...reducer(params)];
    }
    if (infos['allFuncs'] instanceof Array) {
        infos['allFuncs'].forEach((func: Func) => {
            let params = func.params.map(param => ({
                name: (param as Identifier).name,
                start: param.loc.start,
                end: param.loc.end
            }));
            const errors = reducer(params);
            if (errors.length > 0) {
                errors.forEach(e => {
                    infos.report.report(
                        new ErrorMessage(
                            "malformation",
                            "function-parameters-reuse-name",
                            `You must use different name for each function parameters (reused at ${e.to.start.line}:${e.to.start.column})`,
                            { start: e.of.start, end: e.of.end },
                            true
                        )
                    );
                    infos.report.report(
                        new ErrorMessage(
                            "malformation",
                            "function-parameters-reuse-name",
                            `You must use different name for each function parameters (already used at ${e.of.start.line}:${e.of.start.column})`,
                            { start: e.to.start, end: e.to.end },
                            false
                        )
                    );
                })
            }
        })
    }
}

const malformedSwitchCaseEqual = function(infos: Infos) {
    const reducer = function(cases: Array<{ caseValue: ReturnType<typeof deductConstant> | 'default', start: any, end: any }>): Array<{ of: typeof cases[0], to: typeof cases[0] }>  {
        if (cases.length == 0) { return []; }

        let first = cases.shift();

        let badSwitchCases = (cases as any).reduce((acc: any, case_: typeof cases[0]) => {
            if (
                first.caseValue === 'default' && case_.caseValue === 'default' || (
                    typeof first.caseValue !== 'string' && first.caseValue.isDeductible() &&
                    typeof case_.caseValue !== 'string' && case_.caseValue.isDeductible() &&
                    first.caseValue.get() === case_.caseValue.get()
                )
            ) {
                acc.push({
                    of: first,
                    to: case_
                });
            }
            return acc;
        }, []);

        return [ ...badSwitchCases, ...reducer(cases) ];
    }
    if (infos['allBlocks'] instanceof Array) {
        infos['allBlocks'].forEach((block: Block) => {
            if (block.type === "SwitchBlock") {
                let cases = block["cases"].map(case_ => {
                    let condition = case_.prevLinks[0].condition.condition;
                    let caseValue: ReturnType<typeof deductConstant> | string;
                    
                    if (!!condition && !!condition.right && condition.right.type === 'Identifier') {
                        caseValue = deductConstant(infos, case_, condition.right.name);
                    } else if (!!condition && !!condition.right) {
                        caseValue = deductExpression(infos, condition.right);
                    } else {
                        return {
                            caseValue: 'default',
                            start: case_.loc.start,
                            end: case_.loc.end
                        };
                    }

                    return {
                        caseValue,
                        start: condition.right.loc.start,
                        end: condition.right.loc.end
                    };
                });

                const errors = reducer(cases);

                if (errors.length > 0) {
                    errors.forEach(e => {
                        infos.report.report(
                            new ErrorMessage(
                                "malformation",
                                "switch-case-duplicate",
                                `You cannot define multiple cases with the same value (reused at ${e.to.start.line}:${e.to.start.column})`,
                                { start: e.of.start, end: e.of.end },
                                true
                            )
                        );
                        infos.report.report(
                            new ErrorMessage(
                                "malformation",
                                "switch-case-duplicate",
                                `You cannot define multiple cases with the same value (already used at ${e.of.start.line}:${e.of.start.column})`,
                                { start: e.to.start, end: e.to.end },
                                false
                            )
                        );
                    })
                }
            }
        })
    }
}

exports.handler = async function (event: any, context: any) {
    if (!event['allBlocks']) {
        return {
            statusCode: 500,
            headers: 'malformation',
            infos: {
                report: { internal: 'graph not found.' }
            }
        };
    }

    const infos = regenDict(new Infos(event));

    try {
        [
            malformedIfStatement,
            malformedFunctionParameters,
            malformedCallExpressionType,
            malformedEqualities,
            malformedTableInsertion,
            malformedLabels,
            malformedSuper,
            malformedSwitchCaseEqual
        ].forEach(f => f(infos));

        return {
            statusCode: 200,
            headers: 'malformation',
            infos: serializeDict(infos),
        };
    } catch (e) {
        console.log('[malformation] ', e);
        infos.report.report(new ErrorMessage('internal', 'internal', `[malformation] ` + e));
        return {
            statusCode: 500,
            headers: 'malformation',
            infos: serializeDict(infos),
        }
    }
}

// exports.handler = async function(event: any, context: any, debug: boolean = false) {
//     if (!event.ast) {
//         return {
//             "statusCode": 404,
//             "headers": "malformation",
//             "infos": "ast not found."
//         };
//     }

//     event = RegenFactory.regenDic(event);
//     const rules: any = [
//         new Malformed_IfStatement,
//         new Malformed_FunctionParameters,
//         new Malformed_SwitchCaseEqual
//     ];
//     const infos = inspectAST(event.ast, rules, event, debug);

//     return {
//         "statusCode": 200,
//         "headers": "malformation",
//         "infos": Indexer.serializeDic(infos.getInfos(["report", "_allScopes", "_allVars", "_allFuncs", "_allFuncCalls", "_allArgs"])),
//     };
// }
