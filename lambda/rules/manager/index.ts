var AWS = require("aws-sdk");
AWS.config.region = 'eu-west-2';
var lambda = new AWS.Lambda({
    apiVersion: '2031',
    endpoint: process.env.STAGE === 'local' ? 'http://localhost:3000' : 'https://lambda.eu-west-2.amazonaws.com',
});

const AWS_RULE_TAG = `aoc-api-${process.env.STAGE}-`;
const INVALID_RULE_NAMES = ["ast"];

/**
 * @TODO
 * Si une règle peut modifier l'ast, TOUTES les dépendances des règles qui dépendent de cette règle doivent hérité de la modification.
 * Si cela n'est pas fait, cela peut provoqué des pertes de données.
 * ex:
 * si A et B sont des règles pouvant modifier l'ast et que C hérite de A et de B, on a le graph suivant:
 *  A   B
 *   \ /
 *    C
 * Hors on ne peut merge automatiquement les modifications de A et de B. La règle C aura donc un comportement indeterminé.
 *
 * conclusion: une règle A qui a des dépendences A1, A2, ..., An veut dire que AU MOINS A1, A2, ..., An sera passer.
 */

exports.handler = async function (event: any, context: any) {
    return Rule_Manager(event, context).catch(e => ({
      "statusCode": 500,
      "headers": "manager",
      "infos": e
    }));
}

class Rule {
    name: string;
    dependencies: Rule[] = []; /** references list of dependencies */
    infos: any = {};
    inQueue: boolean = false;
    override: boolean = false;

    /** construct a rule put it automatically in Rule.allRules */
    constructor(name: string, deps: string[], override: boolean) {
        for (let invalidRuleName of INVALID_RULE_NAMES) {
            if (name === invalidRuleName) {
                throw `invalid rule name ${name}`;
            }
        }

        this.name = name;
        this.override = override;
        Rule.allRules.push(this);
        for (let dep of deps) {
            /** check if dependancy is already in */
            if (this.hasDependency(dep) === false) {
                this.dependencies.push(Rule.getRule(dep) || Rule.genRule(dep));
            }
        }
    }

    get rulename(): string {
        return AWS_RULE_TAG + this.name;
    }

    hasDependency(name: string): boolean {
        if (name === this.name) {
            throw `the rule ${this.name} depends to itself.`;
        }
        if (this.dependencies.reduce(((p, rule) => p || rule.name === name), false))
            return true;
        for (let dep of this.dependencies) {
            if (dep.name === name) {
                return true;
            }
        }
        return false;
    }

    static getRule(name: string): Rule {
        return this.allRules.find(rule => rule.name === name);
    }

    /** genRule search the rule on aws on create it */
    static genRule(name: string, dependencies: string[] = [], override = false): Rule {
        /** @TODO search the rule on aws */
        return new Rule(name, dependencies, override);
        throw `${name} rule not found.`;
    }

    static allRules: Rule[] = [];
}

async function Rule_Manager(request: any, context: any): Promise<any> {
    if (!request.ast || typeof request.ast !== 'object') {
        throw "Request error: ast not found.";
    }
    let rulesOrder: Rule[][] = buildRulesExecutionQueue(request);
    let report: any = {};

    for (let rules of rulesOrder) {
        await Promise.all(
            rules.map((rule) => executeRule(rule, request.ast))
        ).then((responses) => {
            for (let response of responses) {
                if (response.StatusCode !== 200)
                    throw response.Payload;
            }
            let zipedInfos: any[][] = rules.map((e, i) => [e, JSON.parse(responses[i].Payload)]);
            gatherRuleInfos(zipedInfos, report);
        });
    }
    return {
        statusCode: 200,
        body: JSON.stringify(report)
    };
}

/**
 * @TODO find every rules in aws
 * @TMP we put const list of rules
 */
function generatesRules(ruleNames: string[]): void {
    const rules = [
        { name: 'genGraph', dependencies: [], override: true },
        { name: 'genMetadata', dependencies: ['genGraph'], override: true },
        { name: 'varlog', dependencies: ['genMetadata'], override: true },
        { name: 'typeCheck', dependencies: ['varlog'], override: true },
        { name: 'tautology', dependencies: ['varlog'], override: true },
        { name: 'deadcode', dependencies: ['tautology', 'typeCheck'] },
        { name: 'malformation', dependencies: ['typeCheck'] },
        { name: 'usedImports', dependencies: ['typeCheck'] },
        // { name: 'functionReturn', dependencies: ['typeCheck'] },
        // {name: 'genMetadata2', dependencies: ['tautology']},
    ];
    Rule.allRules = [];
    /** /!\ generate the rule put it automatically in Rule.allRules */
    for (const rule of rules) {
        const r = Rule.getRule(rule.name);
        if (r) {
            r.override = rule.override;
        } else {
            Rule.genRule(rule.name, rule.dependencies, rule.override);
        }
        // ruleNames.forEach(ruleName => Rule.genRule(ruleName));
    }
}

/**
 * infos: [Rule, respond][]
 * Extract the report from all infos and put them into $report
 * put all infos to the appropriate rule
 */
function gatherRuleInfos(infos: any[][], report: any) {
    for (let [rule, response] of infos) {
        if (response.statusCode === 200 && typeof response.infos === "object") {
            /** get the report and remove it from respond.infos */
            if (response.infos.report) {
                for (let key of Object.keys(response.infos.report)) {
                    if (!report[key])
                        report[key] = []
                    Array.prototype.push.apply(report[key], response.infos.report[key]);
                }
                delete response.infos.report;
            }
            if (rule.override) {
                rule.infos = response.infos;
            }
        } else {
            if (!response.statusCode) {
                response.ruleName = rule.name;
                throw JSON.stringify(response);
            } else {
                throw `Internal error: rule ${rule.name}: ${JSON.stringify(response.infos)}`;
            }
        }
    }
}

/**
 * Build a 2d list, which contains for each entry of the top list a list of rules that can be executed together safely
 */
function buildRulesExecutionQueue(request: any): Rule[][] {
    generatesRules(request.rules);

    var lists: Rule[][] = [];
    let rulesCpy: Rule[] = Rule.allRules; /** because we remove elements from the list we need to do a copy */

    while (rulesCpy.length !== 0) {
        const [executable, notExecutable] = rulesCpy.reduce((arr: [Rule[], Rule[]], rule: Rule) => {
            /** put all executable rules on arr[0] and others on arr[1] */
            if (isRuleExecutable(rule)) {
                arr[0].push(rule);
            } else {
                arr[1].push(rule);
            }
            return arr;
        }, [[], []]);

        if (executable.length === 0) {
            throw "circular dependencie found.";
        }

        lists.push(executable);
        rulesCpy = notExecutable;
        lists[lists.length - 1].forEach(rule => {
            rule.inQueue = true;
        });
    }
    if (lists.reduce(((sum, row) => sum + row.length), 0) !== Rule.allRules.length) {
        throw ("Internal Error: The number of rules inserted in buildRulesExecutionQueue() is smaller than the total amount of rules.");
    }
    return lists;
}

/**
 * Build the payload and execute the rules, which returns a promise
 * We add the dependencies' infos into the payload if required
 */
function executeRule(rule: Rule, ast: any): Promise<any> { /** @TODO Build payloads as async */
    /** shift all infos from dep to rule */
    let payload: any;
    if (rule.dependencies.length > 0) {
        payload = rule.dependencies.reduceRight(((p, r: Rule) => {
            return { ...p, ...r.infos };
        }), {});
    } else {
        payload = { ast: ast };
    }
    let params = {
        FunctionName: rule.rulename,
        InvocationType: 'RequestResponse', // synchronous, otherwise change it to 'Event'
        LogType: 'Tail',
        Payload: JSON.stringify(payload)
    };
    return lambda.invoke(params).promise();
}

/**
 * Check if a rule has no more dependencies not in queue
 * A rule is executable iff all its dependencies are already in queue
 */
function isRuleExecutable(rule: Rule): boolean {
    return rule.dependencies.reduce(((p, c) => p && c.inQueue), true);
}
