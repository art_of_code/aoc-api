const mongoose = require('mongoose');

const model = mongoose.model('ProcessedResult', {
  foundErrors: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now
  },
  cognitoID: {
    type: String,
    required: true,
  }
})

module.exports = model;
