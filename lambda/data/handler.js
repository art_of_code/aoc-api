const mongoHelper = require('../../common/mongoHelper/connect');
const middy = require('middy');
const { httpErrorHandler, cors } = require('middy/middlewares');
const mongoose = require('mongoose');
const ProcessedResult = require('./processedResult');

const register = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  await mongoHelper.connectToDatabase();

  const msg = JSON.parse(event.Records[0].Sns.Message);

  const newResult = new ProcessedResult({
    foundErrors: msg.errors,
    cognitoID: msg.userID,
  })
  await newResult.save()
    .catch(e => {
      console.log(e);
    })
  await mongoose.disconnect();
}

module.exports.register = middy(register).use(httpErrorHandler());

const getAll = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  await mongoHelper.connectToDatabase();
  const userID = event.requestContext.authorizer.claims.sub;

  const r = await ProcessedResult.find({ cognitoID: userID })
    .exec()
    .then(results => {
      return ({ statusCode: 200, body: JSON.stringify(results.map(v => { return { foundErrors: v.foundErrors, date: v.date } })) });
    })
    .catch(e => {
      return ({ statusCode: 500, body: JSON.stringify({ error: e }) });
    })

  await mongoose.disconnect();
  return r;
}

module.exports.get = middy(getAll).use(cors()).use(httpErrorHandler());
