const middy = require('middy');
const { cors, httpMultipartBodyParser, httpErrorHandler } = require('middy/middlewares');
const AWS = require('aws-sdk');

let lambda = new AWS.Lambda({
  apiVersion: '2031',
  endpoint: process.env.STAGE === 'local' ? 'http://localhost:3000' : 'https://lambda.eu-west-2.amazonaws.com',
});
let sns = new AWS.SNS();
let payloadRules;

/* const paramsParser = {
  FunctionName: `aoc-api-${process.env.STAGE}-parser`,
  InvocationType: 'RequestResponse',
  LogType: 'Tail',
} */

/* const paramsRuleManager = {
  FunctionName: `aoc-api-${process.env.STAGE}-manager`,
  InvocationType: 'RequestResponse',
  LogType: 'Tail',
} */

const paramsSNS = {
  TopicArn: process.env.SNS_PROCESS_RESULT
}


const proc = async (event, context) => {
  const userID = event.body.userID;
  const file = event.body.file;
  const data = file.content.toString();
  let ast = null;

  try {
    ast = await new Promise((resolve, reject) => {
      lambda.invoke({
        FunctionName: `aoc-api-${process.env.STAGE}-parser`,
        InvocationType: 'RequestResponse',
        LogType: 'Tail',
        Payload: JSON.stringify({ data: data })
      }, (err, data) => {
        if (err) {
          reject({ err })
        } else {
          resolve(data.Payload)
        }
      })
    })
    payloadRules = JSON.stringify({ ast: JSON.parse(ast).program, rules: [], options: {} });
  } catch (e) {
    return e;
  }

  try {
    return await new Promise((resolve, reject) => {
      lambda.invoke({
        FunctionName: `aoc-api-${process.env.STAGE}-manager`,
        InvocationType: 'RequestResponse',
        LogType: 'Tail',
        Payload: payloadRules
      }, (err, data) => {
        if (err) {
          console.log(err);
          reject({ statusCode: 500, body: JSON.stringify(err) });
        } else {
          console.log(data.Payload);
          paramsSNS.Message = JSON.stringify({
            userID: event.requestContext.authorizer.claims.sub,
            errors: data.Payload ? data.Payload : null
          });
          sns.publish(paramsSNS, (err, data) => {
            if (err) console.log(err, err.stack);
            else console.log(data);
          })
          data.Payload = JSON.parse(data.Payload);
          if (data.Payload.body) {
            data.Payload.body = JSON.parse(data.Payload.body);
          }
          resolve({ statusCode: 200, body: JSON.stringify({ errors: data.Payload }) });
        }
      });
    })
  } catch (e) {
    console.log(e);
    return e;
  }
}

module.exports.process = middy(proc).use(httpMultipartBodyParser()).use(cors()).use(httpErrorHandler());
