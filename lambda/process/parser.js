const babelParser = require('@babel/parser');

const parser = async (event, context) => {
  try {
    const ast = babelParser.parse(event.data, { errorRecovery: true, sourceType: "module", allowImportExportEverywhere: true});
    return ast;
  } catch (e) {
    console.log(e);
    return e;
  }
}

module.exports.parser = parser;