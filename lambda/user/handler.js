const mongoHelper = require('../../common/mongoHelper/connect');
const middy = require('middy');
const { httpErrorHandler, cors, jsonBodyParser } = require('middy/middlewares');
const mongoose = require('mongoose');
const ProcessedResult = require('../data/processedResult');
const AWS = require('aws-sdk');

let cognito = new AWS.CognitoIdentityServiceProvider({
  accessKeyId: process.env.COGNITO_APP_ACCESSKEY,
  secretAccessKey: process.env.COGNITO_APP_SECRET_ACCESSKEY,
  region: 'eu-west-2',
});

const del = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  await mongoHelper.connectToDatabase();

  return await new Promise((res, rej) => {
    console.log(event.body.accessToken);
    cognito.deleteUser({ AccessToken: event.body.accessToken }, (err, data) => {
      if (err) {
        console.log(err);
        rej({ statusCode: 500, body: JSON.stringify(err) });
      } else {
        ProcessedResult.deleteMany({ cognitoID: event.requestContext.authorizer.claims.sub })
          .exec()
          .then(async r => {
            console.log(r);
            await mongoose.disconnect();
            res({ statusCode: 200, body: "User deleted" });
          })
          .catch(async e => {
            console.log(e);
            await mongoose.disconnect();
            rej({ statusCode: 500, body: JSON.stringify(e) });
          });
      }
    });
  });
}

module.exports.delete = middy(del).use(jsonBodyParser()).use(cors()).use(httpErrorHandler());
