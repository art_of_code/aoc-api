const mongoose = require('mongoose');
const validator = require('validator');

const model = mongoose.model('User', {
  username: {
    type: String,
    required: true,
    validate: {
      validator(username) {
        return validator.isAlphanumeric(username);
      }
    }
  },
  email: {
    type: String,
    required: true,
    index: { unique: true, dropDups: true },
    validate: {
      validator(email) {
        return validator.isEmail(email);
      }
    }
  },
  password: {
    type: String,
    required: true,
  },
  verified: {
    type: Boolean,
    required: true,
    default: false,
  },
})

module.exports = model;