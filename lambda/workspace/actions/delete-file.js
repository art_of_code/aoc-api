const action_base_class = require('../action-base-class.js');
const file = require('../models/file');
const workspace_token = require('../models/token');

class delete_file_action extends action_base_class  {

  static get_action_key() {
    return 'delete-file';
  }

  constructor(body, event, context) {
    super(event, context);
    this._body = body;
  }

  async delete(path) {
    return file.deleteOne({
      path: path,
      workspace: this._workspace_id
    }).exec();
  }
  
  async exec() {
    const domain = this._event.requestContext.domainName;
    const stage = this._event.requestContext.stage;
    const connectionId = this._event.requestContext.connectionId;
    const callbackUrlForAWS = this._util.format(this._util.format('https://%s/%s', domain, stage));
    
    this._workspace_id = (await workspace_token.findOne({ token: connectionId }).exec()).workspace;
    await this.delete(this._body.path);

    return new Promise((resolve, reject) => {
      const apigatewaymanagementapi = new this._AWS.ApiGatewayManagementApi({
        apiVersion: 'v0',
        endpoint: callbackUrlForAWS,
      });
      apigatewaymanagementapi.postToConnection({
        ConnectionId: connectionId,
        Data: JSON.stringify({ type: 'response', data: "deleted" }),
      }, (err, data) => {
        if (err) {
          console.log('[AWS WS Error]', err);
          reject(err);
          return;
        }
        resolve(data);
      });
    });
  }

};

module.exports = delete_file_action;