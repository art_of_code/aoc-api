const action_base_class = require('../action-base-class.js');
const file = require('../models/file');
const workspace_token = require('../models/token');

class create_file_action extends action_base_class  {

  static get_action_key() {
    return 'create-file';
  }

  constructor(body, event, context) {
    super(event, context);
    this._body = body;
  }

  async exec() {
    const domain = this._event.requestContext.domainName;
    const stage = this._event.requestContext.stage;
    const connectionId = this._event.requestContext.connectionId;
    const callbackUrlForAWS = this._util.format(this._util.format('https://%s/%s', domain, stage));

    const dfile = new file({
      name: this._body.name,
      path: this._body.path,
      data: this._body.data,
      workspace: (await workspace_token.findOne({ token: connectionId }).exec()).workspace
    });

    await dfile.save();

    return new Promise((resolve, reject) => {
      const apigatewaymanagementapi = new this._AWS.ApiGatewayManagementApi({
        apiVersion: 'v0',
        endpoint: callbackUrlForAWS,
      });
      apigatewaymanagementapi.postToConnection({
        ConnectionId: connectionId,
        Data: JSON.stringify({ type: 'response', data: "created" }),
      }, (err, data) => {
        if (err) {
          console.log('[AWS WS Error]', err);
          reject(err);
          return;
        }
        resolve(data);
      });
    });
  }

};

module.exports = create_file_action;