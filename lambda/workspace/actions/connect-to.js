const mongoose = require('mongoose');
const workspace = require('../models/workspace');
const token = require('../models/token');
const action_base_class = require('../action-base-class.js');
const uuid = require('uuid/v4');

class connect_to_action extends action_base_class  {

  static get_action_key() {
    return 'connect-to';
  }

  constructor(body, event, context) {
    super(event, context);
    this._body = body;
  }

  async exec() {
    const domain = this._event.requestContext.domainName;
    const stage = this._event.requestContext.stage;
    const connectionId = this._event.requestContext.connectionId;
    const callbackUrlForAWS = this._util.format(this._util.format('https://%s/%s', domain, stage));

    const wspace = await workspace.findOne({ name: this._body.workspace_name });

    const newToken = new token({
      token: connectionId,
      workspace: wspace._id,
    });

    await newToken.save();

    return new Promise((resolve, reject) => {
      const apigatewaymanagementapi = new this._AWS.ApiGatewayManagementApi({
        apiVersion: 'v0',
        endpoint: callbackUrlForAWS,
      });
      apigatewaymanagementapi.postToConnection({
        ConnectionId: connectionId,
        Data: JSON.stringify({ type: 'response', status: 'success', data: 'connected' }),
      }, (err, data) => {
        if (err) {
          console.log('[AWS WS Error]', err);
          reject(err);
          return;
        }
        resolve(data);
      });
    });
  }

};

module.exports = connect_to_action;