const mongoose = require('mongoose');

const file = mongoose.model('workspace-file', {
  name: {
    type: String,
    required: true,
  },
  path: {
    type: String,
    required: true,
  },
  data: {
    type: Buffer,
  },
  workspace: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'workspace',
  },
})

module.exports = file;