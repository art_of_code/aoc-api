const mongoose = require('mongoose');
const validator = require('validator');

const workspace = mongoose.model('workspace', {
  name: {
    type: String,
    required: true,
    validate: {
      validator(name) {
        return validator.isAlphanumeric(name);
      }
    }
  },
})

module.exports = workspace;