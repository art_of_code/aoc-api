const mongoose = require('mongoose');

const file = mongoose.model('workspace-token', {
  token: {
    type: String,
    required: true,
  },
  workspace: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'workspace',
  },
})

module.exports = file;