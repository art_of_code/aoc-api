const { actions_dispatcher } = require('./actions-dispatcher');
const WorkSpace = require('./models/workspace');
const middy = require('middy');
const { jsonBodyParser, httpErrorHandler, cors } = require('middy/middlewares');
const mongoHelper = require('../../common/mongoHelper/connect');

const success = { statusCode: 200 };
const dispatcher = new actions_dispatcher();

const handlers = {
  create: async (event, context) => {
    context.callbackWaitsForEmptyEventLoop = false;

    await mongoHelper.connectToDatabase();

    console.log(event);

    const { body } = event;
    const newWorkSpace = new WorkSpace({
      name: body.name,
    });

    const r = await newWorkSpace.save()
      .then(() => {
        return ({ statusCode: 200, body: JSON.stringify('workspace created') });
      })
      .catch(e => {
        return ({ statusCode: 500, body: JSON.stringify({ error: e }) });
      });
    await mongoHelper.disconnect();

    return r;
  },

  remove: async (event, context) => {
    context.callbackWaitsForEmptyEventLoop = false;

    await mongoHelper.connectToDatabase();

    const { body } = event;
    try {
      const db_result = await WorkSpace.findOne({
        name: body.name,
      }).exec();

      db_result.remove().exec();
    } catch (e) {
      console.error(e);
      return { statusCode: 500, body: JSON.stringify({ error: 'Cannot remove this workspace' }) };
    }
    await mongoHelper.disconnect();
    return { statusCode: 200, body: JSON.stringify('WorkSpace created') }
  },

  connect: async (event, context) => {
    return success;
  },

  disconnect: async (event, context) => {
    return success;
  },

  default_h: async (event, context) => {
    console.log(event.body);
    const body = JSON.parse(event.body);
    await dispatcher.ready;
    const action = dispatcher.dispatch(body.action, body, event, context);
    const require_db = action.require_db();

    require_db && await mongoHelper.connectToDatabase();
    for (let i = 0; i < action._middlewares.length; ++i) {
      await action._middlewares[i]();
    }
    await action.exec();
    require_db && await mongoHelper.disconnect();

    return success;
  },
};

[ 'create', 'remove' ].forEach((funcName) => { handlers[funcName] = middy(handlers[funcName]).use(jsonBodyParser()).use(cors()).use(httpErrorHandler()); });

Object.assign(module.exports, handlers);