const util = require('util');
const AWS = require('aws-sdk');

class action_base_class {

  constructor(event, context) {
    this._AWS = AWS;
    this._util = util;
    this._event = event;
    this._context = context;
    this._require_db = true;
    this._middlewares = [];
  }

  require_db() { return this._require_db; }
  add_middleware(midware) { this._middlewares.push(midware.bind(this)); }

  async exec() {
    throw new Error('action_base_class::exec() not overrided')
  }

}

module.exports = action_base_class;