const path = require('path');
const fs = require('fs');

class actions_dispatcher {
  constructor() {
    const directoryPath = path.join(__dirname, 'actions');
    this.ready = new Promise(resolve => {
      // This code create a function in runtime that select the right ctor depending on action key
      fs.readdir(directoryPath, (err, files) => {
        if (err) {
          return console.log('Unable to scan directory: ' + err);
        }
        
        const cases = [];
        const ctors_name = [];
        const ctors = [];
      
        files.forEach((file, i) => {
          const ctor = require(path.join(directoryPath, file));
          
          cases.push(`case '${ctor.get_action_key()}': return new action_${i}(body, event, context);\n`);
      
          ctors_name.push(`action_${i}`);
          ctors.push(ctor);
        });
      
        let cases_str = '';
        cases.forEach((v) => cases_str += v);
      
        const base_fct = `
          switch (action) {
            ${cases_str};
            default: throw new Error('Unknown action key');
          }
        `;
      
        // JIT 
        this.dispatch = new Function(...ctors_name, 'action', 'body', 'event', 'context', base_fct).bind(null, ...ctors);
        resolve();
      });
    })

  }
}

module.exports = {
  actions_dispatcher,
}