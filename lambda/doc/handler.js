const middy = require('middy');
const { jsonBodyParser, httpErrorHandler, cors } = require('middy/middlewares');
const mongoHelper = require('../../common/mongoHelper/connect');
const Doc = require('./doc');
const mongoose = require('mongoose');
const ObjectID = require('mongodb').ObjectID;

const create = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  await mongoHelper.connectToDatabase();
  const doc = event.body;

  const newDoc = new Doc(doc);

  const r = await newDoc.save()
    .then(() => {
      return ({ statusCode: 200, body: JSON.stringify(newDoc) });
    })
    .catch(e => {
      return ({ statusCode: 500, body: JSON.stringify({ error: e }) });
    });
  await mongoose.disconnect();
  return r;
}

module.exports.create = middy(create).use(jsonBodyParser()).use(cors()).use(httpErrorHandler());

const edit = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  await mongoHelper.connectToDatabase();
  const u = event.body;

  const update = {}
  u.title ? update.title = u.title : null;
  u.content ? update.content = u.content : null;
  u.elements ? update.elements = u.elements : null;

  const r = await Doc.updateOne({ _id: new ObjectID(u.id) }, { $set: update })
    .exec()
    .then(() => {
      return ({ statusCode: 200, body: JSON.stringify("Doc updated") });
    })
    .catch(e => {
      return ({ statusCode: 500, body: JSON.stringify({ error: e }) });
    });
  await mongoose.disconnect();
  return r;
}

module.exports.edit = middy(edit).use(jsonBodyParser()).use(cors()).use(httpErrorHandler());

const del = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  await mongoHelper.connectToDatabase();

  const id = event.pathParameters.id;
  const r = await Doc.deleteOne({ _id: new ObjectID(id) })
    .exec()
    .then(() => {
      return ({ statusCode: 200, body: JSON.stringify("Doc deleted") });
    })
    .catch(e => {
      return ({ statusCode: 500, body: JSON.stringify({ error: e }) });
    });
  await mongoose.disconnect();
  return r;
}

module.exports.delete = middy(del).use(cors()).use(httpErrorHandler());

const getAll = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  await mongoHelper.connectToDatabase();

  const r = await Doc.find()
    .exec()
    .then(docs => {
      return ({ statusCode: 200, body: JSON.stringify(docs) });
    })
    .catch(e => {
      return ({ statusCode: 500, body: JSON.stringify({ error: e }) });
    })
  await mongoose.disconnect();
  return r;
}

module.exports.getAll = middy(getAll).use(cors()).use(httpErrorHandler());

const get = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  await mongoHelper.connectToDatabase();

  const id = event.pathParameters.id;

  const r = await Doc.findById(id)
    .exec()
    .then(doc => {
      return ({ statusCode: 200, body: JSON.stringify(doc) });
    })
    .catch(e => {
      return ({ statusCode: 500, body: JSON.stringify({ error: e }) });
    })
  await mongoose.disconnect();
  return r;
}

module.exports.get = middy(get).use(cors()).use(httpErrorHandler());

const find = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  await mongoHelper.connectToDatabase();

  const keyWord = event.pathParameters.keyword;

  try {
    const doc = await Doc.find({
      "$or": [
        { title: { "$regex" : keyWord, "$options": "i" } },
        { "elements.content" : { "$regex": keyWord, "$options": "i" } }
    ]
    }).lean().exec();
    await mongoose.disconnect();
    return ({ statusCode: 200, body: JSON.stringify(doc.map(d => ({ id: d._id, title: d.title, content: d.content }))) });
  } catch(e) {
    console.error(e);
    await mongoose.disconnect();
    return ({ statusCode: 500, body: JSON.stringify({ error: e }) });
  }
}

module.exports.find = middy(find).use(cors()).use(httpErrorHandler());