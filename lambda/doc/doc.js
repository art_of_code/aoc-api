const mongoose = require('mongoose');
const validator = require('validator');

const model = mongoose.models.Doc || mongoose.model('Doc', {
  title: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: false,
  },
  elements: {
    type: [this],
    required: false,
  }
});

module.exports = model;