# AOC api

Documentation https://documenter.getpostman.com/view/2422713/SVtSXqNL

# Localhost
- Install npm with your packet manager and update to latest version with `npm install -g npm@latest`
- `npm install`
- `npm install -g typescript`
- `npm install -g serveless`
- `tsc`
- run `sls offline --stage local` if you are lucky if not `sls offline --stage local --skipCacheInvalidation`

A postman collection of all the local route: https://www.getpostman.com/collections/d32df883b8e07e294ca9

If you need a local mongo `docker run -it -p 27017:27017 mongo`

# New rule
- Add in index.ts classes that extend Verifier, with a whiteList to apply to certain parts of the ast
- export an async function with a const rules containing every class of the rule
- add the new rule in the function generatesRules with its dependencies
- add the new rule in serverless.yml at the bottom
- add the index.ts in tsconfig.json