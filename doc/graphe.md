
# AST (Abstract Syntax Tree)

L'AST est le centre même des règles d'Art of Code.


## La base

Tout les objets de l'AST ont tous les propriétés suivantes:  
{  
    type: string  
    start: number // la localisation en charactères du block  
    end: number   // la localisation en charactères de fin du block  
    loc {         // la localisation en position du block  
        start {  
            line: number  
            column: number  
        }  
        end {  
            line: number  
            column: number  
        }  
    }  
}  


## Les Statements

Program {
    body: [ Statement ];
}
Premier block de l'AST. Il equivaut au scope global du programe.

EmptyStatement {
}
Il est utilisé pour faire des boucles sans scope
ex: for (var i = 0; i < 10; i += 3); // à la fin de la boucle, i === 12.
La plupart des autres cas d'usage sont des mauvais cas d'usage (à lister).
ex: if (1); // if sans interet.

BlockStatement {
    body: [ Statement ];
}
Il liste tout les statements qui se trouvent entre {}.
Il génère un scope simple (voir dans la partie "Scope & Storage").

ExpressionStatement { 
    expression: Expression;
}
Une Expression considérer comme Statement.

IfStatement {
    test: Expression;
    consequent: Statement;
    alternate: Statement | null;
}
consequent => condition === true
alterate => condition === false
alternate null si pas de else
la conbimaison else if est considérée comme un IfStatement dans un alternate.

LabeledStatement {
    label: Identifier;
    body: Statement;
}

BreakStatement {
    label: Identifier | null;
}
label peut contenir un Identifier si celui-ci break sur un label.
Le parser throw si le label n'existe pas (à vérifier).

ContinueStatement {
    label: Identifier | null;
}
label peut contenir un Identifier si celui-ci break sur un label.
Le parser throw si le label n'existe pas (à vérifier).

WithStatement {
    object: Expression;
    body: Statement;
}

SwitchStatement {
    discriminant: Expression;
    cases: [ SwitchCase ];
    lexical: boolean;
}

SwitchCase {
    test: Expression | null;
    consequent: [ Statement ];
}
SwitchCase n'est utilisé que dans SwitchStatement.
test === null => default

ReturnStatement {
    argument: Expression | null;
}

ThrowStatement {
    argument: Expression;
}

TryStatement {
    block: BlockStatement;
    handler: CatchClause | null;
    guardedHandlers: [ CatchClause ]; ??
    finalizer: BlockStatement | null;
}
handler => catch
finalizer => finally
Il y a au moins un handler ou un finalizer.

CatchClause {
    param: Pattern;
    guard: Expression | null;
    body: BlockStatement;
}
CatchClause n'est pas considéré comme un Statement. Il ne se trouve que dans des TryStatement.

WhileStatement {
    test: Expression;
    body: Statement;
}

DoWhileStatement {
    test: Expression;
    body: Statement;
}

ForStatement {
    init: VariableDeclaration | Expression | null;
    test: Expression | null;
    update: Expression | null;
    body: Statement;
}

ForInStatement {
    left: VariableDeclaration | Expression;
    right: Expression;
    body: Statement
}

ForOfStatement {
    left: VariableDeclaration | Expression;
    right: Expression;
    body: Statement;
}


## Les Declaration

Les déclaration sont considérer comme des Statements.

FunctionDeclaration {
    id: Identifier;
    params: [ Pattern ];
    defaults: [ Expression ];
    rest: Identifier | null;
    generator: boolean;
    expression: boolean;
    body: BlockStatement | Expression;
}

VariableDeclaration {
    declarations: [ VariableDeclarator ];
    kind: "var" | "let" | "const";
}

VariableDeclarator {
    id: Pattern;
    init: Expression | null;
}
VariableDeclarator n'est pas considérer comme une déclaration car 


## Les Expressions

ThisExpression {
}

ArrayExpression {
    elements: [ Expression | null ];
}

ObjectExpression {
    properties: [ Property ];
}

Property {
    key: Literal | Identifier;
    value: Expression;
    kind: "init" | "get" | "set";
}
Property n'est pas considéré comme une Expression. Il n'est utilisé que dans ObjectExpression et ObjectPattern.

FunctionExpression {
    id: Identifier | null;
    params: [ Pattern ];
    defaults: [ Expression ];
    rest: Identifier | null;
    generator: boolean;
    expression: boolean;
    body: BlockStatement | Expression;
}

ArrowFunctionExpression {
    params: [ Pattern ];
    defaults: [ Expression ];
    rest: Identifier | null;
    generator: boolean;
    expression: boolean;
    body: BlockStatement | Expression;
}

SequenceExpression {
    expressions: [ Expression ];
}

UnaryExpression {
    operator: UnaryOperator;
    prefix: boolean;
    argument: Expression;
}

BinaryExpression {
    operator: BinaryOperator;
    left: Expression;
    right: Expression;
}

AssignmentExpression {
    operator: AssignmentOperator;
    left: Pattern;
    right: Expression;
}

UpdateExpression {
    operator: UpdateOperator;
    argument: Expression;
    prefix: boolean;
}

LogicalExpression {
    operator: LogicalOperator;
    left: Expression;
    right: Expression;
}

ConditionalExpression {
    test: Expression;
    consequent: Expression;
    alternate: Expression;
}

NewExpression {
    callee: Expression;
    arguments: [ Expression ];
}

CallExpression {
     Expression;
     arguments: [ Expression ];
}

MemberExpression {
    object: Expression;
    property: Identifier | Expression;
    computed: boolean;
}

## Les Patterns

Les Patterns sont utilisé dans les déclarations de variables (paramètre de fonction ou VariableDeclarator).

ObjectPattern {
    properties: [ Property ];
}

ArrayPattern {
    elements: [ Pattern | null ];
}



## Les Fonctions

Il y a 3 types de fonctions:
- FunctionDeclaration
- FunctionExpression
- ArrowFunctionExpression

FunctionDeclaration génère 1 à 2 lieux de stockage de variables:
- public (accessible via this). ATTENTION: dépend du mode !
- private (accessible directement via l'identifier).

FunctionExpression génère 1 ou 2 lieux de stockage de variables (dépend du context):
- private (accessible directement via l'identifier).
- public seulement si pas déclaré dans CallExpression ou NewExpression. ATTENTION: dépend du mode !

ArrowFunctionExpression génère 1 lieux de stockage de variables:
- private (accessible directement via l'identifier).

les modes:
execution de fichier:
les lieux public reste inchangé (comme ecrit au dessus).
console:
les lieux publics (pour FunctionDeclaration et FunctionExpression) ne sont génèrés que dans les NewExpression.


## Les Control Flow

IfStatement
WhileStatement
DoWhileStatement
ForStatement
ForInStatement
ForOfStatement
SwitchStatement
SwitchCase
TryStatement
CatchClause

LabeledStatement
BreakStatement
ContinueStatement


## Autres

Identifier {
    name: string;
}

Literal {
    value: string | boolean | null | number;
    raw: string;
    extra: {
        rawValue: string | boolean | null | number;
        raw: string;
    };
}

TemplateLiteral {
    expressions: [ Expression ];
    quasis: [ TemplateElement ];
}
Les templateLiterals sont entre ``. Il permet l'intégration d'Expressions à évaluer.
ex: `10 * 10 = ${10 * 10}`.

TemplateElement {
    value: {
        raw: string;
        cooked: string;
    };
    tail: boolean;
}
Les TemplateElements ne sont utilisés que dans les TemplateLiterals.

enum UnaryOperator {
    "-" | "+" | "!" | "~" | "typeof" | "void" | "delete"
}

enum BinaryOperator {
    "==" | "!=" | "===" | "!=="
         | "<" | "<=" | ">" | ">="
         | "<<" | ">>" | ">>>"
         | "+" | "-" | "*" | "/" | "%"
         | "|" | "^" | "&" | "in"
         | "instanceof" | ".."
}

enum LogicalOperator {
    "||" | "&&"
}

enum AssignmentOperator {
    "=" | "+=" | "-=" | "*=" | "/=" | "%="
        | "<<=" | ">>=" | ">>>="
        | "|=" | "^=" | "&="
}

enum UpdateOperator {
    "++" | "--"
}
