const express = require('express');
const http = require('http');
const socketio = require('socket.io');

class ServerTest {

  constructor() {
    this._expressInstance = express();
    this._httpServer = http.createServer(this._expressInstance);
    this._io = socketio(this._httpServer);


    this._io.on('connection', (socket) => {
      console.log('Client connected !! cool');
      
    })
  }

  async start() {
    new Promise((resolve, reject) => {
      this._httpServer.listen(8080, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(this);
        }
      });
    });
  }

  static bootstrap() { return new ServerTest(); }

};

module.exports = ServerTest.bootstrap();