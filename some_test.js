let unused_var = false;
let used_var_in_deadcode = 9;
let unwriteValue_var;
let tab = ["EIP", "Epitech"];
let int_var = 9;
const const_var = 42;
let i = 47;

function print(f) {
    console.log(f);
}

function bla()
{
    undeclared_var = true; // this var is undeclared
    const_var = 0; // modif const variable
    i = 65;

    return (const_var); // deadcode after return
    if (used_var_in_deadcode === 9) {
        return (0);
    }
}

function foo()
{
    if (int_var === tab) { // Cmp between diff type
        print("int === tab");
    }
}

function bar()
{
    while (int_var === 8) // Loop condition always falss
    {
        int_var += 1;
    }
    zaz()
}

function zaz()
{
    if (int_var === 9){ // Condition always true
        print("int_var === 9");
    }
    else {
        print("int_var != 9");
    }
}

function malform_if()
{
    let variable = 8;

    if (variable == 6) { // nothing in if and use of ==
    }
    else if (variable === 8) { // nothing in else if
    }

    if (true) {
        return;
    } else {
        return;
    }
    malform_if(1);
}

function aled(a) {
    switch (a) {
        case 1:
            print("a is 1");
            break;
        case 1:
            print("a is 1"); // Same case
            break;
        case 3:
            printaif("a is 3"); // function undefined
            break;
        default:
            break;
    }
}

function test()
{
    for (let j = 0; j <= i; j++) {
        console.log(j);
    }
}

function add(a, a) { // same arguments name
    return (a + b); // b never declared
}

add (1, 3, 6); // 3 arguments instead of 2
