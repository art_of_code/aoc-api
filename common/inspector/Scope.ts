import { Infos } from './Infos';
import {Regenerable, Regen, RegenFactory, Indexer, Indexable} from './RegenFactory';

export class Arg extends Indexable {
    name: string;
    value: any;

    constructor(name: string = "", value: any = null) {
        super("Arg", "_allArgs");
        this.name = name;
        this.value = value;
    }

    stringify() {
        return {...this, value: null};
    }

    static allArgs: Arg[] = [];
}
RegenFactory.regenClasses.set("Arg", new Regen(Arg));

export class Var extends Indexable {
    static idIt: number = 0;
    id: number;
    name: string;
    kind: string; /** let, const, var, agument, func, unknow */
    value: any;
    read: any[]; /** get any use of the var */
    write: any[]; /** get any modification done to the var */
    isRead: boolean = false;
    isWrite: boolean = false;

    constructor(name: string = "", value: any = null, kind: string = undefined) {
        super("Var", "_allVars");
        this.id = Var.idIt++;
        this.name = name;
        this.kind = kind;
        this.value = value;
        this.write = [];
        this.read = [];
        this.isRead = false;
    }

    stringify() {
        return {...this, value: null};
    }

    static allVars: Var[] = [];
}
RegenFactory.regenClasses.set("Var", new Regen(Var));

export class Func extends Indexable {
    static idIt = 0;
    id: number;
    name: string;
    value: any;
    returns: any[]; /** ?? */

    constructor(name: string = "", value: any = null) {
        super("Func", "_allFuncs");
        this.id = Func.idIt++;
        this.name = name;
        this.value = value;
        this.returns = [];
    }

    stringify() {
        return {...this, value: null};
    }

    static allFuncs: Func[] = [];
}
RegenFactory.regenClasses.set("Func", new Regen(Func));

export class FuncCall extends Indexable {
    name: string;
    value: any;
    args: Arg[];

    constructor(name: string = "", value: any = null, args: Arg[] = []) {
        super("FuncCall", "_allFuncCalls");
        this.name = name;
        this.value = value;
        this.args = args;
    }

    stringify() {
        return {...this, value: null};
    }

    static allFuncCalls: FuncCall[] = [];
}
RegenFactory.regenClasses.set("FuncCall", new Regen(FuncCall));

/**
 * args: argument du block
 * funcs: function declaré dans le block
 * vars: variable declaré dans le block
 * funcCalls: function appelé dans le block
 * value: block
 * type simple:
 */
let scopeUniqueId = 0;
export class Scope extends Indexable {
    name: string;
    id: number;
    type: string; /** "simple" | "strong" */
    value: any;
    parent: number;
    args: Arg[];
    funcs: Func[];
    vars: Var[];
    funcCalls: FuncCall[];

    constructor(name: string = "", type: string = "simple", value: any = null, parent: number = null, args: Var[] = [], funcs: Func[] = [], vars: Var[] = [], funcCalls: FuncCall[] = []) {
        super("Scope", "_allScopes");
        this.name = name;
        this.id = scopeUniqueId++;
        this.type = type;
        this.value = value;
        this.parent = parent;
        this.args = args;
        this.funcs = funcs;
        this.vars = vars;
        this.funcCalls = funcCalls;
    }

    prev(infos: Infos): Scope {
        return infos._allScopes.find((s: Scope) => {
            return s.id === this.parent;
        })
    }

    insertVar(v: Var, infos: Infos, strong: boolean) {
        if (strong === true) {
            for (let scopeIt: Scope = this; scopeIt; scopeIt = scopeIt.prev(infos)) {
                if (scopeIt.type === "strong") {
                    scopeIt.vars.push(v);
                    break;
                }
            }
        } else {
            this.vars.push(v);
        }
        Indexer.index(infos, v);
    }

    /** type: "global" | "strong" | simple" */
    findVar(name: string, infos: Infos, type: string): {value: Var, scope: Scope} {
        if (type === "simple") {
            let v: Var = this.vars.find((_v: Var) => {
                return _v.name === name;
            });
            if (v) {
                return {value: v, scope: this};
            }
        } else {
            for (let scopeIt: Scope = this; scopeIt; scopeIt = scopeIt.prev(infos)) {
                let v: Var = scopeIt.vars.find((_v: Var) => {
                    return _v.name === name;
                });
                if (v)
                    return {value: v, scope: scopeIt};
                if (type === "strong" && scopeIt.type === "strong")
                    break;
            }
        }
    }

    /** type: "global" | "strong" | simple" */
    findFunc(name: string, infos: Infos, type: string): {value: Func, scope: Scope} {
        if (type === "simple") {
            let v: Func = this.funcs.find((_v: Func) => {
                return _v.name === name;
            });
            if (v) {
                return {value: v, scope: this};
            }
        } else {
            for (let scopeIt: Scope = this; scopeIt; scopeIt = scopeIt.prev(infos)) {
                let v: Func = scopeIt.funcs.find((_v: Func) => {
                    return _v.name === name;
                });
                if (v)
                    return {value: v, scope: scopeIt};
                if (type === "strong" && scopeIt.type === "strong")
                    break;
            }
        }
    }

    static findFunc_loc(block: any, infos: Infos) {
        return infos["_allFuncs"].find(f => {
            return f.value.start === block.start
            && f.value.end === block.end;
        });
    }

    static findScope_loc(block: any, infos: Infos, type: string = "simple") {
        for (let i = infos["_allScopes"].length-1; i >= 0; --i) {
            let scope = infos["_allScopes"][i];
            if (block.start >= scope.value.start
            && block.end <= scope.value.end) {
                if (type === "strong") {
                    while (scope && scope.parent !== null && !["Program", "FunctionDeclaration", "FunctionExpression", "ArrowFunctionExpression"].find(x => x === scope.value.type)) {
                        scope = infos._allScopes[scope.parent];
                    }
                }
                return scope || null;
            }
        }
        return null;
    }

    stringify() {
        return {...this, value: null
            , funcs: this.funcs.map(o => o.stringify())
            , vars: this.vars.map(o => o.stringify())
            , funcCalls: this.funcCalls.map(o => o.stringify())
            , args: this.args.map(o => o.stringify())};
    }
}
RegenFactory.regenClasses.set("Scope", new Regen(Scope, (o: Scope) => {
    return o.regenMember("args")
            .regenMember("funcs")
            .regenMember("vars")
            .regenMember("funcCalls");
}));
Indexer.serializers["_allScopes"] = (o: Scope) => {
    Indexer.serialize(o, "args");
    Indexer.serialize(o, "funcs");
    Indexer.serialize(o, "vars");
    Indexer.serialize(o, "funcCalls");
};