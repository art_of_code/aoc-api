import { Block } from '../graph/Block';
import { Variable } from "../graph/Variables";
import { AssignmentExpression, Expression, UpdateExpression, BinaryExpression, Identifier, CallExpression } from '../graph/Expressions';
import { Infos } from './Infos';

abstract class ConstantDeductorResult {
  private _variable: any;
  abstract isDeductible(): boolean;
  getVariable() { return this._variable as Variable; }
  get(): any { return undefined; }
}

class Echec extends ConstantDeductorResult {
  isDeductible() { return false; }
}

class Success extends ConstantDeductorResult {
  constructor(private _value: any) { super(); }
  isDeductible() { return true; }
  get() { return this._value; }
}

export function deductExpression(infos: Infos, exp: any, cache: Array<Success | Echec> = []) {
  if (!exp) {
    return new Echec();
  }
  
  if (ConstantDeductor.isLiteral(exp)) {
    return new Success(exp.value);
  } else if (exp instanceof Identifier || exp.type === "Identifier") {
    let v = cache.find(v => v.getVariable().name === exp.name);

    if (!!v && v.isDeductible()) {
      return new Success(v.get());
    } else if (!v) {
      return deductConstant(infos, exp, exp.name, cache);
    } else {
      return v
    }
  } else if (exp instanceof BinaryExpression) {
    const b = new BinaryExpressionInterpreter(infos, exp, cache);

    return b.interpret();
  } else if (exp instanceof CallExpression) {
    let func = infos['allFuncs'].find(f => f.name === (exp.callee as any).name);

    if (
      !(func.returns instanceof Array) ||
      func.returns.length <= 0 ||
      !func.returns[0].argument
    ) {
      return new Echec();
    }

    if (!!func) {
      let args = func.params.map((p: Identifier, i: number) => {
        let r = deductExpression(infos, exp.arguments[i], cache);
        (r as any)._variable = p.variable;
        return r;
      }).map((arg: Success | Echec, _i: number, source: Array<typeof arg>) => {
        if (arg.getVariable().writeExpr.length > 0) {
          arg = deductConstant(infos, func.returns[0].argument, arg.getVariable().name, source)
        }
        return arg
      });


      return deductExpression(infos, func.returns[0].argument, args);
    }
  }
  return new Echec();
}

export class BinaryExpressionInterpreter {

  constructor(
    private _infos: Infos,
    private _exp: BinaryExpression,
    private _cache: Array<Success | Echec>
  ) {}

  interpret() {
    if (!this._exp) {
      return new Echec();
    }

    const left = deductExpression(this._infos, this._exp.left, this._cache);
    const right = deductExpression(this._infos, this._exp.right, this._cache);

    if (left.isDeductible() && right.isDeductible()) {
      let interpreter = [
        { op: '+', interpreter: () => left.get() + right.get() },
        { op: '-', interpreter: () => left.get() - right.get() },
        { op: '%', interpreter: () => left.get() % right.get() },
        { op: '*', interpreter: () => left.get() * right.get() },
        { op: '/', interpreter: () => left.get() / right.get() },
        { op: '&', interpreter: () => left.get() & right.get() },
        { op: '|', interpreter: () => left.get() | right.get() },
        { op: '<<', interpreter: () => left.get() << right.get() },
        { op: '>>', interpreter: () => left.get() >> right.get() },
      ].find(inter => inter.op === this._exp.operator)?.interpreter;

      if (!!interpreter) {
        return new Success(interpreter());
      } else {
        return new Echec();
      }
    } else {
      return new Echec();
    }
  }

}

export default class ConstantDeductor {
  private _currentValue: Success | Echec = new Echec();

  static isLiteral(block: any): boolean {
    const literals = [
      "RegExpLiteral",
      "NullLiteral",
      "StringLiteral",
      "BooleanLiteral",
      "NumericLiteral",
      "Literal",
    ];

    return !!block && !!(literals.find(l => l === block.type));
  }

  constructor(private _infos: Infos, private _referenciel: any) {}

  private _updateCurrentValue(v: any) {
    if (this._currentValue instanceof Success) {
      (this._currentValue as any)._value = v;
    } else {
      this._currentValue = new Success(v);
    }
    return this._currentValue;
  }

  private _assignExpr(exp: AssignmentExpression) {
    const right = deductExpression(this._infos, exp.right, [ this._currentValue ]);

    if (right.isDeductible() && (this._currentValue.isDeductible() || exp.operator === '=')) {
      const cur = this._currentValue;
      let interpreter = [
        { op: '=', interpreter: () => right.get() },
        { op: '+=', interpreter: () => cur.get() + right.get() },
        { op: '-=', interpreter: () => cur.get() - right.get() },
        { op: '%=', interpreter: () => cur.get() % right.get() },
        { op: '*=', interpreter: () => cur.get() * right.get() },
        { op: '/=', interpreter: () => cur.get() / right.get() },
        { op: '&=', interpreter: () => cur.get() & right.get() },
        { op: '|=', interpreter: () => cur.get() | right.get() },
        { op: '<<=', interpreter: () => cur.get() << right.get() },
        { op: '>>=', interpreter: () => cur.get() >> right.get() },
      ].find(inter => inter.op === exp.operator)?.interpreter;

      if (!!interpreter) {
        return new Success(interpreter());
      } else {
        return new Echec();
      }
    }
    return new Echec();
  }

  private _updateExpr(exp: UpdateExpression) {
    if (this._currentValue instanceof Echec) {
      // Cannot incr unknow value...
      return this._currentValue;
    }
    
    if (exp.operator === '++') {
      return this._updateCurrentValue(this._currentValue.get() + 1);
    } else if (exp.operator === '--') {
      return this._updateCurrentValue(this._currentValue.get() - 1);
    }
    return new Echec();
  }

  private _getValueForExpression(exp: Expression | any) {
    if (exp instanceof AssignmentExpression) {
      return this._assignExpr(exp);
    } else if (exp instanceof UpdateExpression) {
      return this._updateExpr(exp);
    }

    return new Echec();
  }

  private _deduct(fVar: Variable) {
    fVar.writeExpr.forEach(e => {
      const expression = e.statementLoc['expression'];

      if (
        (expression.loc.start.line > this._referenciel.loc.start.line) ||
        (
          (expression.loc.start.line === this._referenciel.loc.start.line) &&
          (expression.loc.start.column > this._referenciel.loc.start.column)
        )
      ) { return; }

      (this._currentValue as any)._variable = fVar;
      this._currentValue = this._getValueForExpression(expression);
    });
    (this._currentValue as any)._variable = fVar;
  }

  val(varName: string, cache: Array<Success | Echec> = []) {
    if (!this._referenciel || !varName) {
      return new Echec();
    }

    const localVar = this._referenciel.variables || [];
    const scopeVar = this._referenciel.getScope().variables || [];
    let fVar: Variable | undefined;
    
    // init currentValue
    this._currentValue = cache.find(v => v.getVariable().name === varName) || new Echec();

    if (!!(fVar = localVar.find(v => v.name === varName))) {
      this._deduct(fVar);
    } else if (!!(fVar = scopeVar.find(v => v.name === varName))) {
      this._deduct(fVar);
    } else if (
      (this._referenciel as any)?.name === varName
      && !!((this._referenciel as any)?.variable)
    ) {
      this._deduct((this._referenciel as any).variable)
    }
    return this._currentValue;
  }
}

export function deductConstant(infos: Infos, referenciel: any, varName: string, cache: Array<Success | Echec> = []) {
  return (new ConstantDeductor(infos, referenciel)).val(varName, cache);
}