
export var classDict = {};

function getDictProp(prop: string) {
    return 'all'+prop+'s';
}

/* serialize a dictionary 
 * /!!\ ALL properties that begin with '_' will be remove from the dictionnary
 */
export function serializeDict(dict: any) {
    for (const prop in dict) {
        if (prop[0] !== '_') {
            if (dict[prop] instanceof Array) {
                dict[prop].forEach(e => serialize(dict, e));
            } else {
                dict[prop] = serializeProp(dict, dict[prop]);
            }
        } else {
            delete dict[prop];
        }
  }
  return dict;
}

function serialize(dict, o: any) {
    if (typeof o === 'object' && o !== null) {
        for (const prop in o) {
            if (prop[0] !== '_') {
                o[prop] = serializeProp(dict, o[prop]);
            } else {
                delete o[prop];
            }
        }
    }
    return o;
}

function serializeProp(dict: any, o: any) {
    if (typeof o === 'object' && o !== null) {
        if (o instanceof Array) {
            return o.map(e => serializeProp(dict, e));
        }
        if (o.it__ && dict[getDictProp(o.it__)]) {
            const i = dict[getDictProp(o.it__)].indexOf(o);
            if (i !== -1) {
                return { cn__: o.it__, i: i, rt__: 'idx__' };
            }
        } else {
            serialize(dict, o);
        }
    }
    return o;
}

/** regenerate a dictionary */
export function regenDict(dict: any) {
    for (const prop in dict) {
        if (dict[prop] instanceof Array) {
            dict[prop] = dict[prop].map(e => regenIndexable(dict, e));
        }
    }
    Object.keys(dict).forEach((prop) => {
        if (dict[prop] instanceof Array) {
            dict[prop].forEach(e => regen(dict, e));
        } else {
            dict[prop] = regenProp(dict, dict[prop]);
        }
    });
    return dict;
}

function regenIndexable(dict: any, o: any) {
    if (typeof o === 'object' && o !== null && typeof o.it__ === 'string') {
        let obj = new (classDict[o.it__])();
        Object.keys(o).forEach((key) => {
            obj[key] = o[key];
        });
        return obj;
    }
    return o;
}

function regen(dict: any, o: any) {
    if (typeof o === 'object' && o !== null) {
        Object.keys(o).forEach((key) => {
            o[key] = regenProp(dict, o[key]);
        });
    }
}

var bla = [];

function regenProp(dict: any, o: any) {
    if (!(typeof o === 'object' && o !== null)) {
        return o;
    }
    if (o instanceof Array) {
        return o.map(e => regenProp(dict, e));
    }
    if (o.rt__ === 'idx__') {
        return getIndexedProp(dict, o);
    }
    if (classDict[o.rt__]) {
        let obj = new classDict[o.rt__]();
        Object.keys(o).forEach((key) => {
            obj[key] = o[key];
        });
        return obj;
    } else {
        regen(dict, o);
        return o;
    }
}

function getIndexedProp(dict: any, o: any) {
    const dictProp = getDictProp(o['cn__']);
    if (dict[dictProp] instanceof Array) {
        return dict[dictProp][o.i];
    } else {
        return o;
    }
}

/**
 * need to add after each Regenerable class:
 * classDict['<ClassName>'] = <ClassName>
 */
export class Regenerable {
    rt__: string;

    constructor(classType: string) {
        this.rt__ = classType;
    }
}

/**
 * need to add after each Regenerable class:
 * classDict['<ClassName>'] = <ClassName>
 */
export class Indexable {
    it__: string;

    constructor(dict: any | undefined, classType: string) {
        if (dict) {
            const dictProp = getDictProp(classType);
            this.it__ = classType;
            if (!dict[dictProp]) {
                dict[dictProp] = [];
            }
            dict[dictProp].push(this);
        }
    }
}
