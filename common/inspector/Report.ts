import { Loc } from "../graph/Block";

export class Report {
    report(msg: ErrorMessage) {
        if (!this[msg.code])
            this[msg.code] = [];
        this[msg.code].push(msg);
    }
}

export class ErrorMessage {
    type: string;
    code: string;
    msg: string;
    loc: Loc;
    info: boolean;
    steps: ErrorStep[];

    constructor(
            type: string,
            code: string,
            msg: string,
            loc: Loc | any = Loc.noLoc(),
            info: boolean = false,
            steps: ErrorStep[] = null) {
        this.type = type;
        this.code = code;
        this.msg = msg;
        this.loc = loc;
        this.info = info;
        this.steps = steps;
    }
}

export class ErrorStep {
	msg: string;
	loc: Loc;
}