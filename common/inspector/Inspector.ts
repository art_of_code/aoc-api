
var debug = false;
var indent = 0;

import {
    Scope,
    Arg,
    FuncCall,
} from "./Scope";

import { Infos, Verifier } from './Infos';
import { ErrorMessage } from './Report';

function printDebug(arg:any) {
    if (debug) {
        console.log("  ".repeat(indent)+arg);
    }
}

const inspectFunctions = {

	File: (block: any, infos: Infos) => {
		inspectElement(block.program, infos);
	},

    Identifier: (block: any, infos: Infos) => {},

    Literal: (block: any, infos: Infos) => {},

    NumericLiteral: (block: any, infos: Infos) => {},

    BooleanLiteral: (block: any, infos: Infos) => {},

    StringLiteral: (block: any, infos: Infos) => {},

    NullLiteral: (block: any, infos: Infos) => {},

    TemplateLiteral: (block: any, infos: Infos) => {},

    RegExpLiteral: (block: any, infos: Infos) => {},

    ImportDeclaration: (block: any, infos: Infos) => {},

    Program: (block: any, infos: Infos) => {
        inspectArray(block.body, infos);
    },

    ExpressionStatement: (block: any, infos: Infos) => {
        inspectElement(block.expression, infos);
    },

    Directive: null, /** @TODO ?? */

    BlockStatement: (block: any, infos: Infos) => {
        inspectArray(block.body, infos);
    },

    EmptyStatement: (block: any, infos: Infos) => {},

    DebuggerStatement: null,

    WithStatement: (block: any, infos: Infos) => {
        inspectElement(block.body, infos);
    },

    /** CONTROL FLOW */

    ReturnStatement: (block: any, infos: Infos) => {
        inspectElement(block.argument, infos);
    },

    LabeledStatement: (block: any, infos: Infos) => {
        inspectElement(block.label, infos);
        inspectElement(block.body, infos);
    },

    BreakStatement: (block: any, infos: Infos) => {
        if (block.label) {
            inspectElement(block.label, infos);
        }
    },

    ContinueStatement: (block: any, infos: Infos) => {
        if (block.label) {
            inspectElement(block.label, infos);
        }
    },

    /** CHOICE */

    IfStatement: (block: any, infos: Infos) => {
        inspectElement(block.test, infos);
        inspectElement(block.consequent, infos);
        if (block.alternate) {
            inspectElement(block.alternate, infos);
        }
    },

    SwitchStatement: (block: any, infos: Infos) => {
        inspectElement(block.discriminant, infos);
        inspectArray(block.cases, infos);
    },

    SwitchCase: (block: any, infos: Infos) => {
        if (block.test) {
            inspectElement(block.test, infos);
        }
        inspectArray(block.consequent, infos);
    },

    /** EXCEPTIONS */

    ThrowStatement: (block: any, infos: Infos) => {
        inspectElement(block.argument, infos);
    },

    TryStatement: (block: any, infos: Infos) => {
        inspectElement(block.block, infos);
        if (block.handler) {
            inspectElement(block.handler, infos);
        }
        if (block.finallizer) {
            inspectElement(block.finallizer, infos);
        }
    },

    CatchClause: (block: any, infos: Infos) => {
        inspectElement(block.param, infos);
        inspectElement(block.body, infos);
    },

    /** LOOPS */

    WhileStatement: (block: any, infos: Infos) => {
        inspectElement(block.test, infos);
        inspectElement(block.body, infos);
    },

    DoWhileStatement: (block: any, infos: Infos) => {
        inspectElement(block.test, infos);
        inspectElement(block.body, infos);
    },

    ForStatement: (block: any, infos: Infos) => {
        inspectElement(block.init, infos);
        inspectElement(block.test, infos);
        inspectElement(block.update, infos);
        inspectElement(block.body, infos);
    },

    ForInStatement: (block: any, infos: Infos) => {
        inspectElement(block.left, infos);
        inspectElement(block.right, infos);
        inspectElement(block.body, infos);
    },

    ForOfStatement: (block: any, infos: Infos) => {
        inspectElement(block.left, infos);
        inspectElement(block.right, infos);
        inspectElement(block.body, infos);
    },

    /** DECLARATIONS */

    FunctionDeclaration: (block: any, infos: Infos) => {
        if (block.id) {
            inspectElement(block.id, infos);
        }
        inspectArray(block.params, infos);
        inspectElement(block.body, infos);
    },

    VariableDeclaration: (block: any, infos: Infos) => {
        inspectArray(block.declarations, infos);
    },

    VariableDeclarator: (block: any, infos: Infos) => {
        if (block.id)
            inspectElement(block.id, infos);
        if (block.init)
            inspectElement(block.init, infos);
    },

    ClassDeclaration: (block: any, infos: Infos) => {
        inspectElement(block.id, infos);
        if (block.superClass) {
            inspectElement(block.superClass, infos);
        }
        inspectElement(block.body, infos);
    },

    ClassBody: (block: any, infos: Infos) => {
        inspectArray(block.body, infos);
    },

    ClassMethod: (block: any, infos: Infos) => {
        inspectElement(block.key, infos);
        inspectElement(block.value, infos);
    },

    ClassProperty: (block: any, infos: Infos) => {
        inspectElement(block.key, infos);
        if (block.value) {
            inspectElement(block.value, infos);
        }
    },

    /** EXPRESSIONS */

    ThisExpression: (block: any, infos: Infos) => {},

    ArrayExpression: (block: any, infos: Infos) => {
        inspectArray(block.elements, infos);
    },

    ObjectExpression: (block: any, infos: Infos) => {
        inspectArray(block.properties, infos);
    },

    ObjectProperty: (block: any, infos: Infos) => {
        inspectElement(block.key, infos);
        inspectElement(block.value, infos);
    },

    /** @TODO */
    // ObjectMethod: (block: any, infos: Infos) => {
    // },

    FunctionExpression: (block: any, infos: Infos) => {
        if (block.id) {
            inspectElement(block.id, infos);
        }
        inspectArray(block.params, infos);
        inspectElement(block.body, infos);
    },

    ArrowFunctionExpression: (block: any, infos: Infos) => {
        inspectArray(block.params, infos);
        inspectElement(block.body, infos);
    },

    UnaryExpression: (block: any, infos: Infos) => {
        inspectElement(block.argument, infos);
    },

    UpdateExpression: (block: any, infos: Infos) => {
        inspectElement(block.argument, infos);
    },

    BinaryExpression: (block: any, infos: Infos) => {
        inspectElement(block.left, infos);
        inspectElement(block.right, infos);
    },

    AssignmentExpression: (block: any, infos: Infos) => {
        inspectElement(block.right, infos);
        inspectElement(block.left, infos);
    },

    LogicalExpression: (block: any, infos: Infos) => {
        inspectElement(block.left, infos);
        inspectElement(block.right, infos);
    },

    MemberExpression: (block: any, infos: Infos) => {
        inspectElement(block.object, infos);
        inspectElement(block.property, infos);
    },

    ConditionalExpression: (block: any, infos: Infos) => {
        inspectElement(block.test, infos);
        inspectElement(block.consequent, infos);
        inspectElement(block.alternate, infos);
    },

    CallExpression: (block: any, infos: Infos) => {
        inspectElement(block.callee, infos);
        inspectArray(block.arguments, infos);
    },

    NewExpression: (block: any, infos: Infos) => {
        inspectElement(block.callee, infos);
        inspectArray(block.arguments, infos);
    },

    SequenceExpression: (block: any, infos: Infos) => {
        inspectArray(block.expressions, infos);
    },

    /** @TODO */
    // AssignmentPattern: (block: any, infos: Infos) => {
    //     inspectArray(block.properties, infos);
    // },

    ObjectPattern: (block: any, infos: Infos) => {
        inspectArray(block.properties, infos);
    },

}

function inspectArray(blocks: [any], infos: Infos) {
    blocks.forEach((e: any) => inspectElement(e, infos));
}

function inspectElement(block: any, infos: Infos) {
    if (!block) {
        return;
    }
    if (inspectFunctions[block.type]) {
        printDebug(block.type + ' {');
        ++indent;

        infos._verifiers.apply(infos, block);
        inspectFunctions[block.type](block, infos);
        infos._verifiers.endApply(infos, block);
        --indent;
        printDebug('}');
    } else {
        console.log('NO FOUND PARSER : ' + block.type);
        infos.report.report(new ErrorMessage("internal", "internal", `NO FOUND PARSER ${block.type}`, block.loc, true));
    }
}

const util = require('util');

export function inspectAST(ast:any, rules:[Verifier], oldInfos: any = undefined, _debug: boolean = false): Infos {
    debug = _debug;
    debug = false;

    let infos = new Infos(oldInfos);

    if (debug) {
        console.log("AST:");

        console.log(util.inspect(ast, false, null, true /* enable colors */));
        console.log("\n============================================\n");
    }

    infos._verifiers.addVerifiers(rules);

    inspectElement(ast, infos);

    if (debug) {
        ast.tokens = null; /** @DEBUG juste pour le print de debug */
        console.log("\n============================================\n");
        console.log("scope:");

        console.log(util.inspect(infos._allScopes.map(s => s.stringify()), false, null, true /* enable colors */));
    }

    return infos;
}
