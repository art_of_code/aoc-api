
// import { Var, Func, Scope, Arg, FuncCall } from './Scope';
import { Report } from './Report';

export class Infos {
    // _allScopes: Scope[] = [];
    // _currentScope: Scope = null;
    // _allVars: Var[] = [];
    // _allFuncs: Func[] = [];
    _verifiers: VerifierManager = new VerifierManager;
    report: Report = new Report;
    _expressionInfos: ExpressionInfos = new ExpressionInfos;

    constructor(oldInfos: any = undefined) {
        const blacklistedKey = ["ast", "report"];
        if (oldInfos) {
            for (let key of Object.keys(oldInfos)) {
                if (blacklistedKey.find(k => k === key)) {
                    continue;
                }
                this[key] = oldInfos[key];
            }
        }
    }

    // setCurrentScope(block: any): Scope {
    //     return this._currentScope = Scope.findScope_loc(block, this);
    // }

    getInfos(infosKeys: string[]) {
        return infosKeys.reduce((p, key) => {
            if (this[key]) {
                p[key] = this[key];
            }
            return p;
        }, {});
    }
}

export class ExpressionInfos {
    readMode: boolean = false;
    writeMode: boolean = false;
    expressionRecursion: number = 0;
    notAVar: boolean = false;
    // allowRecursiveExpression: boolean = true;
}

export class VerifierManager {
    verifiers: Verifier[] = [];
    allowRecursiveExpression: boolean = true;
    expressionRecursion: number = 0;

    apply(infos: Infos, block: any) {
        this.verifiers.forEach(verifier => {
            if (verifier.accept(block.type) && verifier.apply !== null) {
                verifier.apply(infos, block);
            }
        });
    }

    endApply(infos: Infos, block: any) {
        this.verifiers.forEach(verifier => {
            if (verifier.accept(block.type) && verifier.endApply !== null) {
                verifier.endApply(infos, block);
            }
        });
    }

    addVerifiers(verifiers: Verifier[]) {
        this.verifiers = verifiers.concat(this.verifiers);
    }

    addVerifier(verifier: Verifier) {
        this.addVerifiers([verifier]);
    }

    removeVerifier(verifier: Verifier) {
        this.verifiers.splice(this.verifiers.indexOf(verifier), 1);
    }
}

/**
 * un verifier peut être:
 * - global (par default);
 * - scoped (détruit quand on sort du scope);
 *
 * - appliqué en premier (avant la recursion) (par default);
 * - appliqué en dernier (après les recursions)
 *
 * un verifier peut:
 * - s'appliquer (en appelant le membre apply avec les bons arguments)
 * - se détruire de lui-même (permet de check une seul fois si nécessaire)
 * - avoir une 'white-list' (liste des blocks dans lesquels la fonction doit être appliquée)
 * - avoir une 'black-list' (list des blocks dans lesquels la fonction NE doit PAS être appliquée)
 *
 *
 */
export class Verifier {
    whiteList: string[] = [];
    blackList: string[] = [];
    scoped: any = null; /** scoped = block iff NOT a global Verifier. */
    apply: (infos: Infos, block: any) => void = null;
    endApply: (infos: Infos, block: any) => void = null;

    accept(type: string): boolean {
        if (this.whiteList.length !== 0) {
            return this.whiteList.find(e => e ==type) !== undefined;
        } else {
            return this.blackList.find(e => e == type) === undefined;
        }
    }
}

export class ScopedVerifier extends Verifier {
    _active: boolean = false;
    _scope: any;
    ctor: (infos: Infos, block: any) => void = null;
    dtor: (infos: Infos, block: any) => void = null;

    constructor(scope: any) {
        super();
        this._scope = scope;
        this.apply = function(infos: Infos, block: any) {
            if (this._active === false && block === this._scope) {
                this._active = true;
                this.ctor(infos, block);
            }
        }
        this.endApply = function(infos: Infos, block: any) {
            if (this._active === true && block === this._scope) {
                this.dtor(infos, block);
                infos._verifiers.removeVerifier(this);
            }
        }
    }
}
