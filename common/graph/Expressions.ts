
import { Infos } from '../inspector/Infos'
import { classDict, Indexable } from '../inspector/RegenFactory';
import { Loc, Func, Block } from './Block';
import { Variable, Types } from './Variables';
import { Statement } from './Statements';

export class Expression extends Indexable {
    type: string;
    statementLoc: Statement;
    blockLoc: Block; // defined if the expression is not in a statement
    staticTypes: Types;
    loc: Loc;

    constructor(infos?: Infos, type?: string, expression?: any) {
        super(infos, type);
        if (infos) {
            this.type = type;
            this.loc = new Loc(infos, expression);

            if (infos['_currentStatement']) {
                this.statementLoc = infos['_currentStatement'];
            } else {
                this.blockLoc = infos['_currentBlock'];
            }
        }
    }

    getScope(): Block {
        if (this.statementLoc) {
            return this.statementLoc.blockLoc;
        } else {
            return this.blockLoc;
        }
    }
}

export class Identifier extends Expression {
    name: string;
    variable: Variable;
    argument: boolean;
    notaVar: boolean; // defined when the identifier does not refer to a Variable (ex: memberExpression.property)

    constructor(infos?: Infos, name?: any, block?: any, variable?: Variable) {
        super(infos, 'Identifier', block);
        if (infos) {
            this.name = name;
            this.variable = variable;
        }
    }
}
classDict['Identifier'] = Identifier;

export class UnaryExpression extends Expression {
    operator: string;
    argument: Expression;

    constructor(infos?: Infos, block?: any, argument?: Expression) {
        super(infos, 'UnaryExpression', block);
        if (infos) {
            this.operator = block.operator;
            this.argument = argument;
        }
    }
}
classDict['UnaryExpression'] = UnaryExpression;

export class BinaryExpression extends Expression {
    operator: string;
    left: Expression;
    right: Expression;

    constructor(infos?: Infos, block?: any, left?: Expression, right?: Expression) {
        super(infos, 'BinaryExpression', block);
        if (infos) {
            this.operator = block.operator;
            this.left = left;
            this.right = right;
        }
    }
}
classDict['BinaryExpression'] = BinaryExpression;

export class UpdateExpression extends Expression {
    operator: string;
    prefix: boolean;
    argument: Expression;

    constructor(infos?: Infos, block?: any, argument?: Expression) {
        super(infos, 'UpdateExpression', block);
        if (infos) {
            this.operator = block.operator;
            this.argument = argument;
            this.prefix = block.prefix;
        }
    }
}
classDict['UpdateExpression'] = UpdateExpression;

export class AssignmentExpression extends Expression {
    operator: string;
    left: Expression;
    right: Expression;

    constructor(infos?: Infos, block?: any, left?: Expression, right?: Expression) {
        super(infos, 'AssignmentExpression', block);
        if (infos) {
            this.operator = block.operator;
            this.left = left;
            this.right = right;
        }
    }
}
classDict['AssignmentExpression'] = AssignmentExpression;

export class LogicalExpression extends Expression {
    operator: string;
    left: Expression;
    right: Expression;

    constructor(infos?: Infos, block?: any, left?: Expression, right?: Expression) {
        super(infos, 'LogicalExpression', block);
        if (infos) {
            this.operator = block.operator;
            this.left = left;
            this.right = right;
        }
    }
}
classDict['LogicalExpression'] = LogicalExpression;

export class MemberExpression extends Expression {
    object: Expression;
    property: Expression;
    computed: boolean;

    constructor(infos?: Infos, block?: any, object?: Expression, property?: Expression) {
        super(infos, 'MemberExpression', block);
        if (infos) {
            this.object = object;
            this.property = property;
            this.computed = block.computed;
        }
    }
}
classDict['MemberExpression'] = MemberExpression;

export class ConditionalExpression extends Expression {
    test: Expression;
    consequent: Expression;
    alternate: Expression;

    constructor(infos?: Infos, block?: any, test?: Expression, consequent?: Expression, alternate?: Expression) {
        super(infos, 'ConditionalExpression', block);
        if (infos) {
            this.test = test;
            this.consequent = consequent;
            this.alternate = alternate;
        }
    }
}
classDict['ConditionalExpression'] = ConditionalExpression;

export class CallExpression extends Expression {
    callee: Expression;
    arguments: Expression[];

    constructor(infos?: Infos, block?: any, callee?: Expression, args: Expression[] = []) {
        super(infos, 'CallExpression', block);
        if (infos) {
            this.callee = callee;
            this.arguments = args;
        }
    }
}
classDict['CallExpression'] = CallExpression;

export class NewExpression extends Expression {
    callee: Expression;
    arguments: Expression[];

    constructor(infos?: Infos, block?: any, callee?: Expression, args: Expression[] = []) {
        super(infos, 'NewExpression', block);
        if (infos) {
            this.callee = callee;
            this.arguments = args;
        }
    }
}
classDict['NewExpression'] = NewExpression;

export class SequenceExpression extends Expression {
    expressions: Expression[];

    constructor(infos?: Infos, block?: any, expressions: Expression[] = []) {
        super(infos, 'SequenceExpression', block);
        if (infos) {
            this.expressions = expressions;
        }
    }
}
classDict['SequenceExpression'] = SequenceExpression;

export class ThisExpression extends Expression {
    constructor(infos?: Infos, block?: any) {
        super(infos, 'ThisExpression', block);
    }
}
classDict['ThisExpression'] = ThisExpression;

export class Super extends Expression {
    constructor(infos?: Infos, block?: any) {
        super(infos, 'Super', block);
    }
}
classDict['Super'] = Super;

export class ArrayExpression extends Expression {
    elements: Expression[];

    constructor(infos?: Infos, block?: any, elements: Expression[] = []) {
        super(infos, 'ArrayExpression', block);
        if (infos) {
            this.elements = elements;
        }
    }
}
classDict['ArrayExpression'] = ArrayExpression;

export class ObjectExpression extends Expression {
    properties: ObjectProperty[];

    constructor(infos?: Infos, block?: any, properties: ObjectProperty[] = []) {
        super(infos, 'ObjectExpression', block);
        if (infos) {
            this.properties = properties;
        }
    }
}
classDict['ObjectExpression'] = ObjectExpression;

export class ObjectProperty {
    loc: Loc;
    key: string;
    value: Expression;
    // method: boolean;
    // computed: boolean;
    // shorthand: boolean;

    constructor(infos?: Infos, block?: any, key?: string, value?: Expression) {
        if (infos) {
            this.loc = new Loc(infos, block);
            this.key = key;
            this.value = value;
        }
    }
}

export class NumericLiteral extends Expression {
    value: number;
    extra: any;

    constructor(infos?: Infos, block?: any) {
        super(infos, 'NumericLiteral', block);
        if (infos) {
            this.extra = block.extra;
            this.value = block.value;
        }
    }
}
classDict['NumericLiteral'] = NumericLiteral;

export class BooleanLiteral extends Expression {
    value: boolean;
    extra: any;

    constructor(infos?: Infos, block?: any) {
        super(infos, 'BooleanLiteral', block);
        if (infos) {
            this.extra = block.extra;
            this.value = block.value;
        }
    }
}
classDict['BooleanLiteral'] = BooleanLiteral;

export class StringLiteral extends Expression {
    value: string;
    extra: any;

    constructor(infos?: Infos, block?: any) {
        super(infos, 'StringLiteral', block);
        if (infos) {
            this.extra = block.extra;
            this.value = block.value;
        }
    }
}
classDict['StringLiteral'] = StringLiteral;

export class NullLiteral extends Expression {
    constructor(infos?: Infos, block?: any) {
        super(infos, 'NullLiteral', block);
    }
}
classDict['NullLiteral'] = NullLiteral;

export class TemplateLiteral extends Expression {
    expressions: any[];
    quasis: any[];

    constructor(infos?: Infos, block?: any) {
        super(infos, 'TemplateLiteral', block);
        if (infos) {
            this.expressions = block.expressions;
            this.quasis = block.quasis;
        }
    }
}
classDict['TemplateLiteral'] = TemplateLiteral;

/** @DONE */
export class RegExpLiteral extends Expression {
    expressions: any[];
    quasis: any[];

    constructor(infos?: Infos, block?: any) {
        super(infos, 'RegExpLiteral', block);
        if (infos) {
            this.expressions = block.expressions;
            this.quasis = block.quasis;
        }
    }
}
classDict['RegExpLiteral'] = RegExpLiteral;

export class FuncPointerLiteral extends Expression {
    func: Func;

    constructor(infos?: Infos, block?: any, func?: Func) {
        super(infos, 'FuncPointerLiteral', block);
        if (infos) {
            this.func = func;
        }
    }
}
classDict['FuncPointerLiteral'] = FuncPointerLiteral;

export class AssignmentPattern extends Expression {
    left: Expression;
    right: Expression;

    constructor(infos?: Infos, block?: any, left?: Expression, right?: Expression) {
        super(infos, 'AssignmentPattern', block);
        if (infos) {
            this.left = left;
            this.right = right;
        }
    }
}
classDict['AssignmentPattern'] = AssignmentPattern;

export class ObjectPattern extends Expression {
    properties: ObjectProperty[];

    constructor(infos?: Infos, block?: any, properties?: ObjectProperty[]) {
        super(infos, 'ObjectPattern', block);
        if (infos) {
            this.properties = properties;
        }
    }
}
classDict['ObjectPattern'] = ObjectPattern;

export class RestElement extends Expression {
    argument: Expression;

    constructor(infos?: Infos, block?: any, argument?: Expression) {
        super(infos, 'RestElement', block);
        if (infos) {
            this.argument = argument;
        }
    }
}
classDict['RestElement'] = RestElement;

export class SpreadElement extends Expression {
    argument: Expression;

    constructor(infos?: Infos, block?: any, argument?: Expression) {
        super(infos, 'SpreadElement', block);
        if (infos) {
            this.argument = argument;
        }
    }
}
classDict['SpreadElement'] = SpreadElement;

export class AwaitExpression extends Expression {
    argument: Expression;

    constructor(infos?: Infos, block?: any, argument?: Expression) {
        super(infos, 'AwaitExpression', block);
        if (infos) {
            this.argument = argument;
        }
    }
}
classDict['AwaitExpression'] = AwaitExpression;
