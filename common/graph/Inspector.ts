
const util = require('util');
var _debug = false;
var indent = 0;

import {
    Loc, Block, Link, Func, Condition,
} from './Block';

import {
    Statement, EmptyStatement, ExpressionStatement, ReturnStatement, BreakStatement, ContinueStatement, ThrowStatement
} from './Statements';

import {
    Expression, Identifier, UnaryExpression, NumericLiteral, BooleanLiteral, StringLiteral, NullLiteral, TemplateLiteral, RegExpLiteral, BinaryExpression, UpdateExpression, AssignmentExpression, LogicalExpression, MemberExpression, ConditionalExpression, CallExpression, NewExpression, ThisExpression, ArrayExpression, ObjectExpression, ObjectProperty, SequenceExpression, FuncPointerLiteral, RestElement, SpreadElement, 
} from './Expressions';

import { Infos, Verifier } from '../inspector/Infos';
import { ErrorMessage } from '../inspector/Report';

function printDebug(...arg: any) {
    if (_debug) {
        console.log("  ".repeat(indent), ...arg);
    }
}

var blocksStack = [];

const inspectBlockFunctions = {
    Block: (infos: Infos, block: Block) => {
        inspectStatements(infos, block.statements);
        return block.links.length > 0 ? block.links[0].to : null;
    },

    WithBlock: (infos: Infos, block: Block) => {
        inspectStatements(infos, block.statements);
        return block.links[0].to;
    },

    IfBlock: (infos: Infos, block: Block) => {
        inspectStatements(infos, block.statements);
        blocksStack.push(block['nextBlock']);

        inspectBlock(infos, block.links[0].to); // then
        if (block.links.length > 1) {
            inspectBlock(infos, block.links[1].to); // else
        }

        blocksStack.pop();
        return block['nextBlock'];
    },
    IfBodyThen: (infos: Infos, block: Block) => {}, // do nothing because empty then
    IfBodyElse: (infos: Infos, block: Block) => {}, // do nothing because empty else

    SwitchBlock: (infos: Infos, block: Block) => {
        inspectStatements(infos, block.statements);
        blocksStack.push(block['nextBlock']);

        block['cases'].forEach((caseBlock: Block) => inspectBlock(infos, caseBlock));

        blocksStack.pop();
        return block['nextBlock'];
    },
    // SwitchCaseBlock: (infos: Infos, block: Block) => {},

    /** @TODO */
    TryBlock: (infos: Infos, block: Block) => {
        inspectStatements(infos, block.statements);
    },
    // CatchClause: (infos: Infos, block: Block) => {},
    // Finalizer: (infos: Infos, block: Block) => {},

    WhileBlock: (infos: Infos, block: Block) => {
        inspectBlock(infos, block.links[1].to); // then
        return block.links[0].to;
    },
    WhileBodyBlock: (infos: Infos, block: Block) => {},

    DoWhileBlock: (infos: Infos, block: Block) => {
        inspectStatements(infos, block.statements);
        return block.links[0].to;
    },

    ForBlock: (infos: Infos, block: Block) => {
        inspectBlock(infos, block.links[1].to);
        return block.links[0].to;
    },
    ForBodyBlock: (infos: Infos, block: Block) => {},

    ForInBlock: (infos: Infos, block: Block) => {
        inspectBlock(infos, block.links[1].to);
        return block.links[0].to;
    },
    ForInBodyBlock: (infos: Infos, block: Block) => {},

    ForOfBlock: (infos: Infos, block: Block) => {
        inspectBlock(infos, block.links[1].to);
        return block.links[0].to;
    },
    ForOfBodyBlock: (infos: Infos, block: Block) => {},

    /** @TODO */
    // FunctionBlock: (infos: Infos, block: Block) => {
    // },
}

const inspectStatementFunctions = {
    EmptyStatement: (infos: Infos, block: Block) => {},

    ExpressionStatement: (infos: Infos, block: Block) => {},

    /** @TODO */
    ReturnStatement: (infos: Infos, block: Block) => {},

    /** @TODO */
    BreakStatement: (infos: Infos, block: Block) => {},

    /** @TODO */
    ContinueStatement: (infos: Infos, block: Block) => {},

    /** @TODO */
    ThrowStatement: (infos: Infos, block: Block) => {},

    /** @TODO */
    ImportStatement: (infos: Infos, block: Block) => {},
}

const inspectExpressionFunctions = {
    Identifier: (infos: Infos, expression: Identifier, func: any) => {},

    NumericLiteral: (infos: Infos, expression: NumericLiteral, func: any) => {},

    BooleanLiteral: (infos: Infos, expression: BooleanLiteral, func: any) => {},

    StringLiteral: (infos: Infos, expression: StringLiteral, func: any) => {},

    NullLiteral: (infos: Infos, expression: NullLiteral, func: any) => {},

    FuncPointerLiteral: (infos: Infos, expression: FuncPointerLiteral, func: any) => {},

    RestElement: (infos: Infos, expression: RestElement, func: any) => {},

    SpreadElement: (infos: Infos, expression: SpreadElement, func: any) => {},

    TemplateLiteral: (infos: Infos, expression: TemplateLiteral, func: any) => {},

    RegExpLiteral: (infos: Infos, expression: RegExpLiteral, func: any) => {},

    ThisExpression: (infos: Infos, expression: ThisExpression, func: any) => {},

    ArrayExpression: (infos: Infos, expression: ArrayExpression, func: any) => {
        expression.elements.forEach(elem => {
            inspectExpression(infos, elem, func);
        });
    },

    ObjectExpression: (infos: Infos, expression: ObjectExpression, func: any) => {
        expression.properties.forEach(prop => {
            inspectExpression(infos, prop.value, func);
        });
    },

    // FunctionExpression: (infos: Infos, expression: FunctionExpression, func: any) => {
    //     if (block.id) {
    //         inspectElement(block.id, infos);
    //     }
    //     inspectArray(block.params, infos);
    //     inspectElement(block.body, infos);
    // },

    // ArrowFunctionExpression: (infos: Infos, expression: ArrowFunctionExpression, func: any) => {
    //     inspectArray(block.params, infos);
    //     inspectElement(block.body, infos);
    // },

    UnaryExpression: (infos: Infos, expression: UnaryExpression, func: any) => {
        inspectExpression(infos, expression.argument, func);
    },

    BinaryExpression: (infos: Infos, expression: BinaryExpression, func: any) => {
        inspectExpression(infos, expression.left, func);
        inspectExpression(infos, expression.right, func);
    },

    UpdateExpression: (infos: Infos, expression: UpdateExpression, func: any) => {
        const oldWriteMode = infos._expressionInfos.writeMode;
        const oldReadMode = infos._expressionInfos.readMode;
        infos._expressionInfos.writeMode = true;
        infos._expressionInfos.readMode = true;
        inspectExpression(infos, expression.argument, func);
        infos._expressionInfos.writeMode = oldWriteMode;
        infos._expressionInfos.writeMode = oldReadMode;
    },

    AssignmentExpression: (infos: Infos, expression: AssignmentExpression, func: any) => {
        const oldWriteMode = infos._expressionInfos.writeMode;
        const oldReadMode = infos._expressionInfos.readMode;
        infos._expressionInfos.writeMode = true;
        inspectExpression(infos, expression.left, func);
        infos._expressionInfos.writeMode = oldWriteMode;
        infos._expressionInfos.readMode = true;
        inspectExpression(infos, expression.right, func);
        infos._expressionInfos.readMode = oldReadMode;
    },

    LogicalExpression: (infos: Infos, expression: LogicalExpression, func: any) => {
        inspectExpression(infos, expression.left, func);
        inspectExpression(infos, expression.right, func);
    },

    MemberExpression: (infos: Infos, expression: MemberExpression, func: any) => {
        const oldNotAVar = infos._expressionInfos.notAVar;
        const oldReadMode = infos._expressionInfos.readMode;
        const oldWriteMode = infos._expressionInfos.writeMode;
        infos._expressionInfos.readMode = true;
        inspectExpression(infos, expression.object, func);
        infos._expressionInfos.writeMode = false;
        if (expression.computed) {
            infos._expressionInfos.notAVar = false;
            inspectExpression(infos, expression.property, func);
            infos._expressionInfos.notAVar = oldNotAVar;
        } else {
            infos._expressionInfos.notAVar = true;
            inspectExpression(infos, expression.property, func);
            infos._expressionInfos.notAVar = oldNotAVar;
        }
        infos._expressionInfos.writeMode = oldWriteMode;
        infos._expressionInfos.readMode = oldReadMode;
    },

    ConditionalExpression: (infos: Infos, expression: ConditionalExpression, func: any) => {
        const oldReadMode = infos._expressionInfos.readMode;
        infos._expressionInfos.readMode = true;
        inspectExpression(infos, expression.test, func);
        infos._expressionInfos.readMode = oldReadMode;
        inspectExpression(infos, expression.consequent, func);
        inspectExpression(infos, expression.alternate, func);
    },

    CallExpression: (infos: Infos, expression: CallExpression, func: any) => {
        const oldReadMode = infos._expressionInfos.readMode;
        infos._expressionInfos.readMode = true;
        inspectExpression(infos, expression.callee, func);
        expression.arguments.forEach(arg => {
            inspectExpression(infos, arg, func);
        });
        infos._expressionInfos.readMode = oldReadMode;
    },

    NewExpression: (infos: Infos, expression: NewExpression, func: any) => {
        const oldReadMode = infos._expressionInfos.readMode;
        infos._expressionInfos.readMode = true;
        inspectExpression(infos, expression.callee, func);
        expression.arguments.forEach(arg => {
            inspectExpression(infos, arg, func);
        });
        infos._expressionInfos.readMode = oldReadMode;
    },

    SequenceExpression: (infos: Infos, expression: SequenceExpression, func: any) => {
        const oldReadMode = infos._expressionInfos.readMode;
        infos._expressionInfos.readMode = false;
        expression.expressions.forEach((expr, i) => {
            if (i === expression.expressions.length - 1) {
                infos._expressionInfos.readMode = oldReadMode;
            }
            inspectExpression(infos, expr, func);
        });
    },
}


/** @TODO */
function inspectStatements(infos: Infos, statements: Statement[]) {
    statements.forEach(statement => {
        if (inspectStatementFunctions[statement.type]) {
            infos._verifiers.apply(infos, statement);
        } else {
            console.log(`NO FOUND STATEMENT INSPECTOR: ${statement.type}`);
        }
    });
}

export function inspectExpression(infos: Infos, expression: Expression, func: any) {
    if (!expression) {
        console.log(`EXPRESSION INSPECTOR on undefined`);
        return;
    }
    if (inspectExpressionFunctions[expression.type]) {
        printDebug(expression.type, expression.loc.start.line+':'+expression.loc.start.column, '{');
        ++indent;

        func(infos, expression);
        ++infos._expressionInfos.expressionRecursion;
        inspectExpressionFunctions[expression.type](infos, expression, func);
        --infos._expressionInfos.expressionRecursion;

        --indent;
        printDebug('}');
    } else {
        console.log(`NO FOUND EXPRESSION INSPECTOR: ${expression.type}`);
    }
}

export function inspectBlock(infos: Infos, block: Block) {
    if (!block) {
        console.log(`BLOCK INSPECTOR on undefined`);
        return;
    }
    if (blocksStack.findIndex(b => b === block) != -1) {
        // printDebug(`Recursive: ${block.type}`);
        return;
    }

    const types = block.type.split(':');
    const type = types[types.length-1];

    if (inspectBlockFunctions[type]) {
        printDebug(block.type + '{');
        ++indent;

        blocksStack.push(block);
        const current = infos['_currentBlock'];
        infos['_currentBlock'] = block;
        const nextBlock = inspectBlockFunctions[type](infos, block);
        infos['_currentBlock'] = current;
        blocksStack.pop();

        --indent;
        printDebug('}');

        if (nextBlock) {
            inspectBlock(infos, nextBlock);
        }
    } else {
        console.log(`NO FOUND BLOCK INSPECTOR: ${block.type}`);
    }
}

export function inspectGraphPart(infos: Infos, rules: Verifier[], block: Block, endBlock: Block) {
    blocksStack = [endBlock];
    infos._verifiers.verifiers = rules;
    inspectBlock(infos, block);
    blocksStack = [];
    infos._verifiers.verifiers = [];
}

export function inspectGraph(infos: Infos, rules: Verifier[], debug=false): Infos {
    _debug = true;

    infos._verifiers.addVerifiers(rules);

    const entryPoint = infos['allBlocks'].find(block => block['entrypoint'] === true);
    if (entryPoint) {
        inspectBlock(infos, entryPoint);
    } else {
        console.log('entryPoint not found');
        throw new ErrorMessage("internal", "internal", `entryPoint not found`, Loc.noLoc(), true);
    }

    return infos;
}
