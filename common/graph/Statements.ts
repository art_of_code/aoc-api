
import { Infos } from '../inspector/Infos';
import { classDict, Indexable } from '../inspector/RegenFactory';
import { Loc, Block } from './Block';
import { Expression, StringLiteral } from './Expressions';

export class Statement extends Indexable {
    type: string;
    blockLoc: Block = null;
    loc: Loc;

    constructor(infos?: Infos, block?: any, type?: string, blockLoc: Block = null) {
        super(infos, type);
        if (infos) {
            this.type = type;
            this.loc = new Loc(infos, block);
            this.blockLoc = blockLoc;
        }
    }
}

export class EmptyStatement extends Statement {
    constructor(infos?: Infos, block?: any, blockLoc: Block = null) {
        super(infos, block, "EmptyStatement", blockLoc);
    }
};
classDict['EmptyStatement'] = EmptyStatement;

export class ExpressionStatement extends Statement {
    expression: Expression;

    constructor(infos?: Infos, block?: any, expression?: Expression, blockLoc: Block = null) {
        super(infos, block, "ExpressionStatement", blockLoc);
        if (expression) {
            this.expression = expression;
        }
    }
};
classDict['ExpressionStatement'] = ExpressionStatement;

export class ReturnStatement extends Statement {
    argument?: Expression;
    implicit: boolean = false;

    constructor(infos?: Infos, block?: any, argument?: Expression, blockLoc: Block = null) {
        super(infos, block, "ReturnStatement", blockLoc);
        if (infos) {
            this.argument = argument;
        }
    }
};
classDict['ReturnStatement'] = ReturnStatement;

export class BreakStatement extends Statement {
    label: string;
    goto: Block;

    constructor(infos?: Infos, block?: any, blockLoc: Block = null) {
        super(infos, block, "BreakStatement", blockLoc);
    }
};
classDict['BreakStatement'] = BreakStatement;

export class ContinueStatement extends Statement {
    label: string;
    goto: Block;

    constructor(infos?: Infos, block?: any, blockLoc: Block = null) {
        super(infos, block, "ContinueStatement", blockLoc);
    }
};
classDict['ContinueStatement'] = ContinueStatement;

export class ThrowStatement extends Statement {
    argument: Expression;

    constructor(infos?: Infos, block?: any, argument?: Expression, blockLoc: Block = null) {
        super(infos, block, "ThrowStatement", blockLoc);
        if (infos) {
            this.argument = argument;
        }
    }
};
classDict['ThrowStatement'] = ThrowStatement;

export class ImportStatement extends Statement {
    specifiers: any[];
    source: StringLiteral;

    constructor(infos?: Infos, block?: any, specifiers?: any[], source?: StringLiteral, blockLoc: Block = null) {
        super(infos, block, "ImportStatement", blockLoc);
        if (infos) {
            this.specifiers = specifiers;
            this.source = source;
        }
    }
};
classDict['ImportStatement'] = ImportStatement;
