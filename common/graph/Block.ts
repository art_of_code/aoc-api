
import { Infos } from '../inspector/Infos';
import { classDict, Regenerable, Indexable } from '../inspector/RegenFactory';
import { Statement, ReturnStatement } from './Statements';
import { Expression } from './Expressions';
import { Variable } from './Variables';

export class Loc extends Regenerable {
    static comp = {
        BEFORE: -2,
        CONTAINS: -1,
        EQ: 0,
        IN: 1,
        AFTER: 2,
    };
    start: { line: number, column: number };
    end: { line: number, column: number };

    constructor(infos?: Infos, block?: any) {
        super('Loc');
        if (infos) {
            this.start = block.loc.start;
            this.end = block.loc.end;
        }
    }

    static noLoc() {
        let loc = new Loc();
        loc.start = { line: 0, column: 0 };
        loc.end = { line: 0, column: 0 };
        return loc;
    }

    // compare the object to the argument (if returns IN, that means this is in the argument location)
    compare(loc: Loc | { start: { line: number, column: number }, end: { line: number, column: number } }) {
        if (this.start.line === loc.start.line
            && this.start.column === loc.start.column
            && this.end.line === loc.end.line
            && this.end.column === loc.end.column) {
            return Loc.comp.EQ;
        }
        if (this.start.line > loc.end.line
            || (this.start.line === loc.end.line
                && this.start.column > loc.end.column)) {
            return Loc.comp.AFTER;
        }
        if (this.end.line < loc.start.line
            || (this.end.line === loc.start.line
                && this.end.column < loc.start.column)) {
            return Loc.comp.BEFORE;
        }
        if ((this.start.line > loc.start.line
            || (this.start.line === loc.start.line && this.start.column >= loc.start.column))
            && (this.end.line < loc.end.line
                || (this.end.line === loc.end.line && this.end.column <= loc.end.column))) {
            return Loc.comp.IN;
        }
        return Loc.comp.CONTAINS;
    }
}
classDict['Loc'] = Loc;

/** Block class and serialize */
export class Block extends Indexable {
    type: string;
    strong: boolean;
    loc: Loc;
    variables: Variable[] = [];
    functions: Func[] = [];
    /** @TODO enlever les identifier et faire des fonctions sur les variables */
    // identifiers: Identifier[] = [];
    statements: Statement[] = [];
    prevLinks: Link[] = [];
    links: Link[] = [];
    scope: Block;

    constructor(infos?: Infos, type?: string, strong?: boolean, block?: any, scope?: any) {
        super(infos, 'Block');
        if (infos) {
            this.type = type;
            this.strong = strong;
            this.loc = new Loc(infos, block);
            this.scope = scope ? scope : this;
        }
    }

    getScope(type: "global" | "strong" | "simple" = "global"): Block {
        let scope = this.scope;
        if (type !== "simple") {
            while (scope.prevLinks.length > 0) {
                if (scope.strong === true && type === "strong") {
                    break;
                }
                scope = scope.prevLinks[0].from.scope;
            }
        }
        return scope;
    }

    getVariable(name: string, type: "global" | "strong" | "simple" = "global"): Variable | undefined {
        let scope = this.scope;
        if (type !== "simple") {
            while (scope.prevLinks.length > 0 || scope.type.includes('FunctionBlock')) {
                if (scope.strong === true && type === "strong") {
                    break;
                }
                let id = scope.variables.find(x => x.name === name);
                if (id) {
                    return id;
                }
                if (scope.type.includes('FunctionBlock')) {
                    scope = (scope['func'] as Func).blockLoc.scope;
                } else {
                    scope = scope.prevLinks[0].from.scope;
                }
            }
        }
        return scope.variables.find(x => x.name === name);
    }

    getAllVariables(type: "global" | "strong" | "simple" = "global"): Variable[] {
        let arr = [];
        let scope = this.scope;

        if (type !== "simple") {
            while (scope.prevLinks.length > 0) {
                if (scope.strong === true && type === "strong") {
                    break;
                }
                arr = arr.concat(scope.variables);
                scope = scope.prevLinks[0].from.scope;
            }
        }
        arr.concat(scope.variables);
        return arr;
    }

    getAllTypes(type: "global" | "strong" | "simple" = "global"): string[] {
        let arr = [];
        let scope = this.scope;

        if (type !== "simple") {
            while (scope.prevLinks.length > 0) {
                if (scope.strong === true && type === "strong") {
                    break;
                }
                arr.push(scope.type);
                scope = scope.prevLinks[0].from.scope;
            }
        }
        arr.push(scope.type)
        return arr;
    }
}
classDict['Block'] = Block;

/** @TODO See if For needs a ForBlock class */
export class Link extends Indexable {
    from: Block;
    to: Block;
    condition: Condition | undefined;
    open: boolean = true;

    constructor(infos?: Infos, from?: any, to?: any) {
        super(infos, 'Link');
        if (infos) {
            this.from = from;
            this.to = to;
        }
    }
}
classDict['Link'] = Link;

export class Condition extends Indexable {
    loc: Loc;
    condition: any;
    be: boolean;

    constructor(infos?: Infos, condition?: any, be?: boolean) {
        super(infos, 'Condition');
        if (infos) {
            this.condition = condition;
            this.be = be;
        }
    }
}
classDict['Condition'] = Condition;

export class Func extends Indexable {
    loc: Loc;
    name?: string;
    body: Block;
    params: Expression[] = [];
    returns: ReturnStatement[] = [];
    blockLoc: Block;
    statementLoc: Statement;

    constructor(infos?: Infos, block?: Block, name?: string, params?: Expression[], body?: Block, blockLoc?: Block) {
        super(infos, 'Func');
        if (infos) {
            this.loc = new Loc(infos, block);
            this.name = name;
            this.params = params;
            this.body = body;
            this.blockLoc = blockLoc;
            if (this.body instanceof Block) {
                this.body['func'] = this
            }
        }
    }

    static getFromLoc(infos: Infos, loc: Loc): Func | undefined {
        if (!(infos['allFuncs'] instanceof Array)) {
            return undefined; // no function found
        }
        const functions = infos['allFuncs'] as Func[];
        const possibleFunctions = functions.
            filter(func => loc.compare(func.loc) === Loc.comp.IN).
            sort((f1, f2) => f1.loc.compare(f2.loc));
        if (possibleFunctions.length === 0) {
            return undefined;
        }
        return possibleFunctions[possibleFunctions.length - 1];
    }
}
classDict['Func'] = Func;

/** @TODO refaire la création de class */
export class Class extends Indexable {
    loc: Loc;
    name?: string;
    body: Block;
    methods: Func[] = [];
    superClass: string;

    constructor(infos?: Infos, block?: Block) {
        super(infos, 'Class');
        if (infos) {
            this.loc = new Loc(infos, block);
        }
    }
}
classDict['Class'] = Class;
