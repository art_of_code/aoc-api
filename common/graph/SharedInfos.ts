import {Block} from './Block';

export class TautologyInfos {
	type: String;
	entrypoint: Block;
	predictable: Block;

	constructor(type:String, entry:Block, predictable: Block) {
		this.type = type;
		this.entrypoint = entry;
		this.predictable = predictable;
	}
};