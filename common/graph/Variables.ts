
import { Infos } from '../inspector/Infos';
import { Loc } from './Block';
import { classDict, Regenerable, Indexable } from '../inspector/RegenFactory';
import { Identifier } from './Expressions';

export enum kind_e {
    var,
    let,
    const,
    undefined
};

export const kindToString = (kind: kind_e) => {
    switch (kind) {
        case kind_e.var:
            return 'var';
        case kind_e.let:
            return 'let';
        case kind_e.const:
            return 'const';
        case kind_e.undefined:
            return 'undefined';
        default:
            return 'ERROR';
    }
}

export enum VarType {
    ERROR = 0,
    ANY,
    UNDEFINED,
    NUMBER,
    STRING,
    BOOLEAN,
    OBJECT,
    ARRAY,
    FUNCTION,
    NAN,
    NULL,
    VOID, // used by function returns
}

export class ComplexType {
    id: string;
    types: Types;

    constructor(id: string, types: Types) {
        this.id = id;
        this.types = types;
    }
};

export class Type {
    type: VarType;
    complexType: ComplexType[];

    constructor(type: VarType = VarType.ERROR, complex?: ComplexType[]) {
        this.type = type;
        this.complexType = complex;
    }
}

export type Types = Type[];

export class Variable extends Indexable {
    loc: Loc;
    name: string;
    kind: kind_e;
    isFunction: boolean;
    isArgument: boolean;
    // value: any;
    types: Types;
    isRead: boolean = false;
    isWrite: boolean = false;
    writeExpr: Identifier[] = [];
    readExpr: Identifier[] = [];

    constructor(infos?: Infos, block?: any, kind?: string) {
        super(infos, 'Variable');
        if (infos) {
            this.name = block.name;
            switch (kind) {
                case 'var':
                case 'argument':
                    this.kind = kind_e.var;
                    break;
                case 'let':
                    this.kind = kind_e.let;
                    break;
                case 'const':
                    this.kind = kind_e.const;
                    break;
                default:
                    this.kind = kind_e.undefined;
                    break;
            }
            this.loc = new Loc(infos, block);
        }
    }

    isVarKind() { return this.kind === kind_e.var; }

    isLetKind() { return this.kind === kind_e.let; }

    isConstKind() { return this.kind === kind_e.const; }

    isUndefinedKind() { return this.kind === kind_e.undefined; }

    dump() {
        console.log(`${this.name}: ${typesToString(this.types)}`);
    }
}
classDict['Variable'] = Variable;

// compare if types0 are AT LEAST in types1
export function compareTypes(types0: Types, types1: Types): boolean {
    if (!types0 || !types1) {
        return false;
    }
    for (const type1 of types1) {
        if (!types0.find(type0 => compareType(type0, type1))) {
            return false;
        }
    }
    return true;
}

// compare if type0 is AT LEAST type1
export function compareType(type0: Type, type1: Type): boolean {
    if (type0.type === VarType.NAN || type1.type === VarType.NAN) {
        return false;
    }
    if (type0.type === VarType.ANY || type1.type === VarType.ANY) {
        return true;
    }
    switch (type0.type) {
        case VarType.OBJECT: {
            if (type1.type !== VarType.OBJECT || type0.complexType.length < type1.complexType.length) {
                return false;
            }
            for (const ctype1 of type1.complexType) {
                const ctype0 = type0.complexType.find(t => t.id === ctype1.id);

                if (!ctype0 || compareTypes(ctype0.types, ctype1.types) === false) {
                    return false;
                }
            }
            return true;
        }
        case VarType.ARRAY: {
            if (type1.type !== VarType.ARRAY) {
                return false;
            }
            if (type0.complexType.length !== 1 || type1.complexType.length !== 1) {
                if (type0.complexType.length !== 1) {
                    console.log('Array types should have only one type but got ' + typeToString(type0));
                }
                if (type1.complexType.length !== 1) {
                    console.log('Array types should have only one type but got ' + typeToString(type1));
                }
                return false;
            }
            return compareTypes(type0.complexType[0].types, type1.complexType[0].types);
        }
        case VarType.FUNCTION: {
            return type1.type !== VarType.FUNCTION
                || type0.complexType.length !== type1.complexType.length;
        }
        default: {
            return type0.type === type1.type;
        }
    }
}

export function mergeTypes(typesArray: Types[]): Types {
    if (!typesArray || !typesArray.length || typesArray.findIndex(t => !t || t.length !== 1) !== -1) {
        return [new Type(VarType.ANY)];
    }
    // Array of NaNs
    if (!typesArray.find(types => types[0].type !== VarType.NAN)) {
        return [new Type(VarType.NAN)];
    }

    const notAnyType = typesArray.find(types => types[0].type !== VarType.ANY);
    if (!notAnyType) {
        return [new Type(VarType.ANY)];
    }

    // check if all are arrays
    if (!typesArray.find(types => types[0].type !== VarType.ARRAY)) {
        const types = typesArray.map(types => types[0].complexType[0].types);
        const mergedType = mergeTypes(types);
        return [new Type(VarType.ARRAY, [new ComplexType('', mergedType)])];
    }

    // check if not an object
    if (typesArray.find(types => types[0].type !== VarType.OBJECT)) {
        for (const types of typesArray) {
            if (notAnyType[0].type === VarType.ANY
            || types[0].type === VarType.ANY
            || !compareType(notAnyType[0], types[0])) {
                /** @TODO  ARRAY DE PLUSIEURS TYPES */
                return [new Type(VarType.ANY)];
            }
        }
        return notAnyType;
    }

    let mergedType = new Type(notAnyType[0].type, [...notAnyType[0].complexType]);
    typesArray.forEach(types => {
        mergedType.complexType = mergedType.complexType.filter(complexType => {
            const t = types[0].complexType.find(t => t.id === complexType.id);
            if (!t || complexType.types.length !== 1 || t.types.length !== 1) {
                return false;
            }
            const mergedSubType = mergeTypes([complexType.types, t.types]);
            if (mergedSubType[0].type === VarType.ANY) {
                return false;
            }
            complexType.types[0] = mergedSubType[0]; /** @CHECK if working */
            return true;
        });
    })
    return [mergedType];
}

export function typesToString(types: Types, inline: boolean = false, indent: number = 0): string {
    let str = '';
    if (types instanceof Array) {
        if (types.length > 0) {
            types.forEach((type, i) => {
                if (i !== 0) {
                    str += ' | ';
                }
                str += typeToString(type, inline, indent);
            });
        } else {
            str += typeToString(types[0]);
        }
    } else {
        console.log('types error: ', types);
        str += 'ERROR';
    }
    return str;
}

export function typeToString(type: Type, inline: boolean = false, indent: number = 0): string {
    const indentStr = '  ';
    const complexTypeToString = (complexType: ComplexType, indent: number = 0) => {
        if (inline === true) {
            return `${complexType.id}: ` + typesToString(complexType.types, inline, indent);
        } else {
            return indentStr.repeat(indent)+`${complexType.id}: ` + typesToString(complexType.types, inline, indent);
        }
    }

    let str = '';
    const p = indentStr.repeat(indent);
    switch (type.type) {
        case VarType.ANY: {
            str += 'any';
            break;
        }
        case VarType.NUMBER: {
            str += 'number';
            break;
        }
        case VarType.STRING: {
            str += 'string';
            break;
        }
        case VarType.BOOLEAN: {
            str += 'boolean';
            break;
        }
        case VarType.NULL: {
            str += 'null';
            break;
        }
        case VarType.NAN: {
            str += 'NaN';
            break;
        }
        case VarType.UNDEFINED: {
            str += 'undefined';
            break;
        }
        case VarType.OBJECT: {
            str += '{';
            type.complexType.forEach((e: ComplexType, i) => {
                if (i !== 0) {
                    str += ',';
                }
                if (inline === true) {
                    str += ' ';
                } else {
                    str += '\n';
                }
                str += complexTypeToString(e, indent+1);
            });
            if (inline === true) {
                str += ' }';
            } else {
                str += '\n' + p + '}';
            }
            break;
        }
        case VarType.ARRAY: {
            str += '[';
            type.complexType.forEach((e: ComplexType, i) => {
                if (i !== 0) {
                    str += ',';
                }
                str += ' ';
                str += typesToString(e.types, true, indent+1);
            });
            str += ' ]';
            break;
        }
        case VarType.FUNCTION: {
            str += 'function(';
            type.complexType.forEach((e: ComplexType, i) => {
                if (i === 0) { // return type
                    return;
                }
                if (i !== 1) {
                    str += ', ';
                }
                str += typesToString(e.types, true, indent+1);
            });
            str += '): ' + typesToString(type.complexType[0].types, true, indent+1);
            break;
        }
        case VarType.VOID: {
            str += 'void';
            break;
        }
        default: {
            str += 'ERROR';
            break;
        }
    }
    return str;
}

