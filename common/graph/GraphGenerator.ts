
const util = require('util');

import {
    Loc, Block, Link, Func, Condition, Class,
} from './Block';

import {
    Variable
} from './Variables';

import {
    Statement, EmptyStatement, ExpressionStatement, ReturnStatement, BreakStatement, ContinueStatement, ThrowStatement, ImportStatement
} from './Statements';

import {
    Expression, Identifier, UnaryExpression, NumericLiteral, BooleanLiteral, StringLiteral, NullLiteral, TemplateLiteral, RegExpLiteral, BinaryExpression, UpdateExpression, AssignmentExpression, LogicalExpression, MemberExpression, ConditionalExpression, CallExpression, NewExpression, ThisExpression, Super, ArrayExpression, ObjectExpression, ObjectProperty, SequenceExpression, FuncPointerLiteral, AssignmentPattern, RestElement, SpreadElement, ObjectPattern, AwaitExpression,
} from './Expressions';

import { Infos } from '../inspector/Infos';
import { ErrorMessage } from '../inspector/Report';

const debugFlag = false;

function printDebug(...args: any[]) {
    if (debugFlag) {
        console.log(...args);
    }
}

const generateInterBlocks = (infos: Infos, block: any, type: string, generateFunc: () => void) => {
    if (infos['_currentBlock'].loc.compare(block.loc) === Loc.comp.EQ) {
        infos['_currentBlock'].type += ':' + type; /** @OK */
        generateFunc();
    } else {
        // There is two blocks to create, one before the body and one after
        let firstb = new Block(infos, type, false, block);
        let secondb = new Block(infos, infos['_currentBlock'].type, false, { loc: { start: block.loc.end, end: infos['_currentBlock'].loc.end } }, infos['_currentBlock']);

        firstb.scope = infos['_currentBlock'].scope;
        secondb.scope = infos['_currentBlock'].scope;

        infos['_currentBlock'].loc.end = firstb.loc.start;
        if (infos['_currentBlock'].links.length > 1) {
            throw 'Internal Error: generateInterBlocks: last block as more than one next';
        }

        [secondb.links, infos['_currentBlock'].links] = [infos['_currentBlock'].links, []];
        if (secondb.links.length > 0) {
            secondb.links[0].from = secondb;
        }

        let prev = new Link(infos, infos['_currentBlock'], firstb);
        infos['_currentBlock'].links.push(prev); // prev block.links  => new block
        firstb.prevLinks.push(prev); // new block.prevLinks => prev block
        infos['_currentBlock'] = firstb;

        let next = new Link(infos, infos['_currentBlock'], secondb);
        infos['_currentBlock'].links.push(next); // next block.links  => new block
        secondb.prevLinks.push(next); // new block.prevLinks => next block

        generateFunc();

        infos['_currentBlock'] = secondb;
    }
}

const generateStatementFunctions = {

    /** @DONE */
    File: (infos: Infos, block: any) => {
        generateStatement(infos, block.program);
    },

    /** @DONE */
    Program: (infos: Infos, block: any) => {
        printDebug('==Program==', block);
        block.loc.start.line = -1
        const b = new Block(infos, 'Block', true, block);
        b['entrypoint'] = true;
        infos['_currentBlock'] = b;
        block.body.forEach((statement: any) => generateStatement(infos, statement));
        b.loc.start.line = 0;
    },

    /** @DONE */
    ExpressionStatement: (infos: Infos, block: any) => {
        printDebug('==ExpressionStatement==', block);
        const current: Block = infos['_currentBlock'];
        const statement = new Statement(infos, block, block.type, current);
        infos['_currentStatement'] = statement;
        statement['expression'] = generateExpression(infos, block.expression);
        infos['_currentStatement'] = null;
        current.statements.push(statement);
    },

    /** @DONE */
    BlockStatement: (infos: Infos, block: any) => {
        printDebug('==BlockStatement==', block);
        generateInterBlocks(infos, block, 'Block', () => {
            infos['_currentBlock'].scope = infos['_currentBlock'];
            block.body.forEach((e: any) => generateStatement(infos, e));
        });
    },

    /** @DONE */
    EmptyStatement: (infos: Infos, block: any) => {
        printDebug('==EmptyStatement==', block);
        const current: Block = infos['_currentBlock'];
        current.statements.push(new EmptyStatement(infos, block, current));
    },

    /** @TEST */
    WithStatement: (infos: Infos, block: any) => {
        printDebug('==WithStatement==', block);
        generateInterBlocks(infos, block, 'WithBlock', () => {
            infos['_currentBlock'].object = generateExpression(infos, block.object);
            block.body.body.forEach((e: any) => generateStatement(infos, e));
        });
    },

    /** CONTROL FLOW */

    /** @TEST */
    ReturnStatement: (infos: Infos, block: any) => {
        printDebug('==ReturnStatement==', block);
        const current: Block = infos['_currentBlock'];
        const statement = new ReturnStatement(infos, block, null, current);
        infos['_currentStatement'] = statement;
        statement.argument = generateExpression(infos, block.argument);
        infos['_currentStatement'] = null;
        current.statements.push(statement);
        if (current.links.length > 0) {
            current.links[0].open = false;
        }
    },

    /** @TEST
     * it does not log the labeled statements (only the labeled blocks).
     */
    LabeledStatement: (infos: Infos, block: any) => {
        printDebug('==LabeledStatement==', block);
        const current: Block = infos['_currentBlock'];
        generateStatement(infos, block.body);
        if (infos['_currentBlock'] !== current && current.links.length > 0) {
            current.links[0]['labeled'] = block.label.name;
        }
    },

    /** @TODO */
    BreakStatement: (infos: Infos, block: any) => {
        printDebug('==BreakStatement==', block);
        const current = infos['_currentBlock'];
        const statement = new BreakStatement(infos, block, current);
        current.statements.push(statement);
        if (block.label) {
            statement.label = block.label.name;
        }
    },

    /** @TODO */
    ContinueStatement: (infos: Infos, block: any) => {
        printDebug('==ContinueStatement==', block);
        const current = infos['_currentBlock'];
        const statement = new ContinueStatement(infos, block, current);
        current.statements.push(statement);
        if (block.label) {
            statement.label = block.label.name;
        }
    },

    /** CHOICE */

    /** @TEST */
    IfStatement: (infos: Infos, block: any) => {
        printDebug('==IfStatement==', block);
        generateInterBlocks(infos, block, 'IfBlock', () => {
            const ifBlock = infos['_currentBlock'];
            const nextBlock = infos['_currentBlock'].links[0].to;

            ifBlock['nextBlock'] = nextBlock;
            const consequentBlock = new Block(infos, 'IfBodyThen', false, block.consequent);

            if (ifBlock.type.indexOf(':') === -1) { // not else if
                ifBlock.links[0] = new Link(infos, ifBlock, consequentBlock);
            } else {
                ifBlock.links[0].to = consequentBlock;
            }
            ifBlock.links[0].condition = new Condition(infos, generateExpression(infos, block.test), true);
            consequentBlock.prevLinks.push(ifBlock.links[0]);
            consequentBlock.links.push(new Link(infos, consequentBlock, nextBlock));

            ifBlock.loc.end = consequentBlock.loc.start;
            infos['_currentBlock'] = consequentBlock;

            generateStatement(infos, block.consequent);

            if (block.alternate) {
                const alternateBlock = new Block(infos, 'IfBodyElse', false, block.alternate);
                const linkFrom = new Link(infos, ifBlock, alternateBlock);
                const linkTo = new Link(infos, alternateBlock, nextBlock);

                ifBlock.links.push(linkFrom);
                alternateBlock.prevLinks.push(linkFrom);
                alternateBlock.links.push(linkTo);

                infos['_currentBlock'] = alternateBlock;

                generateStatement(infos, block.alternate);
            } else {
                const linkNoAlternate = new Link(infos, ifBlock, nextBlock);
                ifBlock.links.push(linkNoAlternate);
            }

            nextBlock.prevLinks[0].from = ifBlock;
        });
    },

    /** @TEST
     *
     * pas de corp
     * une seule case
     * une seule case default
     * plusieurs cases
     * plusieurs cases avec un default
     * plusieurs cases avec un default pas à la fin
     *
    */
    SwitchStatement: (infos: Infos, block: any) => {
        printDebug('==SwitchStatement==', block);
        generateInterBlocks(infos, block, 'SwitchBlock', () => {
            const switchBlock: Block = infos['_currentBlock'];
            const next: Block = switchBlock.links[0].to;

            switchBlock["cases"] = [];
            switchBlock['discriminant'] = generateExpression(infos, block.discriminant);
            switchBlock['nextBlock'] = next;
            switchBlock.links[0].condition = new Condition(infos, 'block.cases.length === 0', true)
            block.cases.forEach(element => {
                const case_ = new Block(infos, 'SwitchCaseBlock', false, element.consequent[0], switchBlock.scope);
                switchBlock['cases'].push(case_);

                const caseLink = new Link(infos, switchBlock, case_); // links switch => case
                /** @TODO transformer le test en Expression */
                caseLink.condition = new Condition(infos, { left: switchBlock, right: element.test }, true);

                switchBlock.links.push(caseLink);
                case_.prevLinks.push(caseLink);

                if (element.test === null) { // default case
                    switchBlock.links[0].to = case_;
                }

                if (switchBlock['cases'].length > 1) { // not first case
                    const prev = switchBlock['cases'][switchBlock['cases'].length - 2] // before last
                    prev.links[0].to = case_; // changes previous next link pointing to this case
                }

                const caseNextLink = new Link(infos, case_, next); // links case => end
                case_.links.push(caseNextLink);

                infos['_currentBlock'] = case_;
                generateStatement(infos, element.consequent[0]);
                infos['_currentBlock'] = switchBlock;
            });
        });
    },

    /** EXCEPTIONS */

    /** @TEST */
    ThrowStatement: (infos: Infos, block: any) => {
        printDebug('==ThrowStatement==', block);
        const current: Block = infos['_currentBlock'];
        const statement = new ThrowStatement(infos, block, null, current);
        infos['_currentStatement'] = statement;
        statement.argument = generateExpression(infos, block.argument);
        infos['_currentStatement'] = null;
        current.statements.push(statement);
    },

    /** @TEST */
    TryStatement: (infos: Infos, block: any) => {
        printDebug('==TryStatement==', block);
        generateInterBlocks(infos, block, 'TryBlock', () => {
            const current: Block = infos['_currentBlock'];

            current.loc.end = block.block.loc.end; /** set the loc.end to the end of TryBlock's end */
            generateStatement(infos, block.block);

            if (block.handler) {
                const catchClause = new Block(infos, "CatchClause", false, block.handler);
                catchClause.prevLinks[0] = new Link(infos, current, catchClause);
                catchClause.links.push(current.links[0]);
                current['handler'] = catchClause;
                infos['_currentBlock'] = current['handler'];
                generateStatement(infos, block.handler);
            }
            if (block.finalizer) {
                const finalizer = new Block(infos, "Finalizer", false, block.finalizer);
                finalizer.prevLinks[0] = new Link(infos, current, finalizer);
                finalizer.links.push(current.links[0]);
                current['finalizer'] = finalizer;
                infos['_currentBlock'] = current['finalizer'];
                generateStatement(infos, block.finalizer);
            }
        });
    },

    /** @TEST */
    CatchClause: (infos: Infos, block: any) => {
        printDebug('==CatchClause==', block);
        infos['_currentBlock'].param = generateExpression(infos, block.param);
        generateStatement(infos, block.body);
    },

    /** LOOPS */

    /** @DONE */
    WhileStatement: (infos: Infos, block: any) => {
        printDebug('==WhileStatement==', block);
        generateInterBlocks(infos, block, 'WhileBlock', () => {
            const current = infos['_currentBlock'];
            const body = new Block(infos, 'WhileBodyBlock', false, block.body);
            const next = infos['_currentBlock'].links[0].to;

            const bodyInLink = new Link(infos, current, body); // links current => body
            const bodyOutLink = new Link(infos, body, next); // links body => next | uses next block for breaks and continues

            current.links.push(bodyInLink);
            body.prevLinks.push(bodyInLink);
            body.links.push(bodyOutLink);

            current.links[0].condition = new Condition(infos, generateExpression(infos, block.test), false); // false link
            bodyInLink.condition = new Condition(infos, generateExpression(infos, block.test), true); // true link

            infos['_currentBlock'] = body;
            generateStatement(infos, block.body);
            infos['_currentBlock'] = current;
            bodyOutLink.to = current; // resets the link body => current
        });
    },

    /** @DONE */
    DoWhileStatement: (infos: Infos, block: any) => {
        printDebug('==DoWhileStatement==', block);
        generateInterBlocks(infos, block, 'DoWhileBlock', () => {
            const current = infos['_currentBlock'];

            const trueLink = new Link(infos, current, current); // true link
            trueLink.condition = new Condition(infos, generateExpression(infos, block.test), true);
            current.links[0].condition = new Condition(infos, generateExpression(infos, block.test), false); // false link
            current.links.push(trueLink);

            generateStatement(infos, block.body);
        });
    },

    /** @TEST
     *
     * without init
     * without update
     * without cond
     * body block
     * body statement
     *
     */
    ForStatement: (infos: Infos, block: any) => {
        printDebug('==ForStatement==', block);
        generateInterBlocks(infos, block, 'ForBlock', () => {
            const current: Block = infos['_currentBlock'];
            const body = new Block(infos, 'ForBodyBlock', false, block.body);
            const next = infos['_currentBlock'].links[0].to;

            if (block.init) {
                if (block.init.type === 'VariableDeclaration') {
                    generateStatement(infos, block.init);
                } else {
                    current['init'] = generateExpression(infos, block.init);
                }
            }
            if (block.update) {
                current['update'] = generateExpression(infos, block.update);
            }

            const bodyInLink = new Link(infos, current, body); // links current => body
            const bodyOutLink = new Link(infos, body, next); // links body => next | uses next block for breaks and continues

            current.links.push(bodyInLink);
            body.prevLinks.push(bodyInLink);
            body.links.push(bodyOutLink);

            if (block.test) {
                bodyInLink.condition = new Condition(infos, generateExpression(infos, block.test), true);
                current.links[0].condition = new Condition(infos, generateExpression(infos, block.test), false); /** @TODO transformer la condition */
            } else {
                let loc = block.init
                    ? { line: block.init.loc.end.line, column: block.init.loc.end.column + 1 }
                    : { line: block.loc.end.line, column: block.loc.end.column + 5 };

                current.links[0].condition = new Condition(infos, generateExpression(infos, {
                    type: 'BooleanLiteral',
                    value: false,
                    loc: { start: loc, end: loc }
                }), true);
            }

            infos['_currentBlock'] = body;
            generateStatement(infos, block.body);
            infos['_currentBlock'] = current;
            bodyOutLink.to = current; // resets the link body => current
        });
    },

    /** @TEST
     *
     * body block
     * body statement
     *
     */
    ForInStatement: (infos: Infos, block: any) => {
        printDebug('==ForInStatement==', block);
        generateInterBlocks(infos, block, 'ForInBlock', () => {
            const current = infos['_currentBlock'];
            const body = new Block(infos, 'ForInBodyBlock', false, block.body);
            const next = infos['_currentBlock'].links[0].to;

            current['left'] = generateExpression(infos, block.left);
            current['right'] = generateExpression(infos, block.right);

            const bodyInLink = new Link(infos, current, body); // links current => body
            const bodyOutLink = new Link(infos, body, next); // links body => next | uses next block for breaks and continues

            current.links.push(bodyInLink);
            body.prevLinks.push(bodyInLink);
            body.links.push(bodyOutLink);

            bodyInLink.condition = new Condition(infos, 'iter_end', false);
            current.links[0].condition = new Condition(infos, 'iter_end', true);

            infos['_currentBlock'] = body;
            generateStatement(infos, block.body);
            infos['_currentBlock'] = current;
            bodyOutLink.to = current; // resets the link body => current
        });
    },

    /** @TEST
     *
     * body block
     * body statement
     *
     */
    ForOfStatement: (infos: Infos, block: any) => {
        printDebug('==ForOfStatement==', block);
        generateInterBlocks(infos, block, 'ForOfBlock', () => {
            const current = infos['_currentBlock'];
            const body = new Block(infos, 'ForOfBodyBlock', false, block.body);
            const next = infos['_currentBlock'].links[0].to;

            current['left'] = generateExpression(infos, block.left);
            current['right'] = generateExpression(infos, block.right);

            const bodyInLink = new Link(infos, current, body); // links current => body
            const bodyOutLink = new Link(infos, body, next); // links body => next | uses next block for breaks and continues

            current.links.push(bodyInLink);
            body.prevLinks.push(bodyInLink);
            body.links.push(bodyOutLink);

            bodyInLink.condition = new Condition(infos, 'iter_end', false);
            current.links[0].condition = new Condition(infos, 'iter_end', true);

            infos['_currentBlock'] = body;
            generateStatement(infos, block.body);
            infos['_currentBlock'] = current;
            bodyOutLink.to = current; // resets the link body => current
        });
    },

    /** DECLARATIONS */

    /** @TODO
     * faire que les controleflow générent un block
     * généré une variable "var" block.id.name si n'existe pas.
     * mettre dans le haut du bloc courant
     */
    FunctionDeclaration: (infos: Infos, block: any) => {
        printDebug('==FunctionDeclaration==', block);
        const current: Block = infos['_currentBlock'];
        const func = new Func(infos, block,
            block.id.name,
            [],
            new Block(infos, "FunctionBlock", true, block.body),
            current);
        let type = 'strong';
        infos['_currentBlock'] = func.body;

        // generate variable for the params
        func.params = block.params.map((param: any, i: number) => {
            const paramExpr = generateExpression(infos, param);
            if (paramExpr.type === 'Identifier') {
                const paramId = paramExpr as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var");
                paramId.variable = v;
                v.isArgument = true;
                func.body.variables.push(v);
            } else if (paramExpr.type === 'AssignmentPattern' && (paramExpr as AssignmentPattern).left.type === 'Identifier') {
                const paramId = (paramExpr as AssignmentPattern).left as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var")
                paramId.variable = v;
                v.isArgument = true;
                func.body.variables.push(v);
            } else if (paramExpr.type === 'RestElement' && (paramExpr as RestElement).argument.type === 'Identifier') {
                const paramId = (paramExpr as RestElement).argument as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var")
                v.isArgument = true;
                func.body.variables.push(v);
            }
            return paramExpr;
        });
        generateStatement(infos, block.body);
        const genAssignment = (funcDecl, scope: Block) => {
            /** @TODO rework that shit */
            const startSave = funcDecl.loc.start;
            const endSave = funcDecl.loc.end;
            const funcPointerBlock = { type: 'FuncPointerLiteral', loc: block.id.loc };
            funcDecl.loc.start = scope.loc.start;
            funcDecl.loc.end = scope.loc.start;
            let assignmentExpression: Expression,
                identifier: Expression,
                funcPointerLiteral: Expression,
                statement = new ExpressionStatement(infos, funcDecl,
                    assignmentExpression = new AssignmentExpression(infos, funcDecl,
                        identifier = generateExpression(infos, funcDecl.id),
                        funcPointerLiteral = new FuncPointerLiteral(infos, funcPointerBlock, func)), current);
            statement['declaration'] = true;
            assignmentExpression['declaration'] = true;
            scope.statements.unshift(statement);
            assignmentExpression.statementLoc = statement;
            identifier.statementLoc = statement;
            funcPointerLiteral.statementLoc = statement;
            funcDecl.loc.start = startSave;
            funcDecl.loc.end = endSave;
        };

        /** @TODO refaire la gestion des overwrites car elle doit être faite en top-botton au lieu de linéaire
         * ex:
         ```
         {
           function a() {}
         }
         function a() {}
         ```
         * la première fonction 'a' devrait être instanciée dans le scope simple et non strong
         */
        if (!current.getVariable(block.id.name, 'strong')) {
            // console.log(`var ${block.id.name} not found in strong`);
            const variable = new Variable(infos, block.id, 'var');
            variable.isFunction = true;
            const b = current.getScope('strong');
            b.variables.push(variable);
            b.functions.push(func);
            genAssignment(block, b);
        } else {
            const variable = current.getVariable(block.id.name, 'simple');
            const b = current.getScope('simple');
            b.functions.push(func);
            if (!variable) {
                // console.log(`var ${block.id.name} not found in simple`);
                type = 'simple';
                const variable = new Variable(infos, block.id, 'var');
                variable.isFunction = true;
                b.variables.push(variable);
                genAssignment(block, b);
            } else if (variable.isFunction) {
                // console.log(`var ${block.id.name} is a function`);
                for (const statement of b.statements) {
                    if (statement['declaration']
                        && (((statement as ExpressionStatement).expression as AssignmentExpression).left as Identifier).name === func.name
                        && ((statement as ExpressionStatement).expression as AssignmentExpression).right.type === 'FuncPointerLiteral') {
                        const funcPointer = ((statement as ExpressionStatement).expression as AssignmentExpression).right as FuncPointerLiteral;
                        infos.report.report(new ErrorMessage('genGraph', 'overwrite', `Function \'${funcPointer.func.name}\' is not reachable`, funcPointer.func.loc));
                        funcPointer.func = func;
                    }
                }
            } else {
                // console.log(`var ${block.id.name} is not a function`);
                infos.report.report(new ErrorMessage('genGraph', 'overwrite', `Variable \'${func.name}\' has already been declared`, block.loc));
            }
        }
        infos['_currentBlock'] = current;
    },

    /** @TODO vérifier que les const ont bien une initialisation */
    VariableDeclaration: (infos: Infos, block: any) => {
        printDebug('==VariableDeclaration==', block);
        const current: Block = infos['_currentBlock'];
        const type = (block.kind === 'var') ? 'strong' : 'simple';

        const genAssignment = varDecl => {
            if (varDecl.init !== null) {
                const statement = new ExpressionStatement(infos, varDecl, null, current);
                infos['_currentStatement'] = statement;
                statement.expression = generateExpression(infos, {
                    type: 'AssignmentExpression',
                    loc: varDecl.loc,
                    operator: '=',
                    left: varDecl.id,
                    right: varDecl.init,
                });
                statement['declaration'] = true;
                statement.expression['declaration'] = true;
                infos['_currentStatement'] = null;
                current.statements.push(statement);
            }
        };

        block.declarations.forEach(varDecl => {
            const id = current.getVariable(varDecl.id.name, type);
            if (!id) {
                /** If the identifier is not known in the scope */
                const variable = new Variable(infos, varDecl.id, block.kind)
                current.getScope(type).variables.push(variable);
                genAssignment(varDecl);
            } else if (block.kind === 'var' && id.isVarKind()) {
                /** If it is a redeclaration of an existing var */
                genAssignment(varDecl);
            } else {
                /** Can't redeclare any other type than 'var' */
                infos.report.report(new ErrorMessage('genGraph', 'genGraph', `Variable \'${varDecl.id.name}\' has already been declared`, block.loc));
            }
        });
    },

    /** @TODO */
    ClassDeclaration: (infos: Infos, block: any) => {
        printDebug('==ClassDeclaration== ', block);
        const current = infos['_currentBlock'];
        const type = 'simple'
        const classe = new Class(infos, block);

        if (block.superClass) {
            classe.superClass = block.superClass.name;
        }
        infos['_lastClass'] = classe;
        generateStatement(infos, block.body);
        // const genAssignment = varDecl => {
        //     const statement = new ExpressionStatement(infos, varDecl,
        //         new AssignmentExpression(infos, varDecl,
        //             generateExpression(infos, varDecl.id),
        //             generateExpression(infos, varDecl.init)))
        //     statement['declaration'] = true;
        //     statement.expression['declaration'] = true;
        //     current.statements.push(statement);
        // };

        // const id = current.getVariable(block.id.name, type);
        // if (!id) {
        //     const variable = new Variable(infos, block.id, 'var');
        //     current.getScope(type).variables.push(variable);
        //     genAssignment(block);
        // }
    },

    /** @TODO */
    ClassBody: (infos: Infos, block: any) => {
        printDebug('==ClassBody== ', block);
        if (!infos['_lastClass']) {
            console.log('ClassBody found outside a ClassDeclaration:', block)
            return;
        }
        block.body.forEach(statement => {
            generateStatement(infos, statement);
        });

        // block.body.forEach(statement => generateStatement(infos, statement) );
    },

    /** @TODO */
    ClassMethod: (infos: Infos, block: any) => {
        printDebug('==ClassMethod== ', block);
        if (!(infos['_lastClass'] instanceof Class)) {
            console.log('ClassMethod found outside a ClassDeclaration:', block)
            return;
        }
        const current = infos['_currentBlock'];
        const func = new Func(infos, block, "", [], new Block(infos, "FunctionBlock", true, block.body), current);
        (infos['_lastClass'] as Class).methods.push(func);

        infos['_currentBlock'] = func.body;
        if (block.key && block.key.type === 'Identifier') {
            func.name = block.key.name;
        }

        // generate variable for the params
        func.params = block.params.map((param: any, i: number) => {
            const paramExpr = generateExpression(infos, param);
            if (paramExpr.type === 'Identifier') {
                const paramId = paramExpr as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var");
                paramId.variable = v;
                v.isArgument = true;
                func.body.variables.push(v);
            } else if (paramExpr.type === 'AssignmentPattern' && (paramExpr as AssignmentPattern).left.type === 'Identifier') {
                const paramId = (paramExpr as AssignmentPattern).left as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var")
                paramId.variable = v;
                v.isArgument = true;
                func.body.variables.push(v);
            } else if (paramExpr.type === 'RestElement' && (paramExpr as RestElement).argument.type === 'Identifier') {
                const paramId = (paramExpr as RestElement).argument as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var")
                v.isArgument = true;
                func.body.variables.push(v);
            }
            return paramExpr;
        });

        generateStatement(infos, block.body);
        infos['_currentBlock'] = current;
    },

    /** @TODO */
    ClassProperty: (infos: Infos, block: any) => {
        printDebug('==ClassProperty== ', block);
        // generateStatement(infos, block.key);
        // if (block.value) {
        //     generateStatement(infos, block.value);
        // }
    },

    /** @TODO gérer les 3 cas:
     * import 'a';
     * import foo from 'bar';
     * import { foo1 } from 'bar1';
     * + changer le type ImportStatement.specifiers
     */
    ImportDeclaration: (infos: Infos, block: any) => {
        printDebug('==ImportDeclaration==', block);
        const current: Block = infos['_currentBlock'];
        const statement = new ImportStatement(
            infos, block,
            block.specifiers, null,
            current);
        infos['_currentStatement'] = statement;
        statement.source = generateExpressionFunctions['StringLiteral'](infos, block.source);
        infos['_currentStatement'] = null;
        current.statements.push(statement);
    },
};

const generateExpressionFunctions = {

    /** @DONE */
    Identifier: (infos: Infos, block: any) => {
        printDebug('==Identifier==', block);
        return new Identifier(infos, block.name, block, null);
    },

    /** @DONE */
    NumericLiteral: (infos: Infos, block: any) => {
        printDebug('==NumericLiteral==', block);
        return new NumericLiteral(infos, block);
    },

    /** @DONE */
    BooleanLiteral: (infos: Infos, block: any) => {
        printDebug('==BooleanLiteral==', block);
        return new BooleanLiteral(infos, block);
    },

    /** @DONE */
    StringLiteral: (infos: Infos, block: any) => {
        printDebug('==StringLiteral==', block);
        return new StringLiteral(infos, block);
    },

    /** @DONE */
    NullLiteral: (infos: Infos, block: any) => {
        printDebug('==NullLiteral==', block);
        return new NullLiteral(infos, block);
    },

    /** @DONE */
    TemplateLiteral: (infos: Infos, block: any) => {
        printDebug('==TemplateLiteral==', block);
        return new TemplateLiteral(infos, block);
    },

    /** @DONE */
    RegExpLiteral: (infos: Infos, block: any) => {
        printDebug('==RegExpLiteral==', block);
        return new RegExpLiteral(infos, block);
    },

    /** @TODO
     * faire un FunctionLiteral qui pointe vers une Function
     *
     */
    FunctionExpression: (infos: Infos, block: any) => {
        printDebug('==FunctionExpression==', block);
        const current = infos['_currentBlock'];
        const func = new Func(infos, block, "", [], new Block(infos, "FunctionBlock", true, block.body), current);

        infos['_currentBlock'] = func.body;
        if (block.id) {
            func.name = block.id.name;
            // generate variable for the function (used by recursive calls)
            const v = new Variable(infos, block.id, "var");
            v.isFunction = true;
            func.body.variables.push(v);
        }

        // generate variable for the params
        func.params = block.params.map((param: any, i: number) => {
            const paramExpr = generateExpression(infos, param);
            if (paramExpr.type === 'Identifier') {
                const paramId = paramExpr as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var");
                paramId.variable = v;
                v.isArgument = true;
                func.body.variables.push(v);
            } else if (paramExpr.type === 'AssignmentPattern' && (paramExpr as AssignmentPattern).left.type === 'Identifier') {
                const paramId = (paramExpr as AssignmentPattern).left as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var")
                paramId.variable = v;
                v.isArgument = true;
                func.body.variables.push(v);
            } else if (paramExpr.type === 'RestElement' && (paramExpr as RestElement).argument.type === 'Identifier') {
                const paramId = (paramExpr as RestElement).argument as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var")
                v.isArgument = true;
                func.body.variables.push(v);
            }
            return paramExpr;
        });

        generateStatement(infos, block.body);
        infos['_currentBlock'] = current;
        const blockLoc = { type: 'FuncPointerLiteral', loc: { start: block.loc.start, end: { line: block.loc.start.line, column: block.loc.start.line + 8 } } };
        return new FuncPointerLiteral(infos, blockLoc, func);
    },

    /** @TODO
     * same
     *
     */
    ArrowFunctionExpression: (infos: Infos, block: any) => {
        printDebug('==ArrowFunctionExpression== ', block);
        const current = infos['_currentBlock'];
        const func = new Func(infos, block, "", [], new Block(infos, "FunctionBlock", true, block.body), current);

        infos['_currentBlock'] = func.body;

        // generate variable for the params
        func.params = block.params.map((param: any, i: number) => {
            const paramExpr = generateExpression(infos, param);
            if (paramExpr.type === 'Identifier') {
                const paramId = paramExpr as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var");
                paramId.variable = v;
                v.isArgument = true;
                func.body.variables.push(v);
            } else if (paramExpr.type === 'AssignmentPattern' && (paramExpr as AssignmentPattern).left.type === 'Identifier') {
                const paramId = (paramExpr as AssignmentPattern).left as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var")
                paramId.variable = v;
                v.isArgument = true;
                func.body.variables.push(v);
            } else if (paramExpr.type === 'RestElement' && (paramExpr as RestElement).argument.type === 'Identifier') {
                const paramId = (paramExpr as RestElement).argument as Identifier;
                paramId.argument = true;
                const v = new Variable(infos, paramId, "var")
                v.isArgument = true;
                func.body.variables.push(v);
            }
            return paramExpr;
        });

        if (block.body.type === 'BlockStatement') {
            generateStatement(infos, block.body);
        } else {
            const expr = generateExpression(infos, block.body);
            const returnObj = { type: 'ReturnStatement', loc: { start: block.body.loc.start, end: block.body.loc.start } }
            const statement = new ReturnStatement(infos, returnObj, expr, func.body);
            expr.statementLoc = statement;
            func.body.statements.push(statement);
        }
        infos['_currentBlock'] = current;
        const blockLoc = { type: 'FuncPointerLiteral', loc: { start: block.loc.start, end: { line: block.loc.start.line, column: block.loc.start.line + 8 } } };
        return new FuncPointerLiteral(infos, blockLoc, func);;
    },

    /** @DONE */
    UnaryExpression: (infos: Infos, block: any) => {
        printDebug('==UnaryExpression==', block);
        return new UnaryExpression(infos, block,
            generateExpression(infos, block.argument));
    },

    /** @DONE */
    UpdateExpression: (infos: Infos, block: any) => {
        printDebug('==UpdateExpression==', block);
        return new UpdateExpression(infos, block,
            generateExpression(infos, block.argument));
    },

    /** @DONE */
    BinaryExpression: (infos: Infos, block: any) => {
        printDebug('==BinaryExpression==', block);
        return new BinaryExpression(infos, block,
            generateExpression(infos, block.left),
            generateExpression(infos, block.right));
    },

    /** @DONE */
    AssignmentExpression: (infos: Infos, block: any) => {
        printDebug('==AssignmentExpression==', block);
        return new AssignmentExpression(infos, block,
            generateExpression(infos, block.left),
            generateExpression(infos, block.right));
    },

    /** @DONE */
    LogicalExpression: (infos: Infos, block: any) => {
        printDebug('==LogicalExpression==', block);
        return new LogicalExpression(infos, block,
            generateExpression(infos, block.left),
            generateExpression(infos, block.right));
    },

    /** @DONE */
    MemberExpression: (infos: Infos, block: any) => {
        printDebug('==MemberExpression==', block);
        const expr = new MemberExpression(infos, block,
            generateExpression(infos, block.object),
            generateExpression(infos, block.property));
        if (!expr.computed) {
            if (expr.property.type !== 'Identifier') {
                console.log('ERROR MemberExpression: property should be an Identifier ', block);
            }
            (expr.property as Identifier).notaVar = true;
        }
        return expr;
    },

    /** @DONE */
    ConditionalExpression: (infos: Infos, block: any) => {
        printDebug('==ConditionalExpression==', block);
        return new ConditionalExpression(infos, block,
            generateExpression(infos, block.test),
            generateExpression(infos, block.consequent),
            generateExpression(infos, block.alternate));
    },

    /** @DONE */
    CallExpression: (infos: Infos, block: any) => {
        printDebug('==CallExpression==', block);
        return new CallExpression(infos, block,
            generateExpression(infos, block.callee),
            block.arguments.map(arg => generateExpression(infos, arg)));
    },

    /** @DONE */
    NewExpression: (infos: Infos, block: any) => {
        printDebug('==NewExpression==', block);
        return new NewExpression(infos, block,
            generateExpression(infos, block.callee),
            block.arguments.map(arg => generateExpression(infos, arg)));
    },

    /** @DONE */
    SequenceExpression: (infos: Infos, block: any) => {
        printDebug('==SequenceExpression==', block);
        return new SequenceExpression(infos, block,
            block.expressions.map(expression => generateExpression(infos, expression)));
    },

    /** @DONE */
    ThisExpression: (infos: Infos, block: any) => {
        printDebug('==ThisExpression==', block);
        return new ThisExpression(infos, block);
    },

    /** @DONE */
    Super: (infos: Infos, block: any) => {
        printDebug('==Super==', block);
        return new Super(infos, block);
    },
    /** @DONE */
    ArrayExpression: (infos: Infos, block: any) => {
        printDebug('==ArrayExpression==', block);
        return new ArrayExpression(infos, block,
            block.elements.map(elem =>
                (elem ? generateExpression(infos, elem) : { type: 'EmptyElement' })));
    },

    /** @TEST */
    ObjectExpression: (infos: Infos, block: any) => {
        printDebug('==ObjectExpression==', block);
        return new ObjectExpression(infos, block,
            block.properties.map(prop => {
                let key: string;
                switch (prop.key.type) {
                    case 'NumericLiteral': {
                        key = prop.key.extra.raw;
                        break;
                    }
                    case 'StringLiteral': {
                        key = prop.key.value;
                        break;
                    }
                    case 'Identifier': {
                        key = prop.key.name;
                        break;
                    }
                    default: {
                        infos.report.report(new ErrorMessage('parsing', 'parsing', `bad key type ${prop.key.type}`, prop.key.loc));
                        /** @harderror */
                    }
                }
                return new ObjectProperty(infos, prop,
                    key,
                    generateExpression(infos, prop.value))
            }));
    },

    /** @DONE */
    AssignmentPattern: (infos: Infos, block: any) => {
        printDebug('==AssignmentPattern==', block);
        return new AssignmentPattern(infos, block,
            generateExpression(infos, block.left),
            generateExpression(infos, block.right));
    },

    /** @DONE */
    RestElement: (infos: Infos, block: any) => {
        printDebug('==RestElement==', block);
        return new RestElement(infos, block,
            generateExpression(infos, block.argument));
    },

    /** @DONE */
    SpreadElement: (infos: Infos, block: any) => {
        printDebug('==SpreadElement==', block);
        return new SpreadElement(infos, block,
            generateExpression(infos, block.argument));
    },


    /** @DONE */
    ObjectPattern: (infos: Infos, block: any) => {
        printDebug('==ObjectPattern==', block);
        return new ObjectPattern(infos, block,
            block.properties.map(prop => {
                let key: string;
                switch (prop.key.type) {
                    case 'NumericLiteral': {
                        key = prop.key.extra.raw;
                        break;
                    }
                    case 'StringLiteral': {
                        key = prop.key.value;
                        break;
                    }
                    case 'Identifier': {
                        key = prop.key.name;
                        break;
                    }
                    default: {
                        infos.report.report(new ErrorMessage('parsing', 'parsing', `bad key type ${prop.key.type}`, prop.key.loc));
                        /** @harderror */
                    }
                }
                return new ObjectProperty(infos, prop,
                    key,
                    generateExpression(infos, prop.value))
            }));
    },

    /** @DONE */
    AwaitExpression: (infos: Infos, block: any) => {
        printDebug('==AwaitExpression==', block);
        return new AwaitExpression(infos, block,
            generateExpression(infos, block.argument));
    },
};

function generateStatement(infos: Infos, block: any) {
    if (!block) {
        return;
    }
    if (block instanceof Array) {
        throw new Error("generateStatement get an Array as argument");
    }
    if (generateStatementFunctions[block.type]) {
        generateStatementFunctions[block.type](infos, block);
    } else {
        if (infos['_currentBlock']) {
            /** put the unknown statement in the current block */
            infos['_currentBlock'].statements.push(block);
        }
        const errMessage = `NO FOUND STATEMENT PARSER : ${block.type}:${block.loc.start.line}:${block.loc.start.column}`;
        console.log(errMessage);
        console.log(block);
        // infos.report.report(new ErrorMessage('internal', 'internal', `NO FOUND STATEMENT PARSER ${block.type}`, block.loc, true));
        throw errMessage;
    }
}

/** @DONE */
function generateExpression(infos: Infos, block: any, throwOnNotFound = false): Expression {
    if (block) {
        if (generateExpressionFunctions[block.type]) {
            return generateExpressionFunctions[block.type](infos, block);
        } else {
            const errMessage = `NO FOUND EXPRESSION PARSER : ${block.type}:${block.loc.start.line}:${block.loc.start.column}`;
            console.log(errMessage);
            console.log(block);
            // infos.report.report(new ErrorMessage('internal', 'internal', `NO FOUND EXPRESSION PARSER ${block.type}`, block.loc, true));
            if (throwOnNotFound) {
                throw errMessage;
            }
        }
    }
}

export function generateGraph(ast: any): Infos {
    let infos = new Infos();

    infos['_currentStatement'] = null;
    generateStatement(infos, ast);

    return infos;
}
