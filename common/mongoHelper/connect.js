const mongoose = require('mongoose');

const mongoString = process.env.DB_URI;

module.exports = {
  async connectToDatabase() {
    return mongoose.connect(
      mongoString + '/aoc',
      { user: process.env.DB_ROOT_USERNAME, pass: process.env.DB_ROOT_PASSWORD, authSource: 'admin', useNewUrlParser: true, useUnifiedTopology: false, bufferCommands: false, bufferMaxEntries: 0 }
    );
  },

  async disconnect() { return mongoose.disconnect(); }
}
